package com.esahai.rest.paytm;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.websocket.server.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import com.paytm.merchant.CheckSumServiceHelper;

@Path("/services")
public class Service {

	/*
	 * This method accepts XML as input data.  Below is an example format:
	 * <TransactionObject>
	 * 	<MID>MYINDM93143068797645</MID>
	 * 	<ORDER_ID>ORDR1234</ORDER_ID>
	 * 	<CUST_ID>CUST0001</CUST_ID>
	 * 	<TXN_AMOUNT>20.00</TXN_AMOUNT>
	 * 	<EMAIL>mohan.m@esahai.in</EMAIL>
	 * 	<MOBILE_NO>9100999814</MOBILE_NO>
	 * </TransactionObject>
	 * 
	 * It converts this XML to TransactionObject object and extracts the parameters
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.TEXT_XML)
	@Path("/generateChecksum")
	public Map<String, Object> generateChecksum(@PathParam("p1") String xmlData) {
		String checkSum = null;
		Map<String, Object> returnMap = new HashMap<String, Object>();
		try {
			System.out.println("generateChecksum. xmlData = " + xmlData);
			if (xmlData == null) { //If the input parameter is null, return an empty Map
				return returnMap;
			}

			TreeMap<String, String> paramMap = new TreeMap<String, String>();
			if (xmlData != null) {
				//Use JAXBContext & Unmarshaller to unmarshal the XML string to TransactionObject
				JAXBContext jaxbContext = JAXBContext.newInstance(TransactionObject.class);
				Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

				StringReader reader = new StringReader(xmlData);

				TransactionObject transactionObject = (TransactionObject) unmarshaller.unmarshal(reader);
				System.out.println("generateChecksum. transactionObject = " + transactionObject);

				//Extract the parameters from TransactionObject and place them in TreeMap
				paramMap.put("MID", transactionObject.getMid());
				paramMap.put("ORDER_ID", transactionObject.getOrderId());
				paramMap.put("CUST_ID", transactionObject.getCustId());
				paramMap.put("TXN_AMOUNT", transactionObject.getTxnAmount());
				paramMap.put("EMAIL", transactionObject.getEmail());
				paramMap.put("MOBILE_NO", transactionObject.getMobileNo());
			}

			//INDUSTRY_TYPE_ID, CHANNEL_ID & WEBSITE are constants
			paramMap.put("INDUSTRY_TYPE_ID", Constants.INDUSTRY_TYPE_ID);
			paramMap.put("CHANNEL_ID", Constants.CHANNEL_ID);
			paramMap.put("WEBSITE", Constants.WEBSITE);

			System.out.println("generateChecksum. MERCHANT_KEY = " + Constants.MERCHANT_KEY);
			System.out.println("generateChecksum. paramMap = " + paramMap);

			//Call the genrateCheckSum method to generate the checkSum value
			checkSum = CheckSumServiceHelper.getCheckSumServiceHelper().genrateCheckSum(Constants.MERCHANT_KEY,
					paramMap);

			//Add the generated checkSum to the return Map
			returnMap.put("CHECKSUMHASH", checkSum);

			System.out.println(
					"generateChecksum. checkSum = '" + checkSum + "' (consider the value inside single quote)");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return returnMap;
	}

	/*
	 * This method accepts JSON Object as input data.  Below is an example format:
	 * {
	 * 	"MID": "MYINDM93143068797645",
	 * 	"ORDER_ID":"TestMerchant000111008",
	 * 	"CUST_ID":"mohit.aggarwal@paytm.com",
	 * 	"INDUSTRY_TYPE_ID":"Retail",
	 * 	"CHANNEL_ID":"WAP",
	 * 	"TXN_AMOUNT":"1",
	 * 	"WEBSITE":"APP_STAGING",
	 * 	"EMAIL":"pullaiah.d@esahai.in",
	 * 	"MOBILE_NO":"9502609443",
	 * }
	 * 
	 */	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/generateChecksum")
	public Map<String, Object> generateChecksum(Map<String, String> inputMap) {
		String checkSum = null;
		Map<String, Object> returnMap = new HashMap<String, Object>();
		try {
			System.out.println("generateChecksum. inputMap = " + inputMap);
			if (inputMap == null) { //If the input parameter is null, return an empty Map
				return returnMap;
			}

			TreeMap<String, String> paramMap = new TreeMap<String, String>();
			if (inputMap != null) {
				//Extract the parameters from input Map and place them in TreeMap
				paramMap.put("MID", inputMap.get("MID"));
				paramMap.put("ORDER_ID", inputMap.get("ORDER_ID"));
				paramMap.put("CUST_ID", inputMap.get("CUST_ID"));
				paramMap.put("TXN_AMOUNT", inputMap.get("TXN_AMOUNT"));
				paramMap.put("EMAIL", inputMap.get("EMAIL"));
				paramMap.put("MOBILE_NO", inputMap.get("MOBILE_NO"));
			}

			//INDUSTRY_TYPE_ID, CHANNEL_ID & WEBSITE are constants
			paramMap.put("INDUSTRY_TYPE_ID", Constants.INDUSTRY_TYPE_ID);
			paramMap.put("CHANNEL_ID", Constants.CHANNEL_ID);
			paramMap.put("WEBSITE", Constants.WEBSITE);

			System.out.println("generateChecksum. MERCHANT_KEY = " + Constants.MERCHANT_KEY);
			System.out.println("generateChecksum. paramMap = " + paramMap);

			//Call the genrateCheckSum method to generate the checkSum value
			checkSum = CheckSumServiceHelper.getCheckSumServiceHelper().genrateCheckSum(Constants.MERCHANT_KEY,
					paramMap);

			//Add the generated checkSum to the return Map
			returnMap.put("CHECKSUMHASH", checkSum);

			System.out.println(
					"generateChecksum. checkSum = '" + checkSum + "' (consider the value inside single quote)");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return returnMap;
	}

	/*
	 * This method accepts XML as input data.  Below is an example format:
	 * <TransactionObject>
	 * 	<MID>MYINDM93143068797645</MID>
	 * 	<ORDER_ID>ORDR1234</ORDER_ID>
	 * 	<CUST_ID>CUST0001</CUST_ID>
	 * 	<TXN_AMOUNT>20.00</TXN_AMOUNT>
	 * 	<EMAIL>mohan.m@esahai.in</EMAIL>
	 * 	<MOBILE_NO>9100999814</MOBILE_NO>
	 * 	<CHECKSUMHASH>1rz5xVHsUOmudMcyN328GGc6DnRKNe5hQgDBRR+tIA8WIUhIPMmwJprIHms/3CFP/eUyKrm1wLYE98XST7dEBvn3SMgiTCDHhkiJQSIMFNg=</CHECKSUMHASH>
	 * </TransactionObject>
	 * 
	 * It converts this XML to TransactionObject object and extracts the parameters
	 */	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.TEXT_XML)
	@Path("/verifyChecksum")
	public Map<String, Object> verifyChecksum(@PathParam("p1") String xmlData) throws Exception {
		boolean isValidChecksum = false;
		Map<String, Object> returnMap = new HashMap<String, Object>();
		try {
			System.out.println("verifyChecksum. xmlData = " + xmlData);
			if (xmlData == null) { //If the input parameter is null, return an empty Map
				return returnMap;
			}

			//Use JAXBContext & Unmarshaller to unmarshal the XML string to TransactionObject
			JAXBContext jaxbContext = JAXBContext.newInstance(TransactionObject.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

			StringReader reader = new StringReader(xmlData);

			TransactionObject transactionObject = (TransactionObject) unmarshaller.unmarshal(reader);
			System.out.println("verifyChecksum. transactionObject = " + transactionObject);

			TreeMap<String, String> paramMap = new TreeMap<String, String>();

			//Extract the parameters from TransactionObject and place them in TreeMap
			paramMap.put("MID", transactionObject.getMid());
			paramMap.put("ORDER_ID", transactionObject.getOrderId());
			paramMap.put("CUST_ID", transactionObject.getCustId());
			paramMap.put("TXN_AMOUNT", transactionObject.getTxnAmount());
			paramMap.put("EMAIL", transactionObject.getEmail());
			paramMap.put("MOBILE_NO", transactionObject.getMobileNo());

			//INDUSTRY_TYPE_ID, CHANNEL_ID & WEBSITE are constants
			paramMap.put("INDUSTRY_TYPE_ID", Constants.INDUSTRY_TYPE_ID);
			paramMap.put("CHANNEL_ID", Constants.CHANNEL_ID);
			paramMap.put("WEBSITE", Constants.WEBSITE);

			String checksumHash = transactionObject.getCheckSumHash();

			System.out.println("verifyChecksum. MERCHANT_KEY = " + Constants.MERCHANT_KEY);
			System.out.println("verifyChecksum. paramMap = " + paramMap);
			System.out.println("verifyChecksum. checksumHash = " + checksumHash);

			//Call the verifycheckSum method to verify the checkSum value
			isValidChecksum = CheckSumServiceHelper.getCheckSumServiceHelper().verifycheckSum(Constants.MERCHANT_KEY,
					paramMap, checksumHash);

			System.out.println("verifyChecksum. isValidChecksum = " + isValidChecksum);

			//Add the generated flag to the return Map
			returnMap.put("FLAG", isValidChecksum);

			// if checksum is validated Kindly verify the amount and status
			// if transaction is successful
			// kindly call Paytm Transaction Status API and verify the
			// transaction amount and status.
			// If everything is fine then mark that transaction as successful
			// into your DB.
		} catch (Exception e) {
			e.printStackTrace();
		}

		return returnMap;
	}

	/*
	 * This method accepts JSON Object as input data.  Below is an example format:
	 * {
	 * 	"MID": "MYINDM93143068797645",
	 * 	"ORDER_ID":"TestMerchant000111008",
	 * 	"CUST_ID":"mohit.aggarwal@paytm.com",
	 * 	"INDUSTRY_TYPE_ID":"Retail",
	 * 	"CHANNEL_ID":"WAP",
	 * 	"TXN_AMOUNT":"1",
	 * 	"WEBSITE":"APP_STAGING",
	 * 	"EMAIL":"pullaiah.d@esahai.in",
	 * 	"MOBILE_NO":"9502609443",
	 * 	"CHECKSUMHASH": "eXB++k5EAJAKBPfVwVuDBslFSgttx2eIVyJG5LqwnPlTbc5+2+tCMqh9bTIf07Bi59p2FJDUU9OQaZpC4OBlOrSnc24Uyao/vcTDbJSFc/Y="
	 * }
	 * 
	 */		
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/verifyChecksum")
	public Map<String, Object> verifyChecksum(Map<String, String> inputMap) throws Exception {
		boolean isValidChecksum = false;
		Map<String, Object> returnMap = new HashMap<String, Object>();
		try {
			System.out.println("verifyChecksum. inputMap = " + inputMap);
			if (inputMap == null) { //If the input parameter is null, return an empty Map
				return returnMap;
			}

			TreeMap<String, String> paramMap = new TreeMap<String, String>();

			//Extract the parameters from input Map and place them in TreeMap
			paramMap.put("MID", inputMap.get("MID"));
			paramMap.put("ORDER_ID", inputMap.get("ORDER_ID"));
			paramMap.put("CUST_ID", inputMap.get("CUST_ID"));
			paramMap.put("TXN_AMOUNT", inputMap.get("TXN_AMOUNT"));
			paramMap.put("EMAIL", inputMap.get("EMAIL"));
			paramMap.put("MOBILE_NO", inputMap.get("MOBILE_NO"));

			//INDUSTRY_TYPE_ID, CHANNEL_ID & WEBSITE are constants
			paramMap.put("INDUSTRY_TYPE_ID", Constants.INDUSTRY_TYPE_ID);
			paramMap.put("CHANNEL_ID", Constants.CHANNEL_ID);
			paramMap.put("WEBSITE", Constants.WEBSITE);

			String checksumHash = inputMap.get("CHECKSUMHASH");

			System.out.println("verifyChecksum. MERCHANT_KEY = " + Constants.MERCHANT_KEY);
			System.out.println("verifyChecksum. paramMap = " + paramMap);
			System.out.println("verifyChecksum. checksumHash = " + checksumHash);

			//Call the verifycheckSum method to verify the checkSum value
			isValidChecksum = CheckSumServiceHelper.getCheckSumServiceHelper().verifycheckSum(Constants.MERCHANT_KEY,
					paramMap, checksumHash);

			System.out.println("verifyChecksum. isValidChecksum = " + isValidChecksum);

			//Add the generated flag to the return Map
			returnMap.put("FLAG", isValidChecksum);

			// if checksum is validated Kindly verify the amount and status
			// if transaction is successful
			// kindly call Paytm Transaction Status API and verify the
			// transaction amount and status.
			// If everything is fine then mark that transaction as successful
			// into your DB.
		} catch (Exception e) {
			e.printStackTrace();
		}

		return returnMap;
	}
}