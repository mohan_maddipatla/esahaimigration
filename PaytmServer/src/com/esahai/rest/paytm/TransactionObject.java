package com.esahai.rest.paytm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "TransactionObject")
public class TransactionObject {
	@XmlElement(name = "MID")
	String mid;
	
	@XmlElement(name = "ORDER_ID")
	String orderId;

	@XmlElement(name = "CUST_ID")
	String custId;

	@XmlElement(name = "TXN_AMOUNT")
	String txnAmount;

	@XmlElement(name = "EMAIL")
	String email;

	@XmlElement(name = "MOBILE_NO")
	String mobileNo;
	
	@XmlElement(name = "CHECKSUMHASH")
	String checkSumHash;

	public String getMid() {
		return mid;
	}


	public void setMid(String mid) {
		this.mid = mid;
	}


	public String getOrderId() {
		return orderId;
	}


	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}


	public String getCustId() {
		return custId;
	}


	public void setCustId(String custId) {
		this.custId = custId;
	}


	public String getTxnAmount() {
		return txnAmount;
	}


	public void setTxnAmount(String txnAmount) {
		this.txnAmount = txnAmount;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getMobileNo() {
		return mobileNo;
	}


	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	
	
	
	public String getCheckSumHash() {
		return checkSumHash;
	}


	public void setCheckSumHash(String checkSumHash) {
		this.checkSumHash = checkSumHash;
	}


	@Override
	public String toString() {
		return "MID = " + this.mid + ", ORDER_ID = " + this.orderId + ", CUST_ID = " + this.custId + ", TXN_AMOUNT = " + this.txnAmount + ", EMAIL = " + this.email + ", MOBILE_NO = " + this.mobileNo + ", CHECKSUMHASH = " + this.checkSumHash;
	}
}
