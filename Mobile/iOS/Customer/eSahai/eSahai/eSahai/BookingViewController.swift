//
//  BookingViewController.swift
//  eSahai
//
//  Created by UshaRao on 11/29/16.
//  Copyright © 2016 TechVedika. All rights reserved.
//

import UIKit

class BookingViewController: BaseViewController {

    
    @IBOutlet var btnCancel_Outlet: UIButton!
    @IBOutlet var pageControlObj: UIPageControl!
    @IBOutlet var constraintAmbulanceLeading: NSLayoutConstraint!
    @IBOutlet var imgAmbulance: UIImageView!
    var timer = Timer()
    var timer1 = Timer()
    
    var url = NSString()
    var headers = NSDictionary()
    var request = NSDictionary()
  //  var BookingID = NSString()
    var iscoming = NSString()
    

    
    @IBOutlet weak var TextData: UILabel!
    override func viewDidLoad(){
        super.viewDidLoad()

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: "Ambulance Booking Screen")
        
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        
       
        self.navigationController?.navigationBar.isHidden = true
        // Do any advarional setup after loading the view.
        btnCancel_Outlet.layer.masksToBounds = false
        btnCancel_Outlet.layer.cornerRadius = 40.0
        btnCancel_Outlet.clipsToBounds = true
        
        if(self.iscoming != "Ambulance"){
            self.imgAmbulance.image = UIImage(named:"medical_taxi_img.png")
            
            if(appDelegate.bookingString.length)>0 {
                TextData.text = "Your booking has been cancelled by the Medical Taxi on while we are assigning your booking to another Medical Taxi."
            }else {
                TextData.text = "Finding your nearest Medical Taxi"
            }
        }else{
            if(appDelegate.bookingString.length)>0 {
                TextData.text = appDelegate.bookingString
            }
            
        }
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.moveImage(view:)), userInfo: nil, repeats: true)
        
        timer1 = Timer.scheduledTimer(timeInterval: 45.0, target: self, selector: #selector(self.POPTOVIEWCONTROLLER), userInfo: nil, repeats: false)
        
        // self.perform(#selector(self.cancelBooking), with: nil, afterDelay: 45.0)
        
        //  self.bookingAmbulance()
        let notificationName = Notification.Name("booking_confirmed")
        NotificationCenter.default.addObserver(self, selector: #selector(BookingViewController.BookingConfirmed(notification:)), name: notificationName, object: nil)
        
       // let not2 = Notification.Name("cancel_booking")
        //NotificationCenter.default.addObserver(self, selector: #selector(BookingViewController.goToDashBoard), name: not2, object: nil)
    }
    
    func BookingConfirmed(notification:NSNotification) -> Void {
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
            let bookingTrackingController = mainStoryboard.instantiateViewController(withIdentifier: "BookingTrackingViewController") as! BookingTrackingViewController
            bookingTrackingController.dataDictionary = notification.userInfo as!  NSDictionary
           // bookingTrackingController.booking_id = self.BookingID
            bookingTrackingController.fromBooking = true
            bookingTrackingController.isComingFrom = self.iscoming

            appDelegate.bookingString = ""
            self.timer.invalidate()
            self.timer1.invalidate()
            self.navigationController?.pushViewController(bookingTrackingController, animated: true)
    }
    
    func goToDashBoard() {
        self.timer.invalidate()
       // UserDefaults.standard.setValue(false, forKey:"isTrackingOn")
        appDelegate.bookingString = ""
        for VC: UIViewController in self.navigationController!.viewControllers {
            if (VC is DashboardViewController) {
                self.navigationController!.popToViewController(VC, animated: true)
                return
            }
        }
        
        let signIn = self.storyboard?.instantiateViewController(withIdentifier:"DashboardViewController") as! DashboardViewController
        self.navigationController?.pushViewController(signIn, animated: false)
        
        return
    }

    func cancelBooking() {

            
       /*     let  strUrl = String(format : "%@%@",kServerUrl,"cancelBooking") as NSString
            
        let dictParams = ["iOS":"true",
                          "ambulanceApp": "false",
                          "customerApp": "true",
                          "booking_id": BookingID,
                          "customer_id": UserDefaults.standard.value(forKey: kCustomer_id)as! String,
                          "customer_mobile":UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String]
            as NSDictionary
            let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
        
            sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in
                DispatchQueue.main.async()
                    {
                        let strStatusCode = result["statusCode"] as? String
                        
                        if strStatusCode == "300" {
                            self.timer.invalidate()
                            self.navigationController?.popViewController(animated: true)
                        }
                        else
                        {
                            self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                        }
                }
            }, failureHandler: {(error) in
            })*/
        
        
     /*   let alertController = UIAlertController(title: "", message: "No Ambulance found for the given criteria.. Connect to customer Service?" , preferredStyle: UIAlertControllerStyle.alert)
        
        let DestructiveAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.destructive) { (result : UIAlertAction) -> Void in
        }
        alertController.addAction(DestructiveAction)
        self.present(alertController, animated: true, completion: nil)*/
        
        self.POPTOVIEWCONTROLLER()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btnCancelClicked(_ sender: AnyObject) {
        
        let alertController = UIAlertController(title: "", message: "Are you sure you want to cancel the booking?", preferredStyle: UIAlertControllerStyle.alert)
        let DestructiveAction = UIAlertAction(title: "No", style: UIAlertActionStyle.destructive) { (result : UIAlertAction) -> Void in
            
        }
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            
           // self.cancelBooking()
           
            
            
                
                
            let  strUrl = String(format : "%@%@",kServerUrl,"cancelBooking") as NSString
            
            let dictParams = ["iOS":"true",
                              "ambulanceApp": "false",
                              "customerApp": "true",
                              "booking_id": self.appDelegate.bookingID ,
                              "customer_id": UserDefaults.standard.value(forKey: kCustomer_id)as! String,
                              "customer_mobile":UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String]
                as NSDictionary
            let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
            
            self.sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in
                DispatchQueue.main.async()
                    {
                        let strStatusCode = result["statusCode"] as? String
                        
                        if strStatusCode == "300" {
                            self.timer.invalidate()
                            self.timer1.invalidate()
                            self.appDelegate.bookingString = ""
                            self.navigationController?.popViewController(animated: true)
                        }
                        else if(strStatusCode != "500"){
                            self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                        }
                }
            }, failureHandler: {(error) in
            })
                
             self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(DestructiveAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func noAmbulancesFound()  {
       
    }
    
    
    func POPTOVIEWCONTROLLER() {
        self.timer.invalidate()
      //  UserDefaults.standard.setValue(false, forKey:"isTrackingOn")
       // for VC: UIViewController in self.navigationController!.viewControllers {
         //   if (VC is DashboardViewController) {
              
                let notificationName = Notification.Name("getActiveBooking")
                NotificationCenter.default.post(name: notificationName, object: nil, userInfo: nil )
            appDelegate.bookingString = ""
            self.navigationController?.popViewController(animated: true)
                
               // self.navigationController!.popToViewController(VC, animated: true)
           //     return
           // }
        //}
    }
   
    // MARK: - Navigation

    func moveImage(view: UIImageView) {
        
        if self.constraintAmbulanceLeading.constant <= -320
        {
           self.constraintAmbulanceLeading.constant = 0
        }
        else
        {
            self.constraintAmbulanceLeading.constant = self.constraintAmbulanceLeading.constant-12
        }
        
        if pageControlObj.currentPage >= 2 {
            pageControlObj.currentPage = 0
        }
        else {
            pageControlObj.currentPage = pageControlObj.currentPage+1
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }

}
