//
//  BaseViewController.swift
//  eSahai
//
//  Created by UshaRao on 11/9/16.
//  Copyright © 2016 TechVedika. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit

class BaseViewController: UIViewController {

    var appDelegate = AppDelegate()
    let sharedController = SharedController()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


extension BaseViewController {
    
    func logout(){
        let countryLists = UserDefaults.standard.value(forKey: kCountryList) as! NSArray
        
        for i in 0..<countryLists.count {
            
            if ( ((countryLists.object(at: i) as! NSDictionary ).value(forKey: "country")as! String)  == "India") {
                self.appDelegate.row = i
            }
        }
        UserDefaults.standard.set(false, forKey: kISSIGNEDIN)
        // print(UserDefaults.value(forKey:kISSIGNEDIN))
        UserDefaults.standard.synchronize()
       
        for VC: UIViewController in SlideNavigationController.sharedInstance().viewControllers
        {
            if (VC is SignInViewController) {
                self.navigationController!.popToViewController(VC, animated: true)
                
                let alert = UIAlertController(title: "", message: "Someone else has logged in with same number entered in another device", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.destructive, handler: { (alert: UIAlertAction!) in
                    
                }))
                
               UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
                return
            }
        }
        
        
        let signIn = self.storyboard?.instantiateViewController(withIdentifier:"SignInViewController") as! SignInViewController
        self.navigationController?.pushViewController(signIn, animated: false)
        
        let alert = UIAlertController(title: "", message: "Someone else has logged in with same number entered in another device", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.destructive, handler: { (alert: UIAlertAction!) in
            
        }))
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        return

        
    }
}
