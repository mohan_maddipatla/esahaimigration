//
//  UserDetailsViewController.swift
//  eSahai
//
//  Created by PINKY on 22/03/17.
//  Copyright © 2017 TechVedika. All rights reserved.
//

import UIKit

class UserDetailsViewController: BaseViewController,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate{

    
    var customPickerView : UIView! = UIView()
    var pickerView : UIPickerView = UIPickerView()
    let iOSDeviceScreenSize = UIScreen.main.bounds.size
    
    var testDelegate : delegateUpdate?

   // var BookId = NSString()
    var arrSex = NSMutableArray()
    @IBOutlet weak var txtName: UITextField!
    
    @IBOutlet weak var txtContact: UITextField!
    
    @IBOutlet weak var txtGender: UITextField!
    
    @IBOutlet weak var txtAge: UITextField!
    
    @IBOutlet weak var txtPatientCondition: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       // self.navigationItem.setHidesBackButton(true, animated: false)
        self.title = "Update Patient Info"
        arrSex = ["Male","Female"]

        self.addToolBar(txtAge)
        
        self.getUserDetails()
    }
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return false
    }
    
    

    func getUserDetails() {
        
        let  strUrl = String(format : "%@%@",kServerUrl,"getPatientInfo") as NSString
        
        let dictParams = ["customerApp": "true",
                          "ambulanceApp": "false",
                          "customer_mobile":UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String,
                          "booking_id": appDelegate.bookingID]
            as NSDictionary
        
        let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
        
        sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in DispatchQueue.main.async()
            {
                let strStatusCode = result["statusCode"] as? String
                if strStatusCode == "300" {
                    
                    let tempDict = ((result["responseData"] as! NSArray).object(at: 0)) as! NSMutableDictionary
                    
                    for (key, value) in  tempDict {
                        print("value --> \(value)" )
                        if value is NSNull{
                            tempDict[key] = ""
                            // arrEmergencyTypeList.replaceObject(at: i, with: tempDict)
                        }
                    }

                    
                        if((tempDict.value(forKey: "patient_name" ) as! String ) == "No Name"){
                             self.txtName.text = ""
                        }else {
                             self.txtName.text = (tempDict.value(forKey: "patient_name" ) as! String)
                        }
                       
                        self.txtContact.text = (tempDict.value(forKey: "contact_number" ) as! String)
                    
                    
                    if((tempDict.value(forKey: "age" ) as! String) == "0"){
                        self.txtAge.text = ""
                    }else{
                        self.txtAge.text = (tempDict.value(forKey: "age" ) as! String)
                    }
                    
                    if((tempDict.value(forKey: "patient_condition" ) as! String ) == " "){
                       
                        self.txtPatientCondition.text = ""
                    }else{
                        self.txtPatientCondition.text = (tempDict.value(forKey: "patient_condition" ) as! String)

                    }
                    
                    
                    
                    if((tempDict.value(forKey: "gender" ) as! String ) == ""){
                        self.txtGender.text = self.arrSex.object(at: 0) as! String
                    }else{
                         self.txtGender.text = (tempDict.value(forKey: "gender" ) as! String )
                    }
                    
                   
                }
                else if strStatusCode == "204"{
                    self.logout()
                }
                else if(strStatusCode != "500"){
                self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                }
            }
        }, failureHandler: {(error) in})
    }
     
    
    
    func sendUserDetails() {
        
        let  strUrl = String(format : "%@%@",kServerUrl,"addPatientInfo") as NSString
        
        var Age  = String()
        if(txtAge.text == ""){
            Age = "0"
        }else{
            Age = txtAge.text!
        }
        
        let dictParams = ["customerApp": "true",
                          "ambulanceApp": "false",
                          "customer_mobile":UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String,
                          "patient_name":txtName.text ?? "No Name",
                          "contact_number": txtContact.text ?? "",
                          "patient_condition":txtPatientCondition.text ?? "",
                          "gender":txtGender.text ?? "",
                          "age": Age,
                          "booking_id": appDelegate.bookingID] as NSDictionary
        print(dictParams)
        
        let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
        
         print(dictHeaders)
        sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in DispatchQueue.main.async()
            {
                let strStatusCode = result["statusCode"] as? String
                if strStatusCode == "300" {
                    
                    print(result)
                    self.testDelegate?.updateData()
                    self.navigationController?.popViewController(animated: true)
                    
                }
                else if strStatusCode == "204"{
                    self.logout()
                }
                else if(strStatusCode != "500"){
                    
                    self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                }
            }
        }, failureHandler: {(error) in})
    }
    
    
    @IBAction func submitBtn(_ sender: Any) {
        self.sendUserDetails()
    }
 
    
    @IBAction func dropDownAction(_ sender: Any) {
        self.view.endEditing(true)
        self.openPickerView()
    }
   
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.dismissPicker()
        return textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
       
        if textField == txtGender {
            self.view.endEditing(true)
            self.openPickerView()
            return false
        }
        self.dismissPicker()
        return true
        
    }
   /* func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtContactNumber {
            let oldLength = textField.text?.characters.count
            let replacementLength = string.characters.count
            let rangeLength = range.length
            let newLength = oldLength! - rangeLength + replacementLength
            // var returnKey = (string as NSString).rangeOf("\n").location != NSNotFound
            return newLength <= 10
        }
        else {
            return true
        }
    }*/
    
    func addToolBar(_ textField: UITextField) {
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        toolBar.isTranslucent = false
        toolBar.barTintColor = UIColor(hex: "#195F92")
        let btnDone = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.resignKeyboard))
        let btnSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        btnDone.tintColor = UIColor.white
        toolBar.items = [btnSpace, btnDone]
        textField.inputAccessoryView = toolBar
    }
    
    func resignKeyboard() {
        self.view.endEditing(true)
        self.dismissPicker()
    }
    // MARK: - Picker View
    
    func openPickerView() {
        
        customPickerView.center = CGPoint(x: 160, y: 1100)
        customPickerView.backgroundColor = UIColor.lightGray
        if customPickerView != nil {
            customPickerView.removeFromSuperview()
            customPickerView = nil
        }
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        UIView.commitAnimations()
        let iOSDeviceScreenSize = UIScreen.main.bounds.size
        if iOSDeviceScreenSize.height == 480 {
            customPickerView = UIView(frame: CGRect(x: 0, y: iOSDeviceScreenSize.height - 180, width: iOSDeviceScreenSize.width, height: 180))
        }
        else {
            customPickerView = UIView(frame: CGRect(x: 0, y: iOSDeviceScreenSize.height - 180, width: iOSDeviceScreenSize.width, height: 180))
        }
        self.view.addSubview(customPickerView)
        //Adding toolbar
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: iOSDeviceScreenSize.width, height: 44))
        toolBar.sizeToFit()
        toolBar.isTranslucent = false
        toolBar.barTintColor = UIColor(hex: "#195F92")
        let btnDone = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.dismissPicker))
        let btnSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        btnDone.tintColor = UIColor.white
        toolBar.items = [btnSpace, btnDone]
        customPickerView.addSubview(toolBar)
        if iOSDeviceScreenSize.height == 480 {
            pickerView = UIPickerView(frame: CGRect(x: 0, y: 44, width: iOSDeviceScreenSize.width, height: 136))
        }
        else {
            pickerView = UIPickerView(frame: CGRect(x: 0, y: 44, width: iOSDeviceScreenSize.width, height: 136))
        }
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.backgroundColor = UIColor.white
        pickerView.showsSelectionIndicator = true
        customPickerView.addSubview(pickerView)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        UIView.commitAnimations()
        self.view.bringSubview(toFront: customPickerView)
    }
    func dismissPicker() {
       
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        customPickerView.center = CGPoint(x: 160, y: 3000)
        UIView.commitAnimations()
    }
   
    // MARK: - Picker Delegate Methods
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrSex.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return arrSex.object(at: row) as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        txtGender.text = arrSex.object(at: row) as? String
        
    }
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        let sectionWidth = 200
        return CGFloat(sectionWidth)
    }
    
}
