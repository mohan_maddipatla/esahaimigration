//
//  PromoDetailViewController.swift
//  eSahai
//
//  Created by PINKY on 30/03/17.
//  Copyright © 2017 TechVedika. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit

class PromoDetailViewController: UIViewController {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet var superView: UIView!
    @IBOutlet weak var PromoImageView: UIImageView!
   
    var promodetailsDict = NSDictionary()
    var isFromDidSelectRowPromoBanner = Bool()

    override func viewDidLoad() {
        super.viewDidLoad()

      // self.superView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        
        print (promodetailsDict)
        
        lblTitle.text = promodetailsDict.value(forKey:"title" ) as? String
        lblDescription.text = promodetailsDict.value(forKey: "description") as? String
        
       
        if isFromDidSelectRowPromoBanner
        {
            MBProgressHUD.showAdded(to: appDelegate.window, animated: true)
            isFromDidSelectRowPromoBanner = false
            LazyImage.show(imageView:PromoImageView, url:promodetailsDict.value(forKey: "banner_path") as? String) {
                () in
                
                MBProgressHUD.hide(for: appDelegate.window, animated: true)
                //Lazy loading complete. Do something..
            }
        }
        else
        {
            LazyImage.show(imageView:PromoImageView, url:promodetailsDict.value(forKey: "bannerPath") as? String) {
                () in
                
                //MBProgressHUD.hide(for: appDelegate.window, animated: true)
                //Lazy loading complete. Do something..
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func ButtonCloseAction(_ sender: Any) {
        FBSDKAppEvents.logEvent("Promo Banner viewed")
        self.dismiss(animated: true, completion: nil)
    }
}
