//
//  DetailedEmergiencyInfoViewController.swift
//  eSahai
//
//  Created by UshaRao on 12/7/16.
//  Copyright © 2016 TechVedika. All rights reserved.
//

import UIKit

class DetailedEmergiencyInfoViewController: BaseViewController {

    var dictDetails = NSDictionary()
    
    @IBOutlet var lblTripCost: UILabel!
    @IBOutlet var lblAvgSpeed: UILabel!
    @IBOutlet var lblTripTime: UILabel!
    @IBOutlet var lblTotalDistance: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if( (dictDetails.value(forKey: "distance")as? String) == ""){
            lblTotalDistance.text = "0.00 Kms"
        }else{
            if var totalDistance : Float = Float((dictDetails.value(forKey: "distance")as? String)!){
                totalDistance = totalDistance/1000.0
                lblTotalDistance.text = String(format : "%.2f Kms",totalDistance)
            }
        }
        
        if((dictDetails.value(forKey: "duration") as! String) == ""){
            lblTripTime.text = "00:00:00"
        }else{
            let tripDuration : TimeInterval = TimeInterval(dictDetails.value(forKey: "duration") as! String)!
            
            lblTripTime.text = self.stringFromTimeInterval(interval: tripDuration) as String
        }
        
       // if ( dictDetails.value(forKey: "speed") ){
        if(dictDetails.value(forKey: "speed") as? String == ""){
            lblAvgSpeed.text = "0.0 Kmph"
        }else if let val = dictDetails["speed"] {
            lblAvgSpeed.text = dictDetails.value(forKey: "speed") as? String
        }else{
             lblAvgSpeed.text = "0.0 Kmph"
        }
        
        
        if((dictDetails.value(forKey: "cost") as? String) == ""){
            lblTripCost.text = "₹0.00"
        }else{
             lblTripCost.text = String(format: "₹%@",(dictDetails.value(forKey: "cost") as? String)!)
        }
       
        self.title = "Trip Details"
        
        let newBackButton = UIBarButtonItem(title: "<", style: UIBarButtonItemStyle.plain, target: self, action: #selector(DetailedEmergiencyInfoViewController.goBack))
        self.navigationItem.leftBarButtonItem = newBackButton
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func stringFromTimeInterval(interval: TimeInterval) -> NSString {
        
        var ti = NSInteger(interval)
         ti = ti/1000
        let seconds = ti % 60
        let minutes = (ti / 60) % 60
        let hours = (ti / 3600)
        
        return NSString(format:"%0.2d:%0.2d:%0.2d",hours,minutes,seconds)
    }
    @IBAction func btnDone(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func goBack() {
        self.navigationController?.popViewController(animated: true);
    }
    
    
}
