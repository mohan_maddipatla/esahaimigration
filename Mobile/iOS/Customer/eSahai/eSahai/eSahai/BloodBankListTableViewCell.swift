//
//  BloodBankListTableViewCell.swift
//  eSahai
//
//  Created by eSahai on 20/05/17.
//  Copyright © 2017 TechVedika. All rights reserved.
//

import UIKit

class BloodBankListTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    
    @IBOutlet weak var phNum1: UIButton!
    
    @IBOutlet weak var phNum2: UIButton!
    
    @IBOutlet weak var addressLbl: UILabel!
    
    @IBOutlet weak var webLinkBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
