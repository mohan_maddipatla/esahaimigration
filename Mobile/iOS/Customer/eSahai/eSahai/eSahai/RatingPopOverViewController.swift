//
//  RatingPopOverViewController.swift
//  eSahai
//
//  Created by UshaRao on 12/8/16.
//  Copyright © 2016 TechVedika. All rights reserved.
//

import UIKit

class RatingPopOverViewController: BaseViewController {

    @IBOutlet var txtComment: UITextField!
    @IBOutlet var cosmosViewObj: CosmosView!
    
    @IBOutlet weak var popCancelBtn: UIButton!
    var del : delegatePopOver?

    
    var bookingID = String()
    var IsFromRATEClicked = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true;
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - Textfield Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return textField.resignFirstResponder()
    }


    @IBAction func btnCancelClicked(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
        //self.navigationController?.popViewController(animated: true)
        //let DashCon  = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        //self.navigationController?.pushViewController(DashCon, animated: true)
        //self.p(animated:true,completion:nil)
        //RatingPopOverViewController.isHidden = true
       // RatingPopOverViewController.accessibilityElementsHidden()
       // self.dismiss(animated: true, completion: nil)
        //self.navigationController?.popViewController(animated: true)

        //vc.timer.invalidate()
        //vc.dismiss(animated: true, completion: nil)
        //self.MovetoDashBoard()
       // self.dismiss(animated: true, completion: nil)

    }
    // MARK: - Slide Navigation
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
    func MovetoDashBoard() {
      //  self.timer.invalidate()
       // UserDefaults.standard.setValue(false, forKey:"isTrackingOn")
        
        ///print("\(self.navigationController?.viewControllers)")
        
        //  for VC: UIViewController in (self.navigationController?.viewControllers)!
//        for VC: UIViewController in self.navigationController!.viewControllers {
//            if (VC is DashboardViewController) {
//                self.navigationController!.popToViewController(VC, animated: true)
//                return
//            }
//        }
//        
        let signIn = self.storyboard?.instantiateViewController(withIdentifier:"DashboardViewController") as! DashboardViewController
        self.navigationController?.pushViewController(signIn, animated: false)
        
        return
        
    }
    
   
    
    @IBAction func btnSubmitClicked(_ sender: AnyObject) {
        
        if cosmosViewObj.rating == 0 {
           self.view.makeToast("Please give your rating", duration:kToastDuration, position:CSToastPositionCenter)
            return
        }
        
        self.sendFeedBack(comment: txtComment.text! , rating: cosmosViewObj.rating)
        
    }
    
    func sendFeedBack (comment: String, rating: Double){
        
        let  strUrl = String(format : "%@%@",kServerUrl,"addfeedback") as NSString
        
        if IsFromRATEClicked {
            IsFromRATEClicked = false
        }
        else{
            bookingID = UserDefaults.standard.value(forKey: "bookingid") as! String
        }
        let dictParams = ["customerApp": "true",
                          kCustomer_id:UserDefaults.standard.value(forKey: kCustomer_id)as! String,
                          "customer_mobile":UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String,
                          "comment" : comment,
                          "points":rating,
                          "booking_id":bookingID] as NSDictionary
        
        let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
        
        sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in
            DispatchQueue.main.async()
                {
                    let strStatusCode = result["statusCode"] as? String
                    
                    if strStatusCode == "300"
                    {
                        self.del?.updateTable()
                        self.dismiss(animated: true, completion: nil)
                    }
                    else if strStatusCode == "204"{
                        self.logout()
                    }
                    else if strStatusCode == "500"{
                        return
                    }
                    else if(strStatusCode != "500"){
                    self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                    }
            }
        }, failureHandler: {(error) in
        })
    }
    
}
