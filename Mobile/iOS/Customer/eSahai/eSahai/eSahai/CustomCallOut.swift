//
//  CustomCallOut.swift
//  eSahai
//
//  Created by PINKY on 17/04/17.
//  Copyright © 2017 TechVedika. All rights reserved.
//

import UIKit

class CustomCallOut: UIView{
    @IBOutlet weak var HospitalName: UILabel!
    @IBOutlet weak var Attr1: UILabel!
    @IBOutlet weak var Attr2: UILabel!
    @IBOutlet weak var Attr3: UILabel!
    @IBOutlet weak var selectLbl: UILabel!
    @IBOutlet weak var btnClickable: UIButton!
}

/*class CustomCallOut: UIView,UITableViewDelegate,UITableViewDataSource {
    
    
    //@IBOutlet weak var hospitalName: UILabel!
    
    @IBOutlet weak var attrTblView: UITableView!
    @IBOutlet weak var viewBtn: UILabel!
    
    @IBOutlet weak var btnClickable: UIButton!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    var hospitalName = String()
    
    var attributeDetails = NSArray()
    
    /*    attributes =     (
     {
     "attribute_details" = 3;
     "attribute_name" = Rating;
     id = 19;
     },
     {
     "attribute_details" = 10;
     "attribute_name" = "No. of Beds";
     id = 20;
     },
     {
     "attribute_details" = private;
     "attribute_name" = "Hospital Type";
     id = 21;
     }
     );*/
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return hospitalName
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20.0
    }
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return attributeDetails.count
    }
    
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell? ?? UITableViewCell(style: .default, reuseIdentifier: "cell")
        
        cell.textLabel?.text = "\((attributeDetails.object(at: indexPath.row) as! NSDictionary).value(forKey:"attribute_name") as! NSString) : \((attributeDetails.object(at: indexPath.row) as! NSDictionary).value(forKey:"attribute_details") as! NSString)"
        cell.textLabel?.numberOfLines = 0
        cell.selectionStyle = .none
       // cell.textLabel?.font = UIFont.systemFontOfSize(8.0)
        cell.textLabel?.sizeToFit()
        
        return cell
    }
    
   /* func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }*/
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}*/
