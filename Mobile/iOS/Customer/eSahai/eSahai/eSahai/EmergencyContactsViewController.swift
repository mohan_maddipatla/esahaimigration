//
//  EmergencyContactsViewController.swift
//  eSahai
//
//  Created by UshaRao on 11/9/16.
//  Copyright © 2016 TechVedika. All rights reserved.
//

import UIKit
import Contacts
import PhoneNumberKit


class AddEmergencyContactTableViewCell: UITableViewCell {
    
    @IBOutlet var lblNumber: UILabel!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblRelation: UILabel!
    
    @IBOutlet weak var btnNotify_outlet: UIButton!
    @IBOutlet weak var btnDelete_outlet: UIButton!
    @IBOutlet weak var btnEdit_outlet: UIButton!
}

class EmergencyContactsViewController: BaseViewController,UIPopoverPresentationControllerDelegate,UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate,AddContactViewControllerDelegate {
    
    @IBOutlet var tableViewObj: UITableView!
    var addEmergencyContactCell = AddEmergencyContactTableViewCell()
    var arrContacts = NSMutableArray()
    var getIndexPath = NSInteger()
    var sysContactDict = NSMutableDictionary()
    
    @IBOutlet weak var popOverView: UIScrollView!
    @IBOutlet var txtRelation: UITextField!
    @IBOutlet var txtNumber: UITextField!
    @IBOutlet var txtName: UITextField!
    var isAddContacts = Bool()
    var tempDict1 = NSMutableDictionary()
    
    @IBOutlet weak var countryCode: UITextField!
   
    @IBOutlet weak var isNotify: UIButton!
    
    var arrCountryNames = NSArray()
        //UserDefaults.standard.value(forKey: kCountryNames
     //   ) as! NSArray
    var arrCountryList = NSArray()
    
    var customPickerView : UIView! = UIView()
    var pickerView : UIPickerView = UIPickerView()
    let iOSDeviceScreenSize = UIScreen.main.bounds.size
    
    @IBOutlet weak var txtcountryNames: UITextField!
    @IBOutlet weak var labelDetails: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Emergency Contacts"
        self.getEmergencyContactList()
        tableViewObj.tableFooterView = UIView(frame:CGRect.zero)
        popOverView.isHidden = true
        popOverView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        
        let screenSize: CGRect = UIScreen.main.bounds

        popOverView.contentSize = CGSize(width: screenSize.width, height: screenSize.height)
        
        self.arrCountryNames = UserDefaults.standard.value(forKey: kCountryNames) as! NSArray
        self.arrCountryList = UserDefaults.standard.value(forKey: kCountryList) as! NSArray
        countryCode.text = ((self.arrCountryList.object(at: appDelegate.row) as! NSDictionary ).value(forKey: "code")as! String)
        
        txtcountryNames.text = ((self.arrCountryList.object(at: appDelegate.row) as! NSDictionary ).value(forKey: "country")as! String)


    }

    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = false;
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
        
            self.addToolBar(txtNumber)
    }
    func addToolBar(_ textField: UITextField) {
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        toolBar.isTranslucent = false
        toolBar.barTintColor = UIColor(hex: "#195F92")
        let btnDone = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClicked))
        let btnSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        btnDone.tintColor = UIColor.white
        toolBar.items = [btnSpace, btnDone]
        textField.inputAccessoryView = toolBar
    }
    func doneClicked(){
        
        txtNumber.resignFirstResponder()
    }

    // MARK: - Slide Navigation
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
    
    func getEmergencyContactList()
    {
        let  strUrl = String(format : "%@%@",kServerUrl,"getEmergencyContactsForCustomer") as NSString
        
        let dictParams = ["iOS":"true","customerApp": "true","customer_mobile":UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String,"customerId":UserDefaults.standard.value(forKey: kCustomer_id)as! String, "ambulanceApp": false
] as NSDictionary
        print(dictParams)
        
        let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
        print(dictHeaders)

        
        sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in
            DispatchQueue.main.async()
                {
                    let strStatusCode = result["statusCode"] as? String
                    
                    if strStatusCode == "300"
                    {
                        self.arrContacts = result.value(forKey: "responseData") as! NSMutableArray
                        print(self.arrContacts)
                        self.tableViewObj.reloadData()
                    }
                    else if strStatusCode == "204"{
                        self.logout()
                    }
                    else if(strStatusCode != "500"){
                        self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                    }
            }
            }, failureHandler: {(error) in
        })
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        if(arrContacts.count > 0){
            labelDetails.isHidden = true
        }else{
            labelDetails.isHidden = false
        }
        return arrContacts.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
         addEmergencyContactCell = tableView.dequeueReusableCell(withIdentifier: "AddEmergencyContactTableViewCell") as! AddEmergencyContactTableViewCell
        let tempDict = arrContacts.object(at: indexPath.row) as! NSDictionary
        
        addEmergencyContactCell.lblName.text = tempDict.value(forKey: "contact_person") as! String?
        
        var phoneNum = tempDict.value(forKey: "mobile_number") as! String?
       phoneNum = phoneNum?.replacingOccurrences(of: "(", with: "")
        
       phoneNum =  phoneNum?.replacingOccurrences(of: ")", with: "")
        
            addEmergencyContactCell.lblNumber.text = "\(phoneNum!)"
       
        addEmergencyContactCell.lblRelation.text = tempDict.value(forKey: "relation") as! String?
        
        addEmergencyContactCell.btnEdit_outlet.tag = indexPath.row
        addEmergencyContactCell.btnDelete_outlet.tag = indexPath.row
        
        if ( tempDict.value(forKey: "is_notify") as! String? == "1")  {
            addEmergencyContactCell.btnNotify_outlet.isSelected = true
            let image = UIImage(named: "selected.png")! as UIImage
            addEmergencyContactCell.btnNotify_outlet.setBackgroundImage(image, for: UIControlState.normal)
        }else {
            addEmergencyContactCell.btnNotify_outlet.isSelected = false
            let image = UIImage(named: "unselected.png")! as UIImage
            addEmergencyContactCell.btnNotify_outlet.setBackgroundImage(image, for: UIControlState.normal)
        }
        
        return addEmergencyContactCell
        
    }
    
     func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool{
        return false
    }
    
    func addTapped() {
        
        /* Supports UIAlert Controller */
    
        if( controllerAvailable()){
            handleIOS8()
        } else {
            var actionSheet:UIActionSheet
            actionSheet = UIActionSheet(title: "", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil,otherButtonTitles:"New Contact", "Add from Contacts")
            actionSheet.delegate = self
            actionSheet.show(in: self.view)
            /* Implement the delegate for actionSheet */
        }
        
    }
    
    
    func handleIOS8(){
        let alert = UIAlertController(title:"Add Emergency Contacts", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let newContact = UIAlertAction(title: "New Contact", style: UIAlertActionStyle.default) { (alert) -> Void in
            self.btnAddEmergencyContactsClicked(isSystemContacts: false)
        }
        let systemContact = UIAlertAction(title: "Add from Contacts", style: UIAlertActionStyle.default) { (alert) -> Void in
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SystemContactsViewController") as! SystemContactsViewController
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
            //self.navigationController?.pushViewController(vc, animated: true)
        }
        
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (alert) -> Void in
        }
        
        alert.addAction(newContact)
        alert.addAction(systemContact)
        alert.addAction(cancelButton)
        
        self.present(alert, animated: false, completion: nil)
    }
    
    func btnAddEmergencyContactsClicked( isSystemContacts : Bool) {
        
        self.clearUI()
        popOverView.isHidden = false
        isAddContacts = true
        self.arrCountryNames = UserDefaults.standard.value(forKey: kCountryNames) as! NSArray
        self.arrCountryList = UserDefaults.standard.value(forKey: kCountryList) as! NSArray
        countryCode.text = ((self.arrCountryList.object(at: appDelegate.row) as! NSDictionary ).value(forKey: "code")as! String)
        
        txtcountryNames.text = ((self.arrCountryList.object(at: appDelegate.row) as! NSDictionary ).value(forKey: "country")as! String)

        if(isSystemContacts){
            txtName.text = sysContactDict.value(forKey: "name") as! String?
            txtNumber.text = (sysContactDict.value(forKey: "contact") as! String?)?.replacingOccurrences(of: " ", with: "")
            
            
            var countrycoderow = Int()

            for i in 0..<arrCountryList.count
            {
                print("+\(sysContactDict.value(forKey: "code") as! String)")
                print(i)
                
                if (((arrCountryList.object(at: i) as! NSDictionary).value(forKey: "code")as! String) == "+\(sysContactDict.value(forKey: "code")as! String)")
                {
                    countrycoderow = i
                    break
                }
            }
            
            
            //txtNumber.text = tempDict1.value(forKey: "mobile_number") as! String?
            //txtRelation.text = tempDict1.value(forKey: "relation") as! String?
            
            countryCode.text = ((self.arrCountryList.object(at: countrycoderow) as! NSDictionary ).value(forKey: "code")as! String)
            
            txtcountryNames.text = ((self.arrCountryList.object(at: countrycoderow) as! NSDictionary).value(forKey: "country")as! String)
        }
    }
   
    @IBAction func btnEditAction(sender : UIButton!) {
        
        self.clearUI()
        isAddContacts = false
        tempDict1 = arrContacts.object(at: sender.tag) as! NSMutableDictionary
        popOverView.isHidden = false
        
        txtName.text = tempDict1.value(forKey: "contact_person") as! String?
        
        
        

        var mobNum : String = (tempDict1.value(forKey: "mobile_number") as! String)
        mobNum = mobNum.replacingOccurrences(of: "(", with: "")
        mobNum = mobNum.replacingOccurrences(of: ")", with: "")
        
        var token = mobNum.components(separatedBy: "-")
        
        txtNumber.text = token[1]
        
        for i in 0..<arrCountryList.count {
            
            if ( ((arrCountryList.object(at: i) as! NSDictionary ).value(forKey: "code")as! String)  == "+\(token[0])") {
            
                self.appDelegate.row = i
            }
        }
        
        
        //txtNumber.text = tempDict1.value(forKey: "mobile_number") as! String?
        txtRelation.text = tempDict1.value(forKey: "relation") as! String?
        
        countryCode.text = ((self.arrCountryList.object(at: appDelegate.row) as! NSDictionary ).value(forKey: "code")as! String)
        
        txtcountryNames.text = ((self.arrCountryList.object(at: appDelegate.row) as! NSDictionary ).value(forKey: "country")as! String)
        
        
        if ( tempDict1["is_notify"] as? String == "1")  {
            let image = UIImage(named: "selected.png")! as UIImage
            isNotify.setBackgroundImage(image, for: UIControlState.normal)
            isNotify.isSelected = true
        }else {
            let image = UIImage(named: "unselected.png")! as UIImage
            isNotify.setBackgroundImage(image, for: UIControlState.normal)
            isNotify.isSelected = false
        }
    }
    
    @IBAction func btnDeleteAction(sender : UIButton!) {
        
        let alertController = UIAlertController(title: "", message: "Are you sure you want to Delete the Contact?", preferredStyle: UIAlertControllerStyle.alert)
        let DestructiveAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            
        }
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.destructive) { (result : UIAlertAction) -> Void in
            
            let  strUrl = String(format : "%@%@",kServerUrl,"deleteEmergencyContactForCustomer") as NSString
            
            let tempDict = NSMutableDictionary()
            
            tempDict.setValue(((self.arrContacts.object(at:sender.tag) as! NSDictionary).value(forKey: "id") as! String), forKey: "id")
          
            let tempArray = NSMutableArray()
            tempArray.add(tempDict)
            
            
            let dictParams = ["customerApp": "true","customer_mobile":UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String,"emergecy_contacts" : tempArray ] as NSDictionary
            
            let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
            
            self.sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in
                DispatchQueue.main.async()
                    {
                        let strStatusCode = result["statusCode"] as? String
                        
                        if strStatusCode == "300"
                        {
                            self.getEmergencyContactList()
                            
                        }
                        else if strStatusCode == "204"{
                            self.logout()
                        }
                        else if(strStatusCode != "500"){
                            self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                        }
                }
            }, failureHandler: {(error) in
            })
        }
        alertController.addAction(DestructiveAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {
        if( buttonIndex == 1){
           self.btnAddEmergencyContactsClicked(isSystemContacts: false)
        } else if(buttonIndex == 2){
            self.btnAddEmergencyContactsClicked(isSystemContacts: true)
        }
    }
    
    func controllerAvailable() -> Bool {
        if let gotModernAlert: AnyClass = NSClassFromString("UIAlertController") {
            return true;
        }
        else {
            return false;
        }
        
    }
    
    
    // MARK: - Picker View
    func openPickerView() {
        customPickerView.center = CGPoint(x: 160, y: 1100)
        customPickerView.backgroundColor = UIColor.lightGray
        if customPickerView != nil {
            customPickerView.removeFromSuperview()
            customPickerView = nil
        }
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        UIView.commitAnimations()
        if iOSDeviceScreenSize.height == 480 {
            customPickerView = UIView(frame: CGRect(x: 0, y: iOSDeviceScreenSize.height - 180, width: iOSDeviceScreenSize.width, height: 180))
        }
        else {
            customPickerView = UIView(frame: CGRect(x: 0, y: iOSDeviceScreenSize.height - 180, width: iOSDeviceScreenSize.width, height: 180))
        }
        self.view.addSubview(customPickerView)
        //Adding toolbar
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: iOSDeviceScreenSize.width, height: 44))
        toolBar.sizeToFit()
        toolBar.isTranslucent = false
        toolBar.barTintColor = UIColor(hex: "#195F92")
        let btnDone = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.dismissPicker))
        let btnSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        btnDone.tintColor = UIColor.white
        toolBar.items = [btnSpace, btnDone]
        customPickerView.addSubview(toolBar)
        if iOSDeviceScreenSize.height == 480 {
            pickerView = UIPickerView(frame: CGRect(x: 0, y: 44, width: iOSDeviceScreenSize.width, height: 136))
        }
        else {
            pickerView = UIPickerView(frame: CGRect(x: 0, y: 44, width: iOSDeviceScreenSize.width, height: 136))
        }
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.backgroundColor = UIColor.white
        pickerView.showsSelectionIndicator = true
        customPickerView.addSubview(pickerView)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        UIView.commitAnimations()
        self.view.bringSubview(toFront: customPickerView)
        }
    
    func dismissPicker() {
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        customPickerView.center = CGPoint(x: 160, y: 3000)
        UIView.commitAnimations()
    }
    
    // MARK: - Picker Delegate Methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrCountryNames.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return arrCountryNames.object(at: row) as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        appDelegate.row = row
        txtcountryNames.text = arrCountryNames.object(at: row) as? String
        
        countryCode.text = ((arrCountryList.object(at: row) as! NSDictionary ).value(forKey: "code")as! String)
    }
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        let sectionWidth = 200
        return CGFloat(sectionWidth)
    }
    
    
    // MARK: - Textfield Delegate Methods
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        if textField == countryCode {
           // self.openPickerView()
            return false
        } else if textField == txtcountryNames {
            self.view.endEditing(true)
            self.openPickerView()
            pickerView.selectRow(appDelegate.row, inComponent: 0, animated: false)
            return false
        }
        self.dismissPicker()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
    
        if textField == txtNumber {
            let oldLength = textField.text?.characters.count
            let replacementLength = string.characters.count
            let rangeLength = range.length
            let newLength = oldLength! - rangeLength + replacementLength
            return newLength <= 10
        }
        else {
            return true
        }
    }
    
    @IBAction func btnCancel(_ sender: AnyObject) {
        
        popOverView.endEditing(true)
        popOverView.isHidden = true
    }
    
    
    @IBAction func btnSubmit(_ sender: AnyObject) {
    
        if (txtName.text?.isEmpty)! || (txtNumber.text?.isEmpty)! || (txtRelation.text?.isEmpty)! {
            self.view.makeToast("Please fill all the details", duration:kToastDuration, position:CSToastPositionCenter)
            return
        }
        self.btnSubmitClicked()
        self.dismiss(animated: true, completion: nil)
        popOverView.endEditing(true)

    }
    
    @IBAction func NotifyRelation(_ sender: AnyObject) {
    
        if isNotify.isSelected {
            let image = UIImage(named: "unselected.png")! as UIImage
            isNotify.setBackgroundImage(image, for: UIControlState.normal)
            isNotify.isSelected = false
        }else{
            let image = UIImage(named: "selected.png")! as UIImage
            isNotify.setBackgroundImage(image, for: UIControlState.normal)
            isNotify.isSelected = true
        }
    }
    
    func btnSubmitClicked()
    {
        
        var code = countryCode.text! as String
       // code = code.replacingOccurrences(of: "+", with: "")
        code = "\(code)-\(txtNumber.text!)"
        print (code)
    
        let dictList = [kCustomer_id:UserDefaults.standard.value(forKey: kCustomer_id)as! String,"contact_person":txtName.text! as String,"mobile_number": code ,"relation":txtRelation.text! as String,"is_notify" : isNotify.isSelected] as NSMutableDictionary
        var  strUrl = NSString()
    
        if(isAddContacts){
            strUrl = String(format : "%@%@",kServerUrl,"addEmergencyContactForCustomer") as NSString
        }else{
            strUrl = String(format : "%@%@",kServerUrl,"updateEmergencyContactForCustomer") as NSString
            dictList.setValue( tempDict1.value(forKey: "id") as! String?, forKey: "id")
        }
    
        let tempArr = NSMutableArray()
        tempArr.add(dictList)
    
        let dictParams = ["customerApp": "true","customer_mobile":UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String,"emergecy_contacts": tempArr ] as NSDictionary
    
        let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
    
        
        sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in
            DispatchQueue.main.async()
                {
                    let strStatusCode = result["statusCode"] as? String
    
                    if strStatusCode == "300"
                    {
                        self.popOverView.isHidden = true
                        self.isAddContacts = false
                        self.sysContactDict.removeAllObjects()
                        self.getEmergencyContactList()
                    }
                    else if strStatusCode == "204"{
                        self.logout()
                    }
                    else
                    {
                    self.appDelegate.window?.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                        return
                    }
                }
            }, failureHandler: {(error) in})
    }

    
    func clearUI(){
        txtName.text = ""
        txtRelation.text = ""
        txtNumber.text = ""
        isNotify.isSelected = false
        let image = UIImage(named: "unselected.png")! as UIImage
        isNotify.setBackgroundImage(image, for: UIControlState.normal)
    }
    
    func didFetchContacts(_ contacts: [CNContact]) {
      //  var a = String()
        let phoneNumberKit = PhoneNumberKit()

        if (contacts[0].isKeyAvailable(CNContactPhoneNumbersKey)) {
            for phoneNumber1:CNLabeledValue in contacts[0].phoneNumbers {
                do {
                  let phoneNumber = try phoneNumberKit.parse((phoneNumber1.value).stringValue)
                    sysContactDict.setValue(String(phoneNumber.nationalNumber) , forKey: "contact")
                    sysContactDict.setValue( String(phoneNumber.countryCode), forKey: "code")
                        
                    print("code \(phoneNumber.nationalNumber), contact \(phoneNumber.countryCode)")
                }
                catch {
                    print("Generic parser error")
                }
              
                
               // a = (phoneNumber.value).stringValue
                //a = a.replacingOccurrences(of: "-", with: "")
            }
        }
        sysContactDict.setValue(CNContactFormatter.string(from: contacts[0], style: .fullName)!, forKey: "name")
       
        btnAddEmergencyContactsClicked(isSystemContacts: true)
    }
    
    @IBAction func DropDownAction(_ sender: Any) {
        self.view.endEditing(true)
        self.openPickerView()
        pickerView.selectRow(appDelegate.row, inComponent: 0, animated: false)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch:UITouch = touches.first!
        if touch.view == popOverView {
            if !(popOverView.isHidden){
                popOverView.isHidden = true
            }
        }
    }
}
