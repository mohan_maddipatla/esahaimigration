//
//  CustomDelegate.swift
//  eSahai
//
//  Created by PINKY on 21/02/17.
//  Copyright © 2017 TechVedika. All rights reserved.
//

import Foundation



protocol LocationDelegate{
    func getUserLocation(dict : NSDictionary)
    func getHospitalLocation(dict : NSDictionary)
    func getHospitalLocationDict(dict : NSDictionary)
}

protocol delegatePopOver {
    func updateTable()
}

protocol delegateUpdate {
    func updateData()
}
