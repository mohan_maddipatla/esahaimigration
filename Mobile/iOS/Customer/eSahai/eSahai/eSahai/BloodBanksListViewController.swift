//
//  BloodBanksListViewController.swift
//  eSahai
//
//  Created by eSahai on 20/05/17.
//  Copyright © 2017 TechVedika. All rights reserved.
//

import UIKit
struct objects {
    var SectionName: String!
    var SectionObjects: [String]!
    var SectionAddressObjects: [String]!
    var SectionwebObjects: [String]!
    var SectioPhone1Objects: [String]!
    var SectioPhone2Objects: [String]!
    
    
    
    
    init(SectionName: String, SectionObjects: [String],SectionAddressObjects:[String],SectionwebObjects:[String],SectioPhone1Objects:[String],SectioPhone2Objects:[String]) {
        self.SectionName = SectionName
        self.SectionObjects = SectionObjects
        self.SectionAddressObjects = SectionAddressObjects
        self.SectionwebObjects = SectionwebObjects
        self.SectioPhone1Objects = SectioPhone1Objects
        self.SectioPhone2Objects = SectioPhone2Objects
    }
}


class BloodBanksListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var objectsArray = [objects]()

    @IBOutlet weak var tableObj: UITableView!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bloodBankDetails()
        
        CleverTap.sharedInstance()?.recordEvent("iOS_BloodBanksList")

        
        tableObj.rowHeight = UITableViewAutomaticDimension
        tableObj.estimatedRowHeight = 200
        
        //tableObj.setNeedsLayout()
        //tableObj.layoutIfNeeded()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func bloodBankDetails(){
        
        objectsArray = [objects(SectionName:"Ameerpet",SectionObjects:["Challa Hospital BLOOD BANK"],SectionAddressObjects:["7-1-71/A/1, Dharam Karam Road, Ameerpet, Hyderabad, Telangana 500016"], SectionwebObjects: ["http://www.challahospital.com/"], SectioPhone1Objects: ["+914042417760"], SectioPhone2Objects: ["null"]),
                        objects(SectionName:"A S Rao Nagar",SectionObjects:["Asian Blood Bank Of Asian Health Foundation"], SectionAddressObjects: ["Plot No 2 Shanthi Surabhi Complex, Main Road, A S Rao Nagar, Hyderabad,Telangana - 500762"], SectionwebObjects: ["NA"], SectioPhone1Objects: ["+914027162146"], SectioPhone2Objects: ["+919866192122"]),
                        objects(SectionName:"Balanagar",SectionObjects:["M M Voluntary Blood Bank"], SectionAddressObjects: ["Ferozeguda, 4-194/4, Balanagar, Beside B B R Hospital, Balanagar, Hyderabad,Telangana-500042"], SectionwebObjects: ["NA"], SectioPhone1Objects: ["+914023774469"], SectioPhone2Objects: ["+910466194469"]),
                        objects(SectionName:"Banjara Hills",SectionObjects:["Ntr Memorial Trust"], SectionAddressObjects: ["8-2-120/Nbtr/2, Road No 2, Banjara Hills, Hyderabad,Telangana- 500034, Behind TDP Party Office"], SectionwebObjects: ["http://ntrtrust.org/"], SectioPhone1Objects: ["+914030799999"], SectioPhone2Objects: ["null"]),
                        objects(SectionName:"Barkatpura",SectionObjects:["Mythri Charitable Turst Blood Bank"], SectionAddressObjects: ["3-4-808, Barkatpura, Hyderabad,Telangana-500027"], SectionwebObjects: ["http://www.mythribloodbank.org/mythri_charitable.html"], SectioPhone1Objects: ["+914027550238"], SectioPhone2Objects: ["+914026822271"]),
                        objects(SectionName:"Dilsukhnagar",SectionObjects:["Rajyalaxmi Charitable Trust Blood Bank"], SectionAddressObjects: ["VR Colony, Kamala Nagar, Dilsukhnagar, Hyderabad,Telangana-500060"], SectionwebObjects: ["www.rajyalakshmichatitabletrustbloodbank.in"], SectioPhone1Objects: [" +919666663811"], SectioPhone2Objects: ["null"]),
                        objects(SectionName:"Himayathnagar",SectionObjects:["Rudhira Voluntary Blood Bank"], SectionAddressObjects: ["3-6-10/A 1st Floor, Main Road, Anasurya Commercial Complex, Himayat Nagar"], SectionwebObjects: ["https://www.healthfrog.in/laboratory/rudhira-voluntary-blood-bank-hyderabad-telangana-25_3l9q_f.html"], SectioPhone1Objects: ["+914023220222"], SectioPhone2Objects: ["+914465883770"]),
                        objects(SectionName:"Jubilee Hills",SectionObjects:["Chiranjeevi Eye & Blood Bank"], SectionAddressObjects: ["No.8-2-293/82/A/C.E.B.B, Road Number 1, Jubilee Hills, Hyderabad, Telangana-500033"], SectionwebObjects: ["NA"], SectioPhone1Objects: ["+914023555005"], SectioPhone2Objects: ["null"]),
                        objects(SectionName:"Koti",SectionObjects:["Bhanji Kheraji Blood Bank","Lions club of hyd  blood bank"], SectionAddressObjects: ["Inside feroze gandhi park, opp sbi head office, bank street , koti, Hyderabad, Telangana 500095","Door No 4-4-248, Koti, , Near Government Ent Hospital,Hyderabad - 500095"], SectionwebObjects: ["NA","NA"], SectioPhone1Objects: ["+914024745243","+91402474 5243"], SectioPhone2Objects: ["null","null"]),
                        objects(SectionName:"Kukatpally",SectionObjects:["Janani Voluntary Blood Bank"], SectionAddressObjects: ["G.P Rao Enclave, EWS 58/A, 1st Floor, Road Number 3, Kukatpally, Hyderabad, Telangana-500072"], SectionwebObjects: ["http://www.jananivoluntary.org/"], SectioPhone1Objects: ["+91402315 0496"], SectioPhone2Objects: ["null"]),
                        objects(SectionName:"LB Nagar",SectionObjects:["Kamineni Blood Bank"], SectionAddressObjects: ["L B Nagar, Hyderabad,Telangana - 500036"], SectionwebObjects: ["www.kaminenihospitals.com"], SectioPhone1Objects: ["+914039879999"], SectioPhone2Objects: ["+914039879298"]),
                        objects(SectionName:"Musheerabad",SectionObjects:["Gandhi Hospital"], SectionAddressObjects: ["Main Road, Musheerabad, Hyderabad,Telangana - 500048"], SectionwebObjects: ["www.gandhihospital.in"], SectioPhone1Objects: ["+914027505566"], SectioPhone2Objects: ["+914027510051"]),
                        objects(SectionName:"Nayapul",SectionObjects:["Govt Maternity Hospital Nayapool"], SectionAddressObjects: ["Ameen Bagh, Nayapul, Hyderabad,Telangana - 500002"], SectionwebObjects: ["NA"], SectioPhone1Objects: ["+914024523641"], SectioPhone2Objects: ["null"]),
                        objects(SectionName:"Panjagutta",SectionObjects:["Gene Blood Bank","Genetic Products India Ltd"], SectionAddressObjects: ["Kanthisikara Cmplx, Opp Model House, Panjagutta, Hyderabad - 500004","G Blk Kanthisikara complex, Punjagutta, Hyderabad - 500082"], SectionwebObjects: ["http://www.genebloodbank.in/Home?index.html","NA"], SectioPhone1Objects: ["+914023411658","+914023403882"], SectioPhone2Objects: ["null","null"]),
                        objects(SectionName:"Red Hills",SectionObjects:["Niloufer Hospital For Women & Children"], SectionAddressObjects: ["Red Hills, Hyderabad - 500004"], SectionwebObjects: ["http://www.nilouferhospital.com/"], SectioPhone1Objects: ["+914023314095"], SectioPhone2Objects: ["+914023394265"]),
                        objects(SectionName:"Regimental Bazar",SectionObjects:["Jeeva Dhara Voluntary Blood Bank"], SectionAddressObjects: ["9-4-269-275, 2nd Floor, Above Saluja Hospital, Opposite Railway Station, Behind Swathi tiffin center, Regimental Bazar, Secunderabad, Telangana-500025"], SectionwebObjects: ["jeevadhaarabloodbank.org"], SectioPhone1Objects: [" +91402770 7088"], SectioPhone2Objects: ["null"]),
                        objects(SectionName:"RTC X Roads",SectionObjects:["Sanjeevani Blood Bank"], SectionAddressObjects: ["1-1-79/A Bhagyanagar Complex, Rtc X Roads, Hyderabad,Telangana - 500020"], SectionwebObjects: ["http://www.sanjeeviniblooddonors.org/callus.php"], SectioPhone1Objects: ["+914065522343"], SectioPhone2Objects: ["null"]),
                        objects(SectionName:"S D Road",SectionObjects:["Social Service Blood Bank"], SectionAddressObjects: ["9-1-127/1/D/1, Adj To Madhava Nursing Home, Lane Opp St Marys Church, S D Road, Hyderabad,Telangana - 500003"], SectionwebObjects: ["NA"], SectioPhone1Objects: ["+919985409444,"], SectioPhone2Objects: ["null"]),
                        objects(SectionName:"Sultan Bazar",SectionObjects:["Lions Club Of Hyderabad"], SectionAddressObjects: ["4-4-248, Near Koti, Sultan Bazar, Hyderabad,Telangana - 500095"], SectionwebObjects: ["NA"], SectioPhone1Objects: ["+9124745243"], SectioPhone2Objects: ["null"]),
                        objects(SectionName:"Tarnaka",SectionObjects:["Sridevi blood bank"], SectionAddressObjects: ["207 2nd Flr Pavani Anasuya Towers, Opp Huda, Tarnaka, Hyderabad,Telangana - 500017"], SectionwebObjects: ["NA"], SectioPhone1Objects: ["+914027016016"], SectioPhone2Objects: ["null"]),
                        objects(SectionName:"Vidyanagar",SectionObjects:["Red Cross Blood Bank"], SectionAddressObjects: ["1-9-310, Street No 6, Near Spencers Supermarket, Vidya Nagar, Hyderabad, Telangana- 500044"], SectionwebObjects: ["http://redcrossbloodbank.site/"], SectioPhone1Objects: ["+914027633087"], SectioPhone2Objects: ["null"])
            
            
        ]
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! BloodBankListTableViewCell
        
        //cell.textLabel?.text = objectsArray[indexPath.section].SectionObjects[indexPath.row]
        
        cell.nameLbl.text = objectsArray[indexPath.section].SectionObjects[indexPath.row]
        
        cell.addressLbl.text = objectsArray[indexPath.section].SectionAddressObjects[indexPath.row]
       // cell.addressLbl.numberOfLines = 0
        cell.addressLbl.lineBreakMode = NSLineBreakMode.byWordWrapping
//        cell.addressLbl.sizeToFit()
//        cell.addressLbl.preferredMaxLayoutWidth = 1000
        
        
        //        label.numberOfLines = 4
        //        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        //        let font = UIFont(name: "Helvetica", size: 20.0)
        //        label.font = font
        //        label.text = "Whatever you want the text enter here"
        //        label.sizeToFit()
        
        
        cell.webLinkBtn.setTitle(objectsArray[indexPath.section].SectionwebObjects[indexPath.row], for: UIControlState.normal)
        cell.webLinkBtn.tag = indexPath.row
        
        cell.webLinkBtn.addTarget(self, action: #selector(BloodBanksListViewController
            .tappingWebLink), for: UIControlEvents.touchUpInside)
        cell.webLinkBtn.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        //First Phone Number Handling
        cell.phNum1 .setTitle(objectsArray[indexPath.section].SectioPhone1Objects[indexPath.row], for: UIControlState.normal)
        cell.phNum1.tag = indexPath.row
        cell.phNum1.addTarget(self, action: #selector(BloodBanksListViewController.phoneBtnTapping), for: UIControlEvents.touchUpInside)
        cell.phNum1.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        cell.phNum2.setTitle(objectsArray[indexPath.section].SectioPhone2Objects[indexPath.row], for: UIControlState.normal)
        //Second Phone Number Handling
        
        cell.phNum2.tag = indexPath.row
        cell.phNum2.addTarget(self, action: #selector(BloodBanksListViewController.phoneBtnSecTapping), for: UIControlEvents.touchUpInside)
        
        if (objectsArray[indexPath.section].SectioPhone2Objects[indexPath.row] == "null") {
            cell.phNum2.isHidden = true
            cell.phNum2.removeTarget(self, action: #selector(BloodBanksListViewController.phoneBtnTapping), for: UIControlEvents.touchUpInside)
        }
        else
        {
            
            cell.phNum2.isHidden = false
            //cell.phNum2.reversesTitleShadowWhenHighlighted = false

        }
        
        
        cell.addressLbl.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        
        
        //Weblink Handling
        
        /*
         if (objectsArray[indexPath.section].SectionwebObjects[indexPath.row] == "NA") {
         
         //cell.webTapping.isUserInteractionEnabled = false
         cell.webTapping.isEnabled = false
         print("isUserInteractionEnabled = false")
         
         }
         else
         {
         cell.webTapping.isUserInteractionEnabled = true
         print("isUserInteractionEnabled = true")
         cell.webTapping.isEnabled = true
         
         }
         
         */
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objectsArray[section].SectionObjects.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return objectsArray.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        //        view.tintColor = UIColor.red
        //        let header = view as! UITableViewHeaderFooterView
        //        header.textLabel?.textColor = UIColor.blue
        
        return objectsArray[section].SectionName
        
    }
    
    //    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    //    {
    //        let header = view as! UITableViewHeaderFooterView
    //        header.textLabel?.font = UIFont(name: "Futura", size: 11)
    //        header.textLabel?.textColor = UIColor.blue
    //        header.textLabel?.backgroundColor = UIColor.b
    //    }
    //    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //        //let view = UIView()
    //        //view.backgroundColor = UIColor.white
    //        //view.addSubview(view)
    //        let label = UILabel()
    //        label.text = objectsArray[section].SectionName
    //        label.backgroundColor = UIColor.blue
    //        view.addSubview(label)
    //        return view
    //    }
    //        func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    //    {
    //        let header = view as! UITableViewHeaderFooterView
    //        header.textLabel?.font = UIFont(name: "Futura", size: 11)
    //        header.textLabel?.textColor = UIColor.white
    //    }
    //MARK: Weblink Tapping
    
    func tappingWebLink(sender:UIButton){
       // var bloodWeb = BloodBankWebViewController()
        
        print("Tapping Web Link")
        
        let btnrow = sender.tag
        print("clicked row",btnrow)
        
        let button = sender as? UIButton
        let cell = button?.superview?.superview as? UITableViewCell
        let indexPath = tableObj.indexPath(for: cell!)
        print(indexPath?.row)
        
        
        let url = URL(string: objectsArray[(indexPath?.section)!].SectionwebObjects[(indexPath?.row)!])!
        
        
        //let requestURL = NSURL(string:objectsArray[(indexPath?.section)!].SectionwebObjects[(indexPath?.row)!])
        /*
         let request = NSURLRequest(url:url)
         bloodWeb.webObj.loadRequest(request as URLRequest)
         
         let HomeCon = self.storyboard?.instantiateViewController(withIdentifier:"BloodBankWebViewController")
         self.navigationController?.pushViewController(HomeCon!, animated: true)
         
         */
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            
        } else {
            UIApplication.shared.openURL(url)
            
        }
        
        
        
        
    }
    
    func tappingNAWebLink(sender:UIButton){
        print("Tapping NA Web Link")
        
        //webTapping.isUserInteractionEnabled = false
    }
    
    //MARK: Phone Number Tapping
    func phoneBtnTapping(sender:UIButton) {
        
        let phBtn = sender.tag
        print("Phone Number Row",phBtn)
        
        let button = sender as? UIButton
        let cell = button?.superview?.superview as? UITableViewCell
        let indexPath = tableObj.indexPath(for: cell!)
        print(indexPath?.row)
        //let button:UIButton = (sender as UIButton)
        //let indexPath:NSIndexPath = NSIndexPath(item: button.tag, section: 0)
        let phoneUrl = objectsArray[(indexPath?.section)!].SectioPhone1Objects[(indexPath?.row)!] //your url
        print("First Number",phoneUrl)
        if let url = URL(string: "tel://\(phoneUrl)"){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:])
            } else {
                // Fallback on earlier versions
            }
        }
        
    }
    
    //MARK: Phone Num Second Array Tapping
    
    func phoneBtnSecTapping (sender:UIButton) {
        let phSecBtnObj = sender.tag
        print("Phone NumberSecond Row",phSecBtnObj)
        let button = sender as? UIButton
        let cell = button?.superview?.superview as? UITableViewCell
        let indexPath = tableObj.indexPath(for: cell!)
        print(indexPath?.row)
        //let button:UIButton = (sender as UIButton)
        //let indexPath:NSIndexPath = NSIndexPath(item: button.tag, section: 0)
        let phoneSecUrl = objectsArray[(indexPath?.section)!].SectioPhone2Objects[(indexPath?.row)!]
        print("Second Number",phoneSecUrl)
        
        if let url = URL(string:"tel://\(phoneSecUrl)"){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:])
            } else {
                // Fallback on earlier versions
            }
        }
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
