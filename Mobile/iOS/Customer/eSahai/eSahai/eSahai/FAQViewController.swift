//
//  FAQViewController.swift
//  eSahai
//
//  Created by PINKY on 02/03/17.
//  Copyright © 2017 TechVedika. All rights reserved.
//

import UIKit

struct FAQ {
    var name: String!
    var items: [String]!
    var collapsed: Bool!
    
    init(name: String, items: [String], collapsed: Bool = true) {
        self.name = name
        self.items = items
        self.collapsed = collapsed
    }
}

class FAQViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    
   
    @IBOutlet weak var tblViewFAQ: UITableView!
    var arrFAQ = NSArray()
    var sections = [FAQ]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "FAQ's"
        self.getDetails()
    }
    
    func getDetails() {
        
        let  strUrl = String(format : "%@%@",kServerUrl,"faqList") as NSString
        
        let dictParams = ["customerApp": "true",
                          "ambulanceApp": "false",
                          "customer_mobile":UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String]
            as NSDictionary
        
        let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
       
        sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in DispatchQueue.main.async()
            {
                let strStatusCode = result["statusCode"] as? String
                if strStatusCode == "300" {
                    
                    print(result)
                    
                    self.arrFAQ = result["responseData"] as! NSArray
                    print(result["responseData"])
                    
                    self.setUpData()
                   // self.tblViewFAQ.reloadData()
                }
                else if strStatusCode == "204"{
                    self.logout()
                }
               else if(strStatusCode != "500"){
                    self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                }
            }
        }, failureHandler: {(error) in})
    }
    
    
    func setUpData(){
        
        for i in 0 ..< arrFAQ.count {
          sections.append( FAQ(name: (self.arrFAQ.object(at: i) as! NSDictionary).value(forKey: "question") as! String , items: [(self.arrFAQ.object(at: i) as! NSDictionary).value(forKey: "answer") as! String] ))
        }
        self.tblViewFAQ.reloadData()
    }
    
    // MARK: - Slide Navigation
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
}

/*extension FAQViewController {
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrFAQ.count
    }

     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell? ?? UITableViewCell(style: .default, reuseIdentifier: "cell")
        
      //  let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        let data = self.arrFAQ.object(at: indexPath.row) as! NSDictionary
        cell.textLabel?.attributedText = makeAttributedString(title: data.value(forKey: "question") as! String, subtitle: data.value(forKey: "answer") as! String)
        
        cell.textLabel?.numberOfLines = 0
        
        return cell
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
     func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    func makeAttributedString(title: String, subtitle: String) -> NSAttributedString {
        let titleAttributes = [NSFontAttributeName: UIFont.preferredFont(forTextStyle: .headline), NSForegroundColorAttributeName: UIColor.black]
        let subtitleAttributes = [NSFontAttributeName: UIFont.preferredFont(forTextStyle: .subheadline)]
        
        let titleString = NSMutableAttributedString(string: " Q : \(title)\n", attributes: titleAttributes)
        let subtitleString = NSAttributedString(string: " A : \(subtitle)", attributes: subtitleAttributes)
        
        titleString.append(subtitleString)
        
        return titleString
    }
}*/


//
// MARK: - View Controller DataSource and Delegate
//
extension FAQViewController {
    
     func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].items.count
    }
    
    // Cell
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell? ?? UITableViewCell(style: .default, reuseIdentifier: "cell")
        
        cell.textLabel?.text = sections[indexPath.section].items[indexPath.row]
        cell.textLabel?.numberOfLines = 0
        cell.selectionStyle = .none
        
        
        return cell
    }
    
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return sections[indexPath.section].collapsed! ? 0 : self.calculateHeight(indexPath: indexPath )
        
    }
    
    func calculateHeight(indexPath: IndexPath ) -> CGFloat
    {
        
        let attributes : [String : Any] = [NSFontAttributeName : UIFont.systemFont(ofSize: 20.0)]
        
        let attributedString : NSAttributedString = NSAttributedString(string: sections[indexPath.section].items[indexPath.row] , attributes: attributes)
        
        let rect : CGRect = attributedString.boundingRect(with: CGSize(width: self.view.frame.width, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        
        let requredSize:CGRect = rect
        return requredSize.height + 20
    }
    
    // Header
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as? ExpandableTableViewHeader ?? ExpandableTableViewHeader(reuseIdentifier: "header")
        
        header.titleLabel.text = sections[section].name
        header.titleLabel.numberOfLines = 0
        header.arrowLabel.text = ">"
        header.setCollapsed(sections[section].collapsed)
        
        header.section = section
        header.delegate = self
        
        return header
    }
    
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        
        let attributes : [String : Any] = [NSFontAttributeName : UIFont.systemFont(ofSize: 20.0)]
        
        let attributedString : NSAttributedString = NSAttributedString(string: sections[section].name , attributes: attributes)
        
        let rect : CGRect = attributedString.boundingRect(with: CGSize(width: self.view.frame.width, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        
        let requredSize:CGRect = rect
        
        if( requredSize.height + 10 < 44.0){
            
            return 44.0
            
        }else {
            return requredSize.height + 10
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at:indexPath , animated: false)
    }
}

//
// MARK: - Section Header Delegate
//
extension FAQViewController: ExpandableTableViewHeaderDelegate {
    
    func toggleSection(_ header: ExpandableTableViewHeader, section: Int) {
        
        for i in 0 ..< sections.count {
            
            if section != i {
                sections[i].collapsed = true
                
            }
        }
        
        
        let collapsed = !sections[section].collapsed
        sections[section].collapsed = collapsed
        header.setCollapsed(collapsed)
        self.tblViewFAQ.reloadData()
        
        if(!collapsed){
            self.tableViewScrollToBottom(section: section)
        }
    }
    
    
    func tableViewScrollToBottom(section: Int) {
        
        //let numberOfSections = self.menyTblView.numberOfSections
        let numberOfRows = self.tblViewFAQ.numberOfRows(inSection: section)
        
        if numberOfRows > 0 {
            let indexPath = IndexPath(row: numberOfRows-1, section: section)
            self.tblViewFAQ.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
}

