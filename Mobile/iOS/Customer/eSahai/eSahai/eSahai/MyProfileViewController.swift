//
//  MyProfileViewController.swift
//  eSahai
//
//  Created by UshaRao on 11/9/16.
//  Copyright © 2016 TechVedika. All rights reserved.
//

import UIKit
import MobileCoreServices

class TableViewCell: UITableViewCell {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var btntbl: UIButton!
}


class AttributesTblViewCell: UITableViewCell {
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var lblattributes: UITextField!
    
    public func configure(text: String?, placeholder: String) {
        lblText.text = placeholder
        lblattributes.text = text
        lblattributes.placeholder = placeholder
        
        lblattributes.accessibilityValue = text
        lblattributes.accessibilityLabel = placeholder
    }
}

class MyProfileViewController: BaseViewController,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate {
    
    var customPickerView : UIView! = UIView()
    var pickerView : UIPickerView = UIPickerView()
    var intTag =  NSInteger()
    var arrBloodGroups = NSMutableArray()
    var arrSex = NSMutableArray()
    var arrHowDoYouKnow = NSMutableArray()
    var arrDictHowDoYouKnow = NSMutableArray()

    var loadAttributesViewObj = UIView()
    var items = NSMutableArray()
    var attributes = NSMutableArray()
    var dict = NSMutableDictionary()
    
    var clevertap: CleverTap?

    
    @IBOutlet weak var tblAttributesView: UITableView!
    @IBOutlet weak var tblViewPopUp: UITableView!
    @IBOutlet weak var popview: UIView!
    @IBOutlet weak var tblViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var attributesTableViewConstraint: NSLayoutConstraint!
    
    @IBOutlet var constraintSubmitTop: NSLayoutConstraint!
    
    @IBOutlet var btnAddAttribute_Outlet: UIButton!
    
    @IBOutlet var txtBloodGroup: UITextField!
    @IBOutlet var txtPincode: UITextField!
    @IBOutlet var txtCity: UITextField!
    @IBOutlet var txtState: UITextField!
    @IBOutlet var txtAddress: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtAge: UITextField!
    @IBOutlet var txtGender: UITextField!
    @IBOutlet var txtName: UITextField!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var donateBlood: UIButton!
    @IBOutlet weak var txtHowdoYouKnow: UITextField!
    
    @IBOutlet weak var HowDoYouKnowlbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CleverTap.sharedInstance()?.recordEvent("iOS_MyProfileViewController")
       // CleverTap.sharedInstance()?.profileGetCleverTapID()
        CleverTap.getLocationWithSuccess({(location: CLLocationCoordinate2D) -> Void in
            // do something with location here, optionally set on CleverTap for use in segmentation etc
            CleverTap.setLocation(location)
        }, andError: {(error: String?) -> Void in
            if let e = error {
                print(e)
            }
        })
        
        
        
        // example usage
        let lastTimeAppLaunched = clevertap?.userGetPreviousVisitTime()
        
        
        
       // self.HowDoYouKnowlbl.sizeToFit()
        self.title = "My Profile"
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        arrSex = ["Male","Female"]
        arrBloodGroups = ["A+","A-","B+","B-","O+","O-","AB+","AB-"]
        tblAttributesView.isScrollEnabled = false
        //For Rounded Image View
        imgProfile.layer.borderWidth = 1
        imgProfile.layer.masksToBounds = false
        imgProfile.layer.borderColor = UIColor.white.cgColor
        imgProfile.layer.cornerRadius = 40.0
        imgProfile.clipsToBounds = true
        
        tblAttributesView.isScrollEnabled = false
        tblViewPopUp.isScrollEnabled = false
        
        // let headerView = view as? UIView()
        let headerView = UILabel()
        headerView.frame = CGRect( x : 0, y : 0 ,width: tblViewPopUp.frame.size.width, height: 44)
        headerView.font = headerView.font.withSize(20)
        headerView.textColor = UIColor.white
        headerView.textAlignment = .center
        headerView.text = "Add Attributes"
        headerView.backgroundColor = UIColor(hex :"#1C73A4")
        
        tblViewPopUp.tableHeaderView = headerView
    
        self.getHowDoYouKnow()
        
        self.resizeAttributeView()
        self.addToolBar(txtAge)
        self.addToolBar(txtPincode)
        if UserDefaults.standard.value(forKey: "ProfileImage") != nil
        {
            if(appDelegate.checkInternetConnectivity()){
            let profileData = UserDefaults.standard.value(forKey: "ProfileImage") as! NSDictionary
            
            DispatchQueue.global(qos: .default).async(execute: {() -> Void in
                
                let url = NSURL(string:(profileData["imageUrl"] as? String)!)
                let data = NSData(contentsOf:url as! URL)
                let image = UIImage(data:data! as Data)

                DispatchQueue.main.async(execute: {() -> Void in
                    // Update the UI
                    self.imgProfile.image = image
                })
            })
        }
    }
    }
    
    func getHowDoYouKnow() {
        let  strUrl = String(format :"%@getComeToKnowBy",kServerUrl) as NSString
        
        let url : NSURL = NSURL(string: strUrl as String)!
        
        sharedController.requestGETURL(strURL:url, success:{(result) in
            DispatchQueue.main.async()
                {
                    let strStatusCode = result["statusCode"] as? String
                    
                    if strStatusCode == "300"
                    {
                        self.arrDictHowDoYouKnow = (result["responseData"] as? NSMutableArray)!
                        
                        for i in 0..<self.arrDictHowDoYouKnow.count{
                            let tempDict = self.arrDictHowDoYouKnow.object(at: i) as! NSMutableDictionary
                            
                            for (key, value) in tempDict {
                                print("value --> \(value)" )
                                if((key as! String ) == "know_by"){
                                    self.arrHowDoYouKnow.add(value)
                                }
                            }
                        }
                        
                        print("arrHowDoYouKnow \(self.arrHowDoYouKnow)")
                        self.getProfile()
                        
                    }
                    else if(strStatusCode != "500")
                    {
                        self.appDelegate.window?.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                        return
                    }
                    
            }
            
        }, failure:  {(error) in
        })
    }
    
    func setUpProfileView() {
        
        
        for (key, value) in  self.dict {
            print("value --> \(value)" )
            if value is NSNull{
                self.dict[key] = ""
                // arrEmergencyTypeList.replaceObject(at: i, with: tempDict)
            }
        }
        
        if !(self.dict["customer_name"] as? String == ""){
            UserDefaults.standard.setValue(self.dict["customer_name"] as? String, forKey: "customer_name")
            txtName.text = self.dict["customer_name"] as? String
        }
        else{
            UserDefaults.standard.setValue(nil, forKey: "customer_name")
        }
        if !( self.dict["gender"] as? String == ""){
            txtGender.text = self.dict["gender"] as? String
        }else {
            txtGender.text = arrSex.object(at: 0) as? String
        }
        
        if !( self.dict["age"] as? String == "0"){
            txtAge.text = self.dict["age"] as? String
            UserDefaults.standard.setValue(self.dict["age"] as? String, forKey: "customer_age")
        }
        
        if !( self.dict["customer_email"] as? String  == "null"){
            txtEmail.text = self.dict["customer_email"] as? String
        }
        if !( self.dict["address"] as? String == "null"){
            txtAddress.text = self.dict["address"] as? String
        }
        if !( self.dict["state"] as? String == "null"){
            txtState.text = self.dict["state"] as? String
        }
        if !( self.dict["city"] as? String == "null"){
            txtCity.text = self.dict["city"] as? String
        }
        if !( self.dict["pincode"] as? String == "0"){
            txtPincode.text = self.dict["pincode"] as? String
        }
        if !( self.dict["blood_group"] as? String == ""){
            txtBloodGroup.text = self.dict["blood_group"] as? String
        }else {
            txtBloodGroup.text = arrBloodGroups.object(at: 0) as? String
        }
        if !( self.dict["come_to_know_by"] as? String == ""){
            txtHowdoYouKnow.text = self.dict["come_to_know_by"] as? String
        }else {
            txtHowdoYouKnow.text = arrHowDoYouKnow.object(at: 0) as? String
        }
        
        if !( self.dict["donating_blood"] as? String == "0")  {
            let image = UIImage(named: "selected.png")! as UIImage
            donateBlood.setBackgroundImage(image, for: UIControlState.normal)
            donateBlood.isSelected = true
        }else {
            let image = UIImage(named: "unselected.png")! as UIImage
            donateBlood.setBackgroundImage(image, for: UIControlState.normal)
            donateBlood.isSelected = false
        }
        
        
        if((UserDefaults.standard.value(forKey: "isFirstTime")as! Bool)){
            let image = UIImage(named: "selected.png")! as UIImage
            donateBlood.setBackgroundImage(image, for: UIControlState.normal)
            donateBlood.isSelected = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Slide Navigation
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        
        if(UserDefaults.standard.value(forKey: "isFirstTime")as! Bool){
            return false
        }
        return true
    }
    @IBAction func btnAddAttributesClicked(_ sender: AnyObject) {
        
        self.resignKeyboard()
        if(self.items.count > 0){
            self.tblViewPopUp.isHidden = false
            self.popview.isHidden=false
            self.resizeviews()
            self.navigationController!.navigationBar.isUserInteractionEnabled = false
        }
    }
    
    
    @IBAction func btnLikeToDonateBlood(_ sender: AnyObject) {
        
        if donateBlood.isSelected {
            
            let alertController = UIAlertController(title: "", message: "Do you really don't want to donate blood.?", preferredStyle: UIAlertControllerStyle.alert)
            
            let DestructiveAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
             
                let image = UIImage(named: "unselected.png")! as UIImage
                self.donateBlood.setBackgroundImage(image, for: UIControlState.normal)
                self.donateBlood.isSelected = false
                
            }
            let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                
                let image = UIImage(named: "selected.png")! as UIImage
                self.donateBlood.setBackgroundImage(image, for: UIControlState.normal)
                self.donateBlood.isSelected = true
                
            }
            alertController.addAction(DestructiveAction)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
            
            
        }else{
            
            
            let alertController = UIAlertController(title: "", message: "Thank you for opting to donate blood. You would be notified whenever there is a blood request", preferredStyle: UIAlertControllerStyle.alert)
            
            let DestructiveAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                
                let image = UIImage(named: "unselected.png")! as UIImage
                self.donateBlood.setBackgroundImage(image, for: UIControlState.normal)
                self.donateBlood.isSelected = false
            }
            let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                
                let image = UIImage(named: "selected.png")! as UIImage
                self.donateBlood.setBackgroundImage(image, for: UIControlState.normal)
                self.donateBlood.isSelected = true
            }
            alertController.addAction(DestructiveAction)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
           
        }
    }
    
    // MARK: -  Profile Image Selectiion Code
    @IBAction func btnSelectProfileImage(_ sender: AnyObject) {
        self.resignKeyboard()
        let actionSheet = UIActionSheet(title: "Choose An Option", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Take a photo", "Select from gallery")
            actionSheet.show(in: self.view)
    }
    
    @objc(actionSheet:clickedButtonAtIndex:) func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {
        
        if buttonIndex == 0 {
            return
        }
        if buttonIndex == 1 {
            self.perform(#selector(self.showCamera), with: nil, afterDelay: 0.45)
        }
        else {
            self.perform(#selector(self.showLibrary), with: nil, afterDelay: 0.45)
        }
    }
    
    func showCamera(){
        let imagePicker = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.mediaTypes = [(kUTTypeImage as String)]
            self.navigationController!.present(imagePicker, animated: true, completion: { _ in })
        }
    }
    
    func showLibrary() {
        let imagePicker = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imagePicker.sourceType = .photoLibrary
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.mediaTypes = [(kUTTypeImage as String)]
            self.navigationController!.present(imagePicker, animated: true, completion: { _ in })
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)    {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
       dismiss(animated: true, completion: nil)
        //self.sendImage(toServer: (info[UIImagePickerControllerOriginalImage] as! String))
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            //imgProfile.contentMode = .scaleAspectFit
            setProfilePicture(image: pickedImage)
        }
    }
    
    func setProfilePicture(image : UIImage)
    {
         MBProgressHUD.showAdded(to: self.view, animated: true)
        let  strUrl = String(format : "%@%@",kServerUrl,"updateProfile") as NSString
        
        let url = NSURL(string:strUrl as String)
        
        let request = NSMutableURLRequest(url: url as! URL)
        request.httpMethod = "POST"
        request.addValue(UserDefaults.standard.value(forKey: kCustomer_id)as! String, forHTTPHeaderField: "userId")
        request.addValue(UserDefaults.standard.value(forKey: kToken)as! String, forHTTPHeaderField: kToken)
        
        let boundary = generateBoundaryString()
        
        //define the multipart request type
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        //reducing image size
         let image_data = image.jpeg(.lowest)
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        let body = NSMutableData()
        let fname = "test.png"
        let mimetype = "image/png"
        
        let dictParams = ["customer_mobile": UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String,
             "customerApp": true] as NSDictionary
        
        //define the data post parameter
        for (key, value) in dictParams {
            body.appendString(string: "--\(boundary)\r\n")
            body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.appendString(string: "\(value)\r\n")
        }
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"test\"\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append("hi\r\n".data(using: String.Encoding.utf8)!)
        
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"file\"; filename=\"\(fname)\"\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append(image_data!)
        body.append("\r\n".data(using: String.Encoding.utf8)!)
        
        
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        
        request.httpBody = body as Data
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request as URLRequest) {
            (
            data, response, error) in
           DispatchQueue.main.async(){  
          
            guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                print("error")
                MBProgressHUD.hide(for: self.view, animated: true)
                self.view.makeToast(error?.localizedDescription, duration:kToastDuration , position:CSToastPositionCenter)
                return
            }
            
            do {
                let parsedData = try JSONSerialization.jsonObject(with: data!, options:.mutableContainers) as! [String:Any]
                print(parsedData)
                UserDefaults.standard.setValue(parsedData, forKey: "ProfileImage")
                
                LazyImage.show(imageView:self.imgProfile, url:parsedData["imageUrl"] as? String) {
                        () in
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
            } catch let error as NSError {
                
                print("error=\(error)")
                MBProgressHUD.hide(for: self.view, animated: true)
                self.view.makeToast(error.localizedDescription, duration:kToastDuration , position:CSToastPositionCenter)

                return
            }
            }
        }
        task.resume()
    }
   
    func generateBoundaryString() -> String
    {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    // MARK: - Text field delegate code
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if(textField.tag < self.attributes.count) {
            
            let xyz = self.attributes.object(at:textField.tag)
            let abc = NSMutableDictionary()
            abc.addEntries(from: xyz as! [String : String])
            abc.setValue(textField.text ?? "", forKeyPath: "attribute_details")
            self.attributes.replaceObject(at:textField.tag, with: abc)
            self.tblAttributesView.reloadData()
            self.resizeAttributeView()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.dismissPicker()
        return textField.resignFirstResponder()
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        //self.view.endEditing(true)
        self.dismissPicker()
        
            if textField.tag == 100 {
                self.dismissPicker()
            }
            if textField.tag == 200 {
                self.resignKeyboard()
                intTag = 222
                arrSex = ["Male","Female"]
                self.openPickerView()
                return false
                
            }
            if textField.tag == 300 {
                self.dismissPicker()
                //scrollView.contentOffset = CGPoint(x: 0, y: 50)
            }
            if textField.tag == 400 {
                //scrollView.contentOffset = CGPoint(x: 0, y: 90)
            }
            if textField.tag == 500 {
                //scrollView.contentOffset = CGPoint(x: 0, y: 130)
            }
            if textField.tag == 600 {
                //scrollView.contentOffset = CGPoint(x: 0, y: 170)
                
            }
            if textField.tag == 700 {
                //scrollView.contentOffset = CGPoint(x: 0, y: 210)
            }
            if textField.tag == 800 {
                //scrollView.contentOffset = CGPoint(x: 0, y: 250)
                
            }
            else if textField.tag == 900 {
                
                self.resignKeyboard()
                //scrollView.contentOffset = CGPoint(x: 0, y: 290)
                intTag = 999
              //  arrBloodGroups = ["A+","A-","B+","B-","O+","O-","AB+","AB-"]
                self.openPickerView()
                
                return false
            }
            
            else if textField.tag == 1000 {
                
                self.resignKeyboard()
               // scrollView.contentOffset = CGPoint(x: 0, y: 370)
                intTag = 1111
                // arrBloodGroups = ["A+","A-","B+","B-","O+","O-"]
                self.openPickerView()
                return false
            }
            
            return true
        
    }
    
    func addToolBar(_ textField: UITextField) {
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        toolBar.isTranslucent = false
        toolBar.barTintColor = UIColor(hex: "#195F92")
        let btnDone = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.resignKeyboard))
        let btnSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        btnDone.tintColor = UIColor.white
        toolBar.items = [btnSpace, btnDone]
        textField.inputAccessoryView = toolBar
    }
    
    // MARK: - Picker View
    
    func openPickerView() {
        
        customPickerView.center = CGPoint(x: 160, y: 1100)
        if customPickerView != nil {
            customPickerView.removeFromSuperview()
            customPickerView = nil
        }
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        UIView.commitAnimations()
        let iOSDeviceScreenSize = UIScreen.main.bounds.size
        if iOSDeviceScreenSize.height == 480 {
            customPickerView = UIView(frame: CGRect(x: 0, y: iOSDeviceScreenSize.height - 180, width: iOSDeviceScreenSize.width, height: 180))
        }
        else {
            customPickerView = UIView(frame: CGRect(x: 0, y: iOSDeviceScreenSize.height - 180, width: iOSDeviceScreenSize.width, height: 180))
        }
        self.view.addSubview(customPickerView)
        //Adding toolbar
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: iOSDeviceScreenSize.width, height: 44))
        toolBar.sizeToFit()
        toolBar.isTranslucent = false
        toolBar.barTintColor = UIColor(hex: "#195F92")
        let btnDone = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.dismissPicker))
        let btnSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        btnDone.tintColor = UIColor.white
        toolBar.items = [btnSpace, btnDone]
        customPickerView.addSubview(toolBar)
        if iOSDeviceScreenSize.height == 480 {
            pickerView = UIPickerView(frame: CGRect(x: 0, y: 44, width: iOSDeviceScreenSize.width, height: 136))
        }
        else {
            pickerView = UIPickerView(frame: CGRect(x: 0, y: 44, width: iOSDeviceScreenSize.width, height: 136))
        }
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.backgroundColor = UIColor.white
        pickerView.showsSelectionIndicator = true
        customPickerView.addSubview(pickerView)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        UIView.commitAnimations()
        self.view.bringSubview(toFront: customPickerView)
    }
    func dismissPicker() {
        //scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        customPickerView.center = CGPoint(x: 160, y: 3000)
        UIView.commitAnimations()
    }
    func resignKeyboard() {
        txtName.resignFirstResponder()
        txtGender.resignFirstResponder()
        txtAge.resignFirstResponder()
        txtBloodGroup.resignFirstResponder()
        txtCity.resignFirstResponder()
        txtState.resignFirstResponder()
        txtEmail.resignFirstResponder()
        txtAddress.resignFirstResponder()
        txtPincode.resignFirstResponder()
    }
    // MARK: - Picker Delegate Methods
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if intTag == 222 {
            return arrSex.count
        }
        else if intTag == 999 {
            return arrBloodGroups.count
        }
        else if intTag == 1111 {
            return self.arrHowDoYouKnow.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if intTag == 222 {
            return arrSex.object(at: row) as? String
        }
        else if intTag == 999 {
            return arrBloodGroups.object(at: row) as? String
        }
        else if intTag == 1111 {
            return self.arrHowDoYouKnow.object(at: row) as? String
        }

        
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if intTag == 222 {
            txtGender.text = arrSex.object(at: row) as? String
        }
        else if intTag == 999 {
            txtBloodGroup.text = arrBloodGroups.object(at: row) as? String
        }
        else if intTag == 1111 {
        self.txtHowdoYouKnow.text = self.arrHowDoYouKnow.object(at: row) as? String
        }
    }
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        let sectionWidth = 200
        return CGFloat(sectionWidth)
    }

    @IBAction func btnSubmitClicked(_ sender: AnyObject) {
        
        if (txtName.text?.isEmpty)!{
            self.view.makeToast("Please Enter Your Name", duration:kToastDuration, position:CSToastPositionCenter)
            return
        }
        
       /* if (txtName.text?.isEmpty)! || (txtGender.text?.isEmpty)! || (txtAge.text?.isEmpty)!
        {
            self.view.makeToast("Enter Mandatory details", duration:kToastDuration, position:CSToastPositionCenter)
            return
        }else{*/
            
            let  strUrl = String(format : "%@%@",kServerUrl,"updateCustomerProfile") as NSString
            
            var temp = String()
            if(donateBlood.isSelected){
                temp = "1"
            }else{
                temp = "0"
            }
            
            let dictParams =
                ["customerId": UserDefaults.standard.value(forKey: kCustomer_id)as! String ,
                 "customer_mobile": UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String,
                "customerName": txtName.text! as String ,
                "gender": txtGender.text! as String ,
                "age": txtAge.text! as String ,
                "customerApp": true,
                "blood_group": txtBloodGroup.text! as String ,
                "donating_blood": temp ,
                "come_to_know_by": txtHowdoYouKnow.text! as String ,
                "city":txtCity.text! as String ,
                "state": txtState.text! as String ,
                "address": txtAddress.text! as String ,
                "email": txtEmail.text! as String ,
                "pincode": txtPincode.text! as String ,
                "Attributes": self.attributes
                    ] as NSDictionary
        
        // each of the below mentioned fields are optional
        // with the exception of one of Identity, Email, FBID or GPID
        let profile: Dictionary<String, AnyObject> = [
            "Name": txtName.text! as String as AnyObject,       // String
            "Identity": "" as AnyObject,         // String or number
            "Email": txtAddress.text! as String as AnyObject ,    // Email address of the user
            "Phone": "" as AnyObject,      // Phone (with the country code, starting with +)
            "Gender": txtGender.text! as String as AnyObject ,                // Can be either M or F
            "Employed": "Y" as AnyObject,              // Can be either Y or N
            "Education": "Graduate" as AnyObject,      // Can be either School, College or Graduate
            "Married": "No" as AnyObject,               // Can be either Y or N
            "Age": txtAge.text! as String as AnyObject ,
            
            // optional fields. controls whether the user will be sent email, push etc.
           // "MSG-email": false,           // Disable email notifications
            //"MSG-push": true,             // Enable push notifications
            //"MSG-sms": false              // Disable SMS notifications
        ]
        
       // CleverTap.sharedInstance()?.onUserLogin(profile)
        
        CleverTap.sharedInstance()?.profilePush(profile)

            
            let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
            //CleverTap.sharedInstance()?.onUserLogin(dictParams as! [AnyHashable : Any])
            sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in
                DispatchQueue.main.async(){
                    let strStatusCode = result["statusCode"] as? String
                    
                    if strStatusCode == nil
                    {
                        return
                    }
                    
                    if strStatusCode == "300"
                    {
                        if (UserDefaults.standard.value(forKey: "isFirstTime")as! Bool) {
                            UserDefaults.standard.set(false, forKey: "isFirstTime")
                        }
                        
                        let DashCon  = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
                        self.navigationController?.pushViewController(DashCon, animated: true)
                       // CleverTap.sharedInstance()?.onUserLogin(dictParams as! [AnyHashable : Any])

                        

                    }
                    else if strStatusCode == "204"{
                        
                        self.logout()
                    }
                    else if(strStatusCode != "500"){
                        self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                    }
                }
            }, failureHandler: {(error) in
            })
            
            
        
    }
    
    // MARK: - Pop Up change
    func resizeviews() {
        tblViewHeightConstraint.constant = self.tblViewPopUp.contentSize.height
    }
    func resizeAttributeView() {
        attributesTableViewConstraint.constant = self.tblAttributesView.contentSize.height
    }
    
    // MARK: - TableView Delagates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if(tableView == self.tblViewPopUp){
            return self.items.count
        }
        else if(tableView == self.tblAttributesView) {
            return self.attributes.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if(tableView == self.tblViewPopUp){

            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
            cell.label?.text = (((self.items[indexPath.row]) as! NSDictionary).value(forKey: "type_name"))as? String
            cell.btntbl.tag = indexPath.row
            cell.btntbl.addTarget(self,action:#selector(self.btnClickedAtIndex(sender:)), for: .touchUpInside)
            return cell
        }
        else{
            let attrCell  = tableView.dequeueReusableCell(withIdentifier: "attributesCell", for: indexPath) as! AttributesTblViewCell
            attrCell.lblattributes.delegate = self
            attrCell.lblattributes.tag = indexPath.row
            
            let text1 = (((self.attributes[indexPath.row]) as! NSDictionary).value(forKey: "attribute_details"))as! String
            
            let placeholder1 = (((self.attributes[indexPath.row]) as! NSDictionary).value(forKey: "attribute_name"))as! String
            
            attrCell.configure(text: text1, placeholder: placeholder1)
            return attrCell
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at:indexPath , animated: false)
    }
    
    /*func numberOfSections(in tableView: UITableView) -> Int {
        if(tableView == self.tblViewPopUp){
            return 1
        }
        else if(tableView == self.tblAttributesView) {
            return 1
        }else{
            return 0
        }
    }*/
    
   /* func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if(tableView == self.tblViewPopUp){
            return "Add Attributes"
        }
        else{
                return ""
        }
    }*/
    
    
    
   /* func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if(tableView == self.tblViewPopUp){

        if let headerView = view as? UITableViewHeaderFooterView {
            
            headerView.textLabel?.font = headerView.textLabel?.font.withSize(20)
            headerView.textLabel?.textColor = UIColor.white
            headerView.textLabel?.textAlignment = .center
            headerView.contentView.backgroundColor = UIColor(hex :"#1C73A4")
        }
        }
    }*/
    
    func btnClickedAtIndex(sender : UIButton!) {
        tblViewPopUp.isHidden = true
        self.popview.isHidden=true
        self.navigationController!.navigationBar.isUserInteractionEnabled = true
         let indexNum = sender.tag 
        
        let info: [String: String] = [
            "attribute_name" : ((((self.items[indexNum]) as! NSDictionary).value(forKey: "type_name"))as? String)! ,
            "attribute_details" : "",
            "customer_e_type_id" : ((((self.items[indexNum]) as! NSDictionary).value(forKey: "id"))as? String)!
        ]
        
        self.attributes.add(info)
        
        
        self.tblAttributesView.reloadData()
        self.items.removeObject(at: indexNum)
        self.tblViewPopUp.reloadData()
        self.resizeviews()
        self.resizeAttributeView()
    }
    
    // MARK: - getProfile Syscall
    
    func getProfile() {
        
        let  strUrl = String(format : "%@%@",kServerUrl,"getCustomerProfile") as NSString

        let dictParams = ["iOS":"true","customer_id": UserDefaults.standard.value(forKey: kCustomer_id)as! String,"customerApp": true,"customer_mobile": UserDefaults.standard.value(forKey: kEncryptedMobileNumber)as! String] as NSDictionary
        
        let dictHeaders = ["userId":UserDefaults.standard.value(forKey: kCustomer_id)as! String,kToken:UserDefaults.standard.value(forKey: kToken)as! String] as NSDictionary
        
        sharedController.requestPOSTURL(strUrl: strUrl, postParams: dictParams, postHeaders: dictHeaders, successHandler:{(result) in
            DispatchQueue.main.async(){
                let strStatusCode = result["statusCode"] as? String
                
                if strStatusCode == nil
                {
                    return
                }
                
                if strStatusCode == "300"
                {
                     self.dict = (result["responseData"] as? NSMutableDictionary)!
                    
                    self.items = self.dict["systemAttributes"] as? NSArray as! NSMutableArray
                    self.attributes = self.dict["Attributes"] as? NSArray as! NSMutableArray
                 
                    for res:AnyObject in self.attributes as [AnyObject]{
                        for res1:AnyObject in self.items as [AnyObject]{
                            if((res["customer_e_type_id"] as! String) == (res1["id"] as! String)){
                                self.items.remove(res1)
                            }
                        }
                    }
                    
                    self.tblViewPopUp.reloadData()
                    self.tblAttributesView.reloadData()
                    self.resizeAttributeView()
                    
                    print("printing value -->> \(self.dict)")
                    
                    self.setUpProfileView()
                }
                else if strStatusCode == "204"{
                    self.logout()
                }else if(strStatusCode != "500"){
            
                    self.view.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                }
            }
        }, failureHandler: {(error) in
        })
    }
    
    @IBAction func btnDropDown(_ sender: Any) {
        self.view.endEditing(true)
        self.dismissPicker()
        self.intTag = 1111
        self.openPickerView()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
       // let testView : view
        let touch:UITouch = touches.first!
        if touch.view != tblViewPopUp {
            if !(self.tblViewPopUp.isHidden){
                self.tblViewPopUp.isHidden = true
                self.popview.isHidden = true
                self.navigationController!.navigationBar.isUserInteractionEnabled = true
            }
        }
    }
}
extension NSMutableData {
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in PNG format
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the PNG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    var png: Data? { return UIImagePNGRepresentation(self) }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ quality: JPEGQuality) -> Data? {
        return UIImageJPEGRepresentation(self, quality.rawValue)
    }
}
