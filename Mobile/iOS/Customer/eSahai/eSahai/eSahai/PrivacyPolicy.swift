//
//  PrivacyPolicy.swift
//  eSahai
//
//  Created by PINKY on 01/03/17.
//  Copyright © 2017 TechVedika. All rights reserved.
//

import UIKit

class PrivacyPolicy: BaseViewController {

    @IBOutlet weak var webViewPP: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Privacy Policy"

        webViewPP.scrollView.isScrollEnabled = true

        // Do any additional setup after loading the view.
        self.getPrivacyPolicyDetails()
    }
    
    func getPrivacyPolicyDetails(){
        
        let  strUrl = String(format :"%@getPrivacyPolicy",kServerUrl) as NSString
        
        let url : NSURL = NSURL(string: strUrl as String)!
        
        sharedController.requestGETURL(strURL:url, success:{(result) in
            DispatchQueue.main.async()
                {
                    let strStatusCode = result["statusCode"] as? String
                    
                    if strStatusCode == "300"
                    {
                        
                        
                        let htmlString : String = (result["responseData"] as! NSDictionary).value(forKey: "privacy_and_policy") as! String
                        
                        let aStr = String(format: "<html><body>%@</body></html>", htmlString)

                        self.webViewPP.loadHTMLString(aStr,
                                                    baseURL: nil)

                    }
                    else
                    {
                        self.appDelegate.window?.makeToast(result["statusMessage"] as! String, duration:kToastDuration, position:CSToastPositionCenter)
                        return
                    }
                    
            }
            
        }, failure:  {(error) in
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Slide Navigation
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
