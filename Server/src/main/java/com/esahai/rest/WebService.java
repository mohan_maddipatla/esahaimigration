package com.esahai.rest;

import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.esahai.dbconnect.dao.*;
import com.esahai.dbconnect.entity.*;
import com.google.gson.Gson;

@Path("/services")
public class WebService {
    
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getAccessRolesDetails")
	public String getAccessRoles(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		AccessRolesDao accessrolesDao = (AccessRolesDao) context.getBean("AccessRolesDao",
				AccessRolesDao.class);

		List<AccessRoles> accessrolesList = accessrolesDao.getAccessRolesDetails();

		String json = new Gson().toJson(accessrolesList);
		return json;
	}


	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getAcceptedAmbulancesDetails")
	public String getAcceptedAmbulances(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		AcceptedAmbulancesDao acceptedambulancesDao = (AcceptedAmbulancesDao) context.getBean("AcceptedAmbulancesDao",
				AcceptedAmbulancesDao.class);

		List<AcceptedAmbulances> acceptedAmbulancesList = acceptedambulancesDao.getAcceptedAmbulancesDetails();

		String json = new Gson().toJson(acceptedAmbulancesList);
		return json;
	}

	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getAmbulanceAttributesDetails")
	public String getAmbulanceAttributes(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		AmbulanceAttributesDao ambulanceattributesDao = (AmbulanceAttributesDao) context.getBean("AmbulanceAttributesDao",
				AmbulanceAttributesDao.class);

		List<AmbulanceAttributes> ambulanceAttributesList = ambulanceattributesDao.getAmbulanceAttributesDetails();

		String json = new Gson().toJson(ambulanceAttributesList);
		return json;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getAmbulance_e_AttributesDetails")
	public String getAmbulance_e_Attributes(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		Ambulance_e_AttributesDao ambulance_e_attributesDao = (Ambulance_e_AttributesDao) context.getBean("Ambulance_e_AttributesDao",
				Ambulance_e_AttributesDao.class);

		List<Ambulance_e_Attributes> ambulance_e_AttributesList = ambulance_e_attributesDao.getAmbulance_e_AttributesDetails();

		String json = new Gson().toJson(ambulance_e_AttributesList);
		return json;
	}


   	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getEmergencyBookings")
	public String getEmergencyBookings(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		EmergencyBookingDao emergencyBookingDao = (EmergencyBookingDao) context.getBean("emergencyBookingDao",
				EmergencyBookingDao.class);

		List<EmergencyBooking> emergencyBookingList = emergencyBookingDao.getAllEmergencyBookings();

		String json = new Gson().toJson(emergencyBookingList);
		return json;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBloodBankDetails")
	public String getBloodBankDetails(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		BloodBankDao bloodBankDao = (BloodBankDao) context.getBean("bloodBankDao", BloodBankDao.class);

		List<BloodBank> bloodBankList = bloodBankDao.getBloodBankDetails();

		String json = new Gson().toJson(bloodBankList);
		json="{"+'"'+"statusCode"+'"'+": 300,"+'"'+"statusData"+'"'+":"+'"'+"valid"+'"'+","+'"'+"responseData"+'"'+":"+json+"}";
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getVehicleTypeDetails")
	public String getVehicleTypeDetails(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		VehicleTypeDao VehicleTypeDao = (VehicleTypeDao) context.getBean("VehicleTypeDao", VehicleTypeDao.class);

		List<VehicleType> VehicleTypeList = VehicleTypeDao.getVehicleTypeDetails();

		String json = new Gson().toJson(VehicleTypeList);
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getUserTypeDetails")
	public String getUserTypeDetails(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		UserTypeDao UserTypeDao = (UserTypeDao) context.getBean("UserTypeDao", UserTypeDao.class);

		List<UserType> UserTypeList = UserTypeDao.getUserTypeDetails();

		String json = new Gson().toJson(UserTypeList);
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getUserAppRoles")
	public String getUserAppRoles(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		UserAppRolesDao UserAppRolesDao = (UserAppRolesDao) context.getBean("UserAppRolesDao", UserAppRolesDao.class);

		List<UserAppRoles> UserAppRolesList = UserAppRolesDao.getUserAppRolesDetails();

		String json = new Gson().toJson(UserAppRolesList);
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getTrackingDetails")
	public String getTracking(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		TrackingDao TrackingDao = (TrackingDao) context.getBean("TrackingDao", TrackingDao.class);

		List<Tracking> TrackingList = TrackingDao.getTracking();

		String json = new Gson().toJson(TrackingList);
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getAmbulanceDriverDetails")
	public String getAmbulanceDriverDetails(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		AmbulanceDriverDao AmbulanceDriverDao = (AmbulanceDriverDao) context.getBean("AmbulanceDriverDao", AmbulanceDriverDao.class);

		List<AmbulanceDriver> AmbulanceDriverList = AmbulanceDriverDao.getAmbulanceDriverDetails();

		String json = new Gson().toJson(AmbulanceDriverList);
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getTermsConditions")
	public String getTermsConditions(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		TermsConditionsDao TermsConditionsDao = (TermsConditionsDao) context.getBean("TermsConditionsDao", TermsConditionsDao.class);

		List<TermsConditions> TermsConditionsList = TermsConditionsDao.getTermsConditions();

		String json = new Gson().toJson(TermsConditionsList);
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getAmbulance_E_Types")
	public String getAmbulance_e_types(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		Ambulance_e_typesDao Ambulance_e_typesDao = (Ambulance_e_typesDao) context.getBean("Ambulance_e_typesDao", Ambulance_e_typesDao.class);

		List<Ambulance_e_types> Ambulance_e_typesList = Ambulance_e_typesDao.getAmbulance_e_typesDetails();

		String json = new Gson().toJson(Ambulance_e_typesList);
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getSystemUsersDetails")
	public String getSystemUsersDetails(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SystemUsersDao SystemUsersDao = (SystemUsersDao) context.getBean("SystemUsersDao", SystemUsersDao.class);

		List<SystemUsers> systemUsersList = SystemUsersDao.getSytemUserDetails();

		String json = new Gson().toJson(systemUsersList);
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getStatusListDetails")
	public String getStatusListDetails(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		StatusListDao StatusListDao = (StatusListDao) context.getBean("StatusListDao", StatusListDao.class);

		List<StatusList> StatusList = StatusListDao.getStatusListDetails();

		String json = new Gson().toJson(StatusList);
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getSmsLogDetails")
	public String getSmsLogDetails(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SmsLogDao SmsLogDao = (SmsLogDao) context.getBean("SmsLogDao", SmsLogDao.class);

		List<SmsLog> SmsLogList = SmsLogDao.getSmsLogDetails();

		String json = new Gson().toJson(SmsLogList);
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getAmbulanceMasterDetails")
	public String getAmbulanceMasterDetails(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		AmbulanceMasterDao AmbulanceMasterDao = (AmbulanceMasterDao) context.getBean("AmbulanceMasterDao", AmbulanceMasterDao.class);

		List<AmbulanceMaster> AmbulanceMasterList = AmbulanceMasterDao.getAmbulanceMasterDetails();

		String json = new Gson().toJson(AmbulanceMasterList);
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getApiLogsDetails")
	public String getApiLogsDetails(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ApiLogsDao ApiLogsDao = (ApiLogsDao) context.getBean("ApiLogsDao", ApiLogsDao.class);

		List<ApiLogs> ApiLogsList = ApiLogsDao.getApiLogsDetails();

		String json = new Gson().toJson(ApiLogsList);
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getApiServiceDetails")
	public String getApiServiceDetails(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ApiServiceDao ApiServiceDao = (ApiServiceDao) context.getBean("ApiServiceDao", ApiServiceDao.class);

		List<ApiService> ApiServiceList = ApiServiceDao.getApiServiceDetails();

		String json = new Gson().toJson(ApiServiceList);
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getAppDetails")
	public String getAppDetails(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		AppDao AppDao = (AppDao) context.getBean("AppDao", AppDao.class);

		List<App> AppList = AppDao.getAppDetails();

		String json = new Gson().toJson(AppList);
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getAppSettingsDetails")
	public String getAppSettingsDetails(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		AppSettingsDao AppSettingsDao = (AppSettingsDao) context.getBean("AppSettingsDao", AppSettingsDao.class);

		List<AppSettings> AppSettingsList = AppSettingsDao.getAppSettingsDetails();

		String json = new Gson().toJson(AppSettingsList);
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getAppVersionDetails")
	public String getAppVersionDetails(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		AppVersionDao AppVersionDao = (AppVersionDao) context.getBean("AppVersionDao", AppVersionDao.class);

		List<AppVersion> AppVersionList = AppVersionDao.getAppVersionDetails();

		String json = new Gson().toJson(AppVersionList);
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getSmsDetails")
	public String getSmsDetails(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SmsDetailsDao SmsDetailsDao = (SmsDetailsDao) context.getBean("SmsDetailsDao", SmsDetailsDao.class);

		List<SmsDetails> SmsDetailsList = SmsDetailsDao.getSmsDetails();

		String json = new Gson().toJson(SmsDetailsList);
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getServiceTypeDetails")
	public String getServiceTypeDetails(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ServiceTypeDao ServiceTypeDao = (ServiceTypeDao) context.getBean("ServiceTypeDao", ServiceTypeDao.class);

		List<ServiceType> ServiceTypeList = ServiceTypeDao.getServiceTypeDetails();

		String json = new Gson().toJson(ServiceTypeList);
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getServiceTablesDetails")
	public String getServiceTablesDetails(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ServiceTablesDao ServiceTablesDao = (ServiceTablesDao) context.getBean("ServiceTablesDao", ServiceTablesDao.class);

		List<ServiceTables> ServiceTablesList = ServiceTablesDao.getServiceTablesDetails();

		String json = new Gson().toJson(ServiceTablesList);
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBloodRequestDetails")
	public String getBloodRequestDetails(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		BloodRequestDao BloodRequestDao = (BloodRequestDao) context.getBean("BloodRequestDao", BloodRequestDao.class);

		List<BloodRequest> BloodRequestList = BloodRequestDao.getBloodRequestDetails();

		String json = new Gson().toJson(BloodRequestList);
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBookingAmbulanceLogDetails")
	public String getBookingAmbulanceLogDetails(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		BookingAmbulanceLogDao BookingAmbulanceLogDao = (BookingAmbulanceLogDao) context.getBean("BookingAmbulanceLogDao", BookingAmbulanceLogDao.class);

		List<BookingAmbulanceLog> BookingAmbulanceLogList = BookingAmbulanceLogDao.getBookingAmbulanceLogDetails();

		String json = new Gson().toJson(BookingAmbulanceLogList);
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getBookingIdDetails")
	public String getBookingIdDetails(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		BookingIdDao BookingIdDao = (BookingIdDao) context.getBean("BookingIdDao", BookingIdDao.class);

		List<BookingId> BookingIdList = BookingIdDao.getBookingIdDetails();

		String json = new Gson().toJson(BookingIdList);
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getComeToKnowByDetails")
	public String geComeToKnowByDetails(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ComeToKnowByDao ComeToKnowByDao = (ComeToKnowByDao) context.getBean("ComeToKnowByDao", ComeToKnowByDao.class);

		List<ComeToKnowBy> ComeToKnowByList = ComeToKnowByDao.getComeToKnowByDetails();

		String json = new Gson().toJson(ComeToKnowByList);
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getCountryListDetails")
	public String getCountryListDetails(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		CountryListDao CountryListDao = (CountryListDao) context.getBean("CountryListDao", CountryListDao.class);

		List<CountryList> CountryList = CountryListDao.getCountryListDetails();

		String json = new Gson().toJson(CountryList);
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getCustomerAttributesDetails")
	public String getCustomerAttributesDetails(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		CustomerAttributesDao CustomerAttributesDao = (CustomerAttributesDao) context.getBean("CustomerAttributesDao", CustomerAttributesDao.class);

		List<CustomerAttributes> CustomerAttributesList = CustomerAttributesDao.getCustomerAttributesDetails();

		String json = new Gson().toJson(CustomerAttributesList);
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getCustomerEContactsDetails")
	public String getCustomerEContactsDetails(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		CustomerEContactsDao CustomerEContactsDao = (CustomerEContactsDao) context.getBean("CustomerEContactsDao", CustomerEContactsDao.class);

		List<CustomerEContacts> CustomerEContactsList = CustomerEContactsDao.getCustomerEContactsDetails();

		String json = new Gson().toJson(CustomerEContactsList);
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getServicesDetails")
	public String getServicesDetails(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ServiceDao ServiceDao = (ServiceDao) context.getBean("ServiceDao", ServiceDao.class);

		List<Services> ServicesList = ServiceDao.getServicesDetails();

		String json = new Gson().toJson(ServicesList);
		return json;
	}
}