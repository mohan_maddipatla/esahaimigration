package com.esahai.dbconnect.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.sql.Timestamp;

@Entity
@Table(name="ambulance_driver")
public class AmbulanceDriver implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@Column(name = "ambulance_id")
	private int ambulance_id;

	@Column(name = "driver_id")
	private int driver_id;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "created_date")
	private Timestamp created_date;

	@Column(name = "updated_date")
	private Timestamp updated_date;
	
	@Column(name = "latitude")
	private String latitude;
	
	@Column(name = "longitude")
	private String longitude;
	
	@Column(name = "geoHashCode")
	private String geoHashCode;
	
	@Column(name = "is_assigned")
	private byte is_assigned;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAmbulance_id() {
		return ambulance_id;
	}

	public void setAmbulance_id(int ambulance_id) {
		this.ambulance_id = ambulance_id;
	}

	public int getDriver_id() {
		return driver_id;
	}
	
	public void setDriver_id(int driver_id) {
		this.driver_id = driver_id;
	}	
	
	public String getStatus(){
		return status;
	}
	
	public void setStatus(String status){
		this.status=status;
	}
	
	public String getLongitude(){
		return longitude;
	}
	
	public void setLongitude(String longitude){
		this.longitude=longitude;
	}
		
	public String getLatitude(){
		return latitude;
	}
	public void setLatitude(String latitude){
		this.latitude=latitude;
	}
	
	public String getGeohashCode() {
		return geoHashCode;
	}

	public void setGeohashCode(String geoHashCode) {
		this.geoHashCode = geoHashCode;
	}
	
	public Timestamp getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Timestamp created_date) {
		this.created_date = created_date;
	}
    
	public Timestamp getUpdated_date() {
		return updated_date;
	}

	public void setUpdated_date(Timestamp updated_date) {
		this.updated_date = updated_date;
	}
    
	public byte getAssignedStatus() {
		return is_assigned;
	}

	public void setAssignedStatus(byte is_assigned) {
		this.is_assigned = is_assigned;
	}

}