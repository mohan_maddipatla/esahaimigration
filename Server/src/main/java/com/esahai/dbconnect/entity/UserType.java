package com.esahai.dbconnect.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.sql.Timestamp;

@Entity
@Table(name="user_types")
public class UserType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	
	@Column(name ="type_name")
	private String type_name;
	
	
	@Column(name = "created_date")
	private Timestamp created_date;

	@Column(name = "updated_date")
	private Timestamp updated_date;
	
	public int getVehicle_id() {
		return id;
	}

	public void setVehicle_id(int id) {
		this.id = id;
	}
	
	public String getType_name(){
		return type_name;
	}
	
	public void setType_name(String name){
		this.type_name=name;
	}
	
	public Timestamp getCreated_date(){
		return created_date;
	}
	
	public void setCreated_date(Timestamp date){
		this.created_date=date;
	}
	
	public Timestamp getUpdated_date(){
		return updated_date;
	}
	
	public void setUpdated_date(Timestamp date){
		this.updated_date=date;
	}
	
	
}
