package com.esahai.dbconnect.entity;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="servicetype")

public class ServiceType implements Serializable{
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	
	@Column(name = "group_name")
	private String group_name;
	
	@Column(name = "type_name")
	private String type_name;
	
	@Column(name = "isActive")
	private String isActive;
	
	@Column(name = "isDefault")
	private String isDefault;
	
	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id=id;
	}
	
	public String getGroup_name(){
		return group_name;
	}
	
	public void setGroup_name(String name){
		this.group_name=name;
	}
	
	public String getType_name(){
		return type_name;
	}
	
	public void setType_name(String name){
		this.type_name=name;
	}
	
	public String getIsActive(){
		return isActive;
	}
	
	public void setIsActive(String is){
		this.isActive=is;
	}
	
	public String getIsDefault(){
		return isDefault;
	}
	
	public void setIsDefault(String is){
		this.isDefault=is;
	}
}
