package com.esahai.dbconnect.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="api_log")
public class ApiLogs implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@Column(name = "api_path")
	private String api_path;
	
	@Column(name = "result_code")
	private String result_code;
    
	@Column(name = "evd")
	private String evd;
	
	@Column(name = "user_id")
	private String user_id;
	
	@Column(name = "request_time")
	private Timestamp request_time;
	
	@Column(name = "created_date")
	private Timestamp created_date;
	
	@Column(name = "response_time")
	private Timestamp response_time;
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

    public String getResultCode(){
    	return result_code;
    }
    
    public void setResultCode(String result_code){
    	this.result_code = result_code;
    }

    public String getApiPath(){
    	return api_path;
    }
    
    public void setApiPath(String api_path){
    	this.api_path = api_path;
    }

    public String getUserId(){
    	return user_id;
    }
    
    public void setUserId(String user_id){
    	this.user_id = user_id;
    }

    public String getEvd(){
    	return evd;
    }
    
    public void setEvd(String evd){
    	this.evd = evd;
    }

    public Timestamp getCreatedDate(){
    	return created_date;
    }
    
    public void setCreatedDate(Timestamp created_date){
    	this.created_date = created_date;
    }
    
    public Timestamp getRequestTime(){
    	return request_time;
    }
    
    public void setRequestTime(Timestamp request_time){
    	this.request_time = request_time;
    }
    public Timestamp getResponseTime(){
    	return response_time;
    }
    
    public void setResponseTime(Timestamp response_time){
    	this.response_time = response_time;
    }
}