package com.esahai.dbconnect.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="access_roles")
public class AccessRoles implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@Column(name = "service")
	private String service;

	@Column(name = "component")
	private String component;
	
	@Column(name = "access")
	private String access;
		
	@Column(name = "requestor")
	private String requestor;
	
	@Column(name = "roleId")
	private int roleId;

	@Column(name = "filters")
	private String filters;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getService(){
		return service;
	}
	
	public void setService(String service){
		this.service=service;
	}
	
	public String getComponent(){
		return component;
	}
	
	public void setComponent(String component){
		this.component=component;
	}
		
	public String getAccess(){
		return access;
	}
	public void setAccess(String access){
		this.access=access;
	}
	
	public String getRequestor() {
		return requestor;
	}

	public void setRequestor(String requestor) {
		this.requestor = requestor;
	}
    
	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
    
	public String getFilters() {
		return filters;
	}

	public void setFilters(String filters) {
		this.filters = filters;
	}

}