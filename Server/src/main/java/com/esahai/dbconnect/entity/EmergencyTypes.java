package com.esahai.dbconnect.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="emergency_types")
public class EmergencyTypes implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	
	@Column(name = "type_name")
	private String type_name;
	
	@Column(name = "updated_date")
	private Timestamp updated_date;
	
	@Column(name = "created_date")
	private Timestamp created_date;
	
	@Column(name = "status")
	private String status;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
        
    public String getStatus(){
    	return status;
    }
    
    public void setStatus(String status){
    	this.status = status;
    }

    public String getTypeName(){
    	return type_name;
    }
    
    public void setTypeName(String type_name){
    	this.type_name = type_name;
    }
    
    public Timestamp getCreatedDate(){
    	return created_date;
    }
    
    public void setCreatedDate(Timestamp created_date){
    	this.created_date = created_date;
    }
    
    public Timestamp getUpdatedDate(){
    	return updated_date;
    }
    
    public void setUpdatedDate(Timestamp updated_date){
    	this.updated_date = updated_date;
    }
    
}