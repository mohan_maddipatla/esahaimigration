package com.esahai.dbconnect.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="driver_master")
public class DriverMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "driver_id")
	private int driver_id;
	
	@Column(name = "group_id")
	private int group_id;
	
	@Column(name = "driver_mobile_number")
	private String driver_mobile_number;
	
	@Column(name = "driver_name")
	private String driver_name;
	
	@Column(name = "driver_email")
	private String driver_email;
	
	@Column(name = "driver_pin")
	private String driver_pin;
	
	@Column(name = "license_number")
	private String license_number;
	
	@Column(name = "updated_date")
	private Timestamp updated_date;
	
	@Column(name = "created_date")
	private Timestamp created_date;
	
	@Column(name = "file_url")
	private String file_url;
	
	@Column(name = "is_active")
	private String is_active;
	
	public int getDriverId() {
		return driver_id;
	}

	public void setDriverId(int driver_id) {
		this.driver_id = driver_id;
	}
    
	public int getGroupId() {
		return group_id;
	}

	public void setGroupId(int group_id) {
		this.group_id = group_id;
	}
    
    public String getActiveStatus(){
    	return is_active;
    }
    
    public void setActiveStatus(String is_active){
    	this.is_active = is_active;
    }

    public String getDriverName(){
    	return driver_name;
    }
    
    public void setDriverName(String driver_name){
    	this.driver_name = driver_name;
    }

    public String getDriverMobileNumber(){
    	return driver_mobile_number;
    }
    
    public void setDriverMobileNumber(String driver_mobile_number){
    	this.driver_mobile_number = driver_mobile_number;
    }

    public String getDriverEmail(){
    	return driver_email;
    }
    
    public void setDriverEmail(String driver_email){
    	this.driver_email = driver_email;
    }
    
    public String getLicenseNumber(){
    	return license_number;
    }
    
    public void setLicenseNumber(String license_number){
    	this.license_number = license_number;
    }
    
    public String getDriverPin(){
    	return driver_pin;
    }
    
    public void setDriverPin(String driver_pin){
    	this.driver_pin = driver_pin;
    }
    
    public Timestamp getCreatedDate(){
    	return created_date;
    }
    
    public void setCreatedDate(Timestamp created_date){
    	this.created_date = created_date;
    }
    
    public Timestamp getUpdatedDate(){
    	return updated_date;
    }
    
    public void setUpdatedDate(Timestamp updated_date){
    	this.updated_date = updated_date;
    }

    public String getFileUrl(){
    	return file_url;
    }
    
    public void setFileUrl(String file_url){
    	this.file_url = file_url;
    }

}