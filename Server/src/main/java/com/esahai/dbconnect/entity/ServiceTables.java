package com.esahai.dbconnect.entity;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="servicetables")

public class ServiceTables implements Serializable{
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@Column(name = "serviceId")
	private int serviceId;
	
	@Column(name = "tableName")
	private String tableName;
		
	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id=id;
	}
	
	public int getServiceId(){
		return serviceId;
	}
	
	public void setServiceID(int id){
		this.serviceId=id;
	}
	
	public String getTableName(){
		return tableName;
	}
	
	public void setTableName(String name){
		this.tableName=name;
	}
}
