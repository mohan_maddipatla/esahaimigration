package com.esahai.dbconnect.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity
@Table(name="blood_bank")
public class BloodBank implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "contact_number_1")
	private long contactNumber;
	
	@Column(name = "contact_number_2")
	private Integer contactNumber2;
	
	@Column(name = "contact_number_3")
	private Integer contactNumber3;

	@Column(name = "website")
	private String website;

	@Column(name = "address")
	private String address;
	
	@Column(name = "latitude")
	private BigDecimal latitude;
	
	@Column(name = "longitude")
	private BigDecimal longitude;
	
	@Column(name = "geohash")
	private String geohash;
	
	@Column(name = "created_date")
	private Timestamp created_date;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getContactNumber() {
		return (int) (contactNumber);
	}
	
	public Integer getContactNumber2() {
		return contactNumber2;
	}
	public Integer getContactNumber3() {
		return contactNumber3;
	}

	public void setContactNumber(long contactNumber) {
		this.contactNumber = contactNumber;
	}
	
	public void setContactNumber2(Integer contactNumber) {
		this.contactNumber2 = contactNumber;
	}
	
	public void setContactNumber3(Integer contactNumber) {
		this.contactNumber3 = contactNumber;
	}
	
	
	public void setLongitude(BigDecimal longitude){
		this.longitude=longitude;
	}
	
	public BigDecimal getLongitude(){
		return longitude;
	}

	
	public void setLatitude(BigDecimal latitude){
		this.latitude=latitude;
	}
	
	public BigDecimal getLatitude(){
		return latitude;
	}
	
	public String getGeohash() {
		return geohash;
	}

	public void setGeohash(String geohash) {
		this.geohash = geohash;
	}
	
	public Timestamp getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Timestamp created_date) {
		this.created_date = created_date;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}