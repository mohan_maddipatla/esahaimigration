package com.esahai.dbconnect.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="services")
public class Services implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@Column(name = "service_type_id")
	private String service_type_id;

	@Column(name = "name")
	private String name;
	
	@Column(name = "label")
	private String label;
	
	@Column(name = "description")
	private String description;

	@Column(name = "isActive")
	private String isActive;
	
	@Column(name = "isDbservice")
	private String isDbService;
	
	@Column(name = "isDefault")
	private String isDefault;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getService_type_id() {
		return service_type_id;
	}
	
	public void setService_type_id(String id) {
		this.service_type_id = id;
	}
	
	public void setLabel(String label){
		this.label=label;
	}
	
	public String getLabel(){
		return label;
	}

	public void setDescription(String desc){
		this.description=desc;
	}
	
	public String getDescription(){
		return description;
	}
	
	public String getIsActive() {
		return isActive;
	}

	public void setisActive(String is) {
		this.isActive = is;
	}
	
	public String getIsDbService() {
		return isDbService;
	}

	public void setIsDbService(String service) {
		this.isDbService = service;
	}

	public String getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(String defau) {
		this.isDefault = defau;
	}
}