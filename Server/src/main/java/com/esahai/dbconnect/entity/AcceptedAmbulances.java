package com.esahai.dbconnect.entity;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="accepted_ambulances")
public class AcceptedAmbulances implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "ambulance_id")
	private Integer ambulance_id;
	
	@Column(name = "booking_id")
	private BigInteger booking_id;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

    public Integer getAmbulance_id() {
		return ambulance_id;
	}
	
	public BigInteger getBooking_id() {
		return booking_id;
	}
	
	public void setAmbulance_id(Integer ambulance_id) {
		this.ambulance_id = ambulance_id;
	}
	
	public void setBooking_id(BigInteger booking_id) {
		this.booking_id = booking_id;
	}
	
}