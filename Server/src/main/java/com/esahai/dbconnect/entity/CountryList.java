package com.esahai.dbconnect.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="country_list")
public class CountryList implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
    
	@Column(name = "country")
	private String country;
	
	@Column(name = "code")
	private String code;
	
	@Column(name = "created_datetime")
	private Timestamp created_datetime;
	
	@Column(name = "updated_datetime")
	private Timestamp updated_datetime;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

    public String getCountry(){
    	return country;
    }
    
    public void setCountry(String country){
    	this.country = country;
    }

    public String getCode(){
    	return code;
    }
    
    public void setCode(String code){
    	this.code = code;
    }
    
    public Timestamp getCreatedDateTime(){
    	return created_datetime;
    }
    
    public void setCreatedDatetime(Timestamp created_datetime){
    	this.created_datetime = created_datetime;
    }
    
    public Timestamp getUpdatedDateTime(){
    	return updated_datetime;
    }
    
    public void setUpdatedDatetime(Timestamp updated_datetime){
    	this.updated_datetime = updated_datetime;
    }
    
}