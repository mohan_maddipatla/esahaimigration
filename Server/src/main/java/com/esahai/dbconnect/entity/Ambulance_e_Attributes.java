package com.esahai.dbconnect.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ambulance_e_attributes")
public class Ambulance_e_Attributes implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@Column(name = "ambulance_id")
	private int ambulance_id;
	
	@Column(name = "emergency_type_id")
	private int emergency_type_id;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	    
	public int getAmbulanceId() {
		return ambulance_id;
	}

	public void setAmbulanceId(int ambulance_id) {
		this.ambulance_id = ambulance_id;
	}
    
	public int getEmergency_type_Id() {
		return emergency_type_id;
	}

	public void setEmergency_type_Id(int emergency_type_id) {
		this.emergency_type_id = emergency_type_id;
	}
    
}