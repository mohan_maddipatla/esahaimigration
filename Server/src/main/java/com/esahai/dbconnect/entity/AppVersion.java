package com.esahai.dbconnect.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="app_version")
public class AppVersion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@Column(name = "version_no")
	private String version_no;
	
	@Column(name = "build_os")
	private String build_os;
	
	@Column(name = "app_type")
	private String app_type;
	
	@Column(name = "build_date")
	private Timestamp build_date;
	
	@Column(name = "force_upgrade")
	private short force_upgrade;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

    public String getVersionNo(){
    	return version_no;
    }
    
    public void setVersionNo(String version_no){
    	this.version_no = version_no;
    }

    public String getBuildOS(){
    	return build_os;
    }
    
    public void setBuildOS(String build_os){
    	this.build_os = build_os;
    }
    public String getAppType(){
    	return app_type;
    }
    
    public void setAppType(String app_type){
    	this.app_type = app_type;
    }

    public Timestamp getBuildDate(){
    	return build_date;
    }
    
    public void setBuildDate(Timestamp build_date){
    	this.build_date = build_date;
    }
    public short getForceUpgrade(){
    	return force_upgrade;
    }
    
    public void setForceUpgrade(short force_upgrade){
    	this.force_upgrade = force_upgrade;
    }

}