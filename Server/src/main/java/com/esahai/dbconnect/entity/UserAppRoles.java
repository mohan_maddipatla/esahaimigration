package com.esahai.dbconnect.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="user_app_roles")
public class UserAppRoles implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	
	@Column(name ="user_id")
	private String user_id;
	
	@Column(name = "app_id")
	private int app_id;
	
	@Column(name ="role_id")
	private String role_id;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getUser_id(){
		return user_id;
	}
	
	public void setUser_id(String id){
		this.user_id=id;
	}
	
	public int getApp_id() {
		return app_id;
	}

	public void setApp_id(int id) {
		this.app_id = id;
	}
	
	public String getRole_id(){
		return role_id;
	}
	
	public void setRole_id(String id){
		this.role_id=id;
	}
}
