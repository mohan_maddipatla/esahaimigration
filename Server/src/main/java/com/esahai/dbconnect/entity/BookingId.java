package com.esahai.dbconnect.entity;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="booking_id")
public class BookingId implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "booking_id")
	private BigInteger booking_id;

	public BigInteger getBookingId() {
		return booking_id;
	}

	public void setBookingId(BigInteger booking_id) {
		this.booking_id = booking_id;
	}

}