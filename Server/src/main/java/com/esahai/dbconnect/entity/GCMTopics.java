package com.esahai.dbconnect.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="gcm_topics")
public class GCMTopics implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	
	@Column(name = "topic_name")
	private String topic_name;
	
	@Column(name = "gcm_certificate")
	private String gcm_certificate;
	
	@Column(name = "topic_short_name")
	private String topic_short_name;
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
        
    public String getTopicName(){
    	return topic_name;
    }
    
    public void setTopicName(String topic_name){
    	this.topic_name= topic_name;
    }
    
    public String getGCMCertificate(){
    	return gcm_certificate;
    }
    
    public void setGCMCertificate(String gcm_certificate){
    	this.gcm_certificate= gcm_certificate;
    }

    public String getTopicShortName(){
    	return topic_short_name;
    }
    
    public void setTopicShortName(String topic_short_name){
    	this.topic_short_name = topic_short_name;
    }
       
}