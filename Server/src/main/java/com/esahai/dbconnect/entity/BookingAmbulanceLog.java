package com.esahai.dbconnect.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="booking_ambulance_log")
public class BookingAmbulanceLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
    
	@Column(name = "ambulance_ids")
	private String ambulance_ids;

	@Column(name = "ambulance_numbers")
	private String ambulance_numbers;
	
	@Column(name = "booking_id")
	private BigInteger booking_id;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BigInteger getBookingId() {
		return booking_id;
	}

	public void setBookingId(BigInteger booking_id) {
		this.booking_id = booking_id;
	}

    public String getAmbulanceIds(){
    	return ambulance_ids;
    }
    
    public void setAmbulanceIds(String ambulance_ids){
    	this.ambulance_ids = ambulance_ids;
    }

    public String getAmbulanceNumbers(){
    	return ambulance_numbers;
    }
    
    public void setAmbulanceNumbers(String ambulance_numbers){
    	this.ambulance_numbers = ambulance_numbers;
    }
    
}