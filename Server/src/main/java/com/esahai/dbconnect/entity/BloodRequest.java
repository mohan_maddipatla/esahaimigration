package com.esahai.dbconnect.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="blood_request")
public class BloodRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
    
	@Column(name = "customer_id")
	private int customer_id;

	@Column(name = "name")
	private String name;
	
	@Column(name = "contact")
	private String contact;
	
	@Column(name = "blood_group")
	private String blood_group;
	
	@Column(name = "quantity")
	private String quantity;
	
	@Column(name = "latitude")
	private String latitude;
	
	@Column(name = "longitude")
	private String longitude;
	
	@Column(name = "location")
	private String location;
	
	@Column(name = "geohash")
	private String geohash;
	
	@Column(name = "time")
	private Timestamp time;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCustomerId() {
		return customer_id;
	}

	public void setCustomerId(int customer_id) {
		this.customer_id = customer_id;
	}

    public String name(){
    	return name;
    }
    
    public void setName(String name){
    	this.name = name;
    }

    public String getContact(){
    	return contact;
    }
    
    public void setContact(String contact){
    	this.contact = contact;
    }
    
    public String getBloodGroup(){
    	return blood_group;
    }
    
    public void setBloodGroup(String blood_group){
    	this.blood_group = blood_group;
    }

    public String getLocation(){
    	return location;
    }
    
    public void setLocation(String location){
    	this.location = location;
    }
    
    public String getLatitude(){
    	return latitude;
    }
    
    public void setLongitude(String longitude){
    	this.longitude = longitude;
    }
    
    public String getLongitude(){
    	return longitude;
    }
    
    public void setLatitude(String latitude){
    	this.latitude = latitude;
    }

    public Timestamp getTime(){
    	return time;
    }
    
    public void setTime(Timestamp time){
    	this.time = time;
    }
    
    public String getQuantity(){
    	return quantity;
    }
    
    public void setQuantity(String quantity){
    	this.quantity = quantity;
    }
    
    public String getGeohash(){
    	return geohash;
    }
    
    public void setGeohash(String geohash){
    	this.geohash = geohash;
    }
}