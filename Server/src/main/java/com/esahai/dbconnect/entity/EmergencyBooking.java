package com.esahai.dbconnect.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="emergency_booking")
public class EmergencyBooking implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "booking_id")
	private Long bookingId;

	@Column(name = "customer_mobile")
	private String customerMobile;

	@Column(name = "customer_name")
	private String customerName;

	@Column(name = "booked_by")
	private String bookedBy;

	@Column(name = "emergency_type")
	private String emergencyType;

	@Column(name = "emergency_longitude")
	private String emergencyLontitude;

	@Column(name = "emergency_latitude")
	private String emergencyLatitude;

	@Column(name = "ambulance_id")
	private Integer ambulanceId;

	@Column(name = "hospital")
	private String hospital;

	@Column(name = "ambulance_start_longitude")
	private String ambulanceStartLongitude;

	@Column(name = "ambulance_start_latitude")
	private String ambulanceStartLatitude;

	@Column(name = "created_datetime")
	private Date createdDateTime;

	@Column(name = "updated_datetime")
	private Date updatedDateTime;

	@Column(name = "distance")
	private String distance;

	@Column(name = "duration")
	private String duration;

	@Column(name = "cost")
	private String cost;

	@Column(name = "emergency_geoHashCode")
	private String emergencyGeoHashCode;

	@Column(name = "ambulance_start_geoHashCode")
	private String ambulanceStartGeoHashCode;

	@Column(name = "booking_status")
	private String bookingStatus;

	@Column(name = "customer_id")
	private String customerId;

	@Column(name = "eme_address")
	private String emeAddress;

	@Column(name = "ambulance_number")
	private String ambulanceNumber;

	@Column(name = "group_id")
	private String groupId;

	@Column(name = "group_type_id")
	private String groupTypeId;

	@Column(name = "emergency_type_id")
	private String emergencyTypeId;

	@Column(name = "is_self")
	private Integer isSelf;

	@Column(name = "patient_number")
	private String patientNumber;

	@Column(name = "reason")
	private String reason;

	@Column(name = "drop_latitude")
	private String dropLatitude;

	@Column(name = "drop_longitude")
	private String dropLongitude;

	@Column(name = "drop_geoHashCode")
	private String dropGeoHashCode;

	@Column(name = "pickup_datetime")
	private Date pickupDatetime;

	@Column(name = "assistance")
	private String assistance;

	public Long getBookingId() {
		return bookingId;
	}

	public void setBookingId(Long bookingId) {
		this.bookingId = bookingId;
	}

	public String getCustomerMobile() {
		return customerMobile;
	}

	public void setCustomerMobile(String customerMobile) {
		this.customerMobile = customerMobile;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getBookedBy() {
		return bookedBy;
	}

	public void setBookedBy(String bookedBy) {
		this.bookedBy = bookedBy;
	}

	public String getEmergencyType() {
		return emergencyType;
	}

	public void setEmergencyType(String emergencyType) {
		this.emergencyType = emergencyType;
	}

	public String getEmergencyLontitude() {
		return emergencyLontitude;
	}

	public void setEmergencyLontitude(String emergencyLontitude) {
		this.emergencyLontitude = emergencyLontitude;
	}

	public String getEmergencyLatitude() {
		return emergencyLatitude;
	}

	public void setEmergencyLatitude(String emergencyLatitude) {
		this.emergencyLatitude = emergencyLatitude;
	}

	public Integer getAmbulanceId() {
		return ambulanceId;
	}

	public void setAmbulanceId(Integer ambulanceId) {
		this.ambulanceId = ambulanceId;
	}

	public String getHospital() {
		return hospital;
	}

	public void setHospital(String hospital) {
		this.hospital = hospital;
	}

	public String getAmbulanceStartLongitude() {
		return ambulanceStartLongitude;
	}

	public void setAmbulanceStartLongitude(String ambulanceStartLongitude) {
		this.ambulanceStartLongitude = ambulanceStartLongitude;
	}

	public String getAmbulanceStartLatitude() {
		return ambulanceStartLatitude;
	}

	public void setAmbulanceStartLatitude(String ambulanceStartLatitude) {
		this.ambulanceStartLatitude = ambulanceStartLatitude;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}

	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getEmergencyGeoHashCode() {
		return emergencyGeoHashCode;
	}

	public void setEmergencyGeoHashCode(String emergencyGeoHashCode) {
		this.emergencyGeoHashCode = emergencyGeoHashCode;
	}

	public String getAmbulanceStartGeoHashCode() {
		return ambulanceStartGeoHashCode;
	}

	public void setAmbulanceStartGeoHashCode(String ambulanceStartGeoHashCode) {
		this.ambulanceStartGeoHashCode = ambulanceStartGeoHashCode;
	}

	public String getBookingStatus() {
		return bookingStatus;
	}

	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getEmeAddress() {
		return emeAddress;
	}

	public void setEmeAddress(String emeAddress) {
		this.emeAddress = emeAddress;
	}

	public String getAmbulanceNumber() {
		return ambulanceNumber;
	}

	public void setAmbulanceNumber(String ambulanceNumber) {
		this.ambulanceNumber = ambulanceNumber;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getGroupTypeId() {
		return groupTypeId;
	}

	public void setGroupTypeId(String groupTypeId) {
		this.groupTypeId = groupTypeId;
	}

	public String getEmergencyTypeId() {
		return emergencyTypeId;
	}

	public void setEmergencyTypeId(String emergencyTypeId) {
		this.emergencyTypeId = emergencyTypeId;
	}

	public Integer getIsSelf() {
		return isSelf;
	}

	public void setIsSelf(Integer isSelf) {
		this.isSelf = isSelf;
	}

	public String getPatientNumber() {
		return patientNumber;
	}

	public void setPatientNumber(String patientNumber) {
		this.patientNumber = patientNumber;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getDropLatitude() {
		return dropLatitude;
	}

	public void setDropLatitude(String dropLatitude) {
		this.dropLatitude = dropLatitude;
	}

	public String getDropLongitude() {
		return dropLongitude;
	}

	public void setDropLongitude(String dropLongitude) {
		this.dropLongitude = dropLongitude;
	}

	public String getDropGeoHashCode() {
		return dropGeoHashCode;
	}

	public void setDropGeoHashCode(String dropGeoHashCode) {
		this.dropGeoHashCode = dropGeoHashCode;
	}

	public Date getPickupDatetime() {
		return pickupDatetime;
	}

	public void setPickupDatetime(Date pickupDatetime) {
		this.pickupDatetime = pickupDatetime;
	}

	public String getAssistance() {
		return assistance;
	}

	public void setAssistance(String assistance) {
		this.assistance = assistance;
	}
	
	public String toString() {
		return "Booking Id = " + this.bookingId + ", Booking Status = " + this.bookingStatus + ", cost = " + this.cost;
	}
}