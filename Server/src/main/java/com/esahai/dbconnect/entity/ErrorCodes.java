package com.esahai.dbconnect.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="error_codes")
public class ErrorCodes implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	
	@Column(name = "apiName")
	private String apiName;
	
	@Column(name = "apiCode")
	private String apiCode;
	
	@Column(name = "errorCode")
	private String errorCode;
	
	@Column(name = "errorDesc")
	private String errorDesc;
	
	@Column(name = "displayText")
	private String displayText;
	
	@Column(name = "comment")
	private String comment;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
        
    public String getApiName(){
    	return apiName;
    }
    
    public void setApiName(String apiName){
    	this.apiName = apiName;
    }

    public String getApiCode(){
    	return apiCode;
    }
    
    public void setApiCode(String apiCode){
    	this.apiCode = apiCode;
    }
    
    public String getErrorCode(){
    	return errorCode;
    }
    
    public void setErrorCode(String errorCode){
    	this.errorCode = errorCode;
    }
    
    public String getErrorDesc(){
    	return errorDesc;
    }
    
    public void setErrorDesc(String errorDesc){
    	this.errorDesc = errorDesc;
    }
    
    public String getDisplayText(){
    	return displayText;
    }
    
    public void setDisplayText(String displayText){
    	this.displayText = displayText;
    }
    
    public String getComment(){
    	return comment;
    }
    
    public void setComment(String comment){
    	this.comment = comment;
    }
    
}