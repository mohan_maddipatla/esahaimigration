package com.esahai.dbconnect.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="customer_master")
public class CustomerMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "customer_id")
	private int customer_id;
	
	@Column(name = "pincode")
	private int pincode;
    
	@Column(name = "age")
	private int age;
    
	@Column(name = "customer_mobile_number")
	private String customer_mobile_number;
	
	@Column(name = "customer_name")
	private String customer_name;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "state")
	private String state;
	
	@Column(name = "city")
	private String city;
	
	@Column(name = "device_imei")
	private String device_imei;
	
	@Column(name = "device_os")
	private String device_os;
	
	@Column(name = "device_type")
	private String device_type;
	
	@Column(name = "device_token")
	private String device_token;
	
	@Column(name = "is_verified")
	private String is_verified;
	
	@Column(name = "sms_status")
	private String sms_status;
	
	@Column(name = "loginStatus")
	private String loginstatus;
	
	@Column(name = "updated_date")
	private Timestamp updated_date;
	
	@Column(name = "created_date")
	private Timestamp created_date;
	
	@Column(name = "file_url")
	private String file_url;
	
	@Column(name = "token")
	private String token;
	
	@Column(name = "gender")
	private String gender;
	
	@Column(name = "new_user")
	private String new_user;
	
	@Column(name = "come_to_know_by")
	private String come_to_know_by;
	
	@Column(name = "blood_group")
	private String blood_group;
	
	@Column(name = "app_version")
	private String app_version;
	
	@Column(name = "otp")
	private String otp;
	
	@Column(name = "is_active")
	private String is_active;
	
	@Column(name = "customer_email")
	private String customer_email;
	
	@Column(name = "donating_blood")
	private short donating_blood;
	
	public int getCustomerId() {
		return customer_id;
	}

	public void setCustomerId(int customer_id) {
		this.customer_id = customer_id;
	}
    
	public int getPincode() {
		return pincode;
	}

	public void setPincode(int pincode) {
		this.pincode = pincode;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

    public String getActiveStatus(){
    	return is_active;
    }
    
    public void setActiveStatus(String is_active){
    	this.is_active = is_active;
    }

    public String getOTP(){
    	return otp;
    }
    
    public void setOTP(String otp){
    	this.otp = otp;
    }
    
    public String getCustomerName(){
    	return customer_name;
    }
    
    public void setCustomerName(String customer_name){
    	this.customer_name = customer_name;
    }

    public String getCustomerMobileNumber(){
    	return customer_mobile_number;
    }
    
    public void setCustomerMobileNumber(String customer_mobile_number){
    	this.customer_mobile_number = customer_mobile_number;
    }

    public String getCustomerEmail(){
    	return customer_email;
    }
    
    public void setCustomerEmail(String customer_email){
    	this.customer_email = customer_email;
    }
    public String getAddress(){
    	return address;
    }
    
    public void setAddress(String address){
    	this.address = address;
    }
    public String getState(){
    	return state;
    }
    
    public void setState(String state){
    	this.state = state;
    }
    public String getCity(){
    	return city;
    }
    
    public void setCity(String city){
    	this.city = city;
    }
    
    public String getDeviceImei(){
    	return device_imei;
    }
    
    public void setDeviceOS(String device_os){
    	this.device_os = device_os;
    }
    public String getDeviceOS(){
    	return device_os;
    }
    
    public void setDeviceImei(String device_imei){
    	this.device_imei = device_imei;
    }
    
    public String getDeviceType(){
    	return device_type;
    }
    
    public void setDeviceType(String device_type){
    	this.device_type = device_type;
    }
    
    public String getDeviceToken(){
    	return device_token;
    }
    
    public void setDeviceToken(String device_token){
    	this.device_token = device_token;
    }
    
    public Timestamp getCreatedDate(){
    	return created_date;
    }
    
    public void setCreatedDate(Timestamp created_date){
    	this.created_date = created_date;
    }
    
    public Timestamp getUpdatedDate(){
    	return updated_date;
    }
    
    public void setUpdatedDate(Timestamp updated_date){
    	this.updated_date = updated_date;
    }

    public String getVerificationStatus(){
    	return is_verified;
    }
    
    public void setVerificationStatus(String is_verified){
    	this.is_verified = is_verified;
    }

    public String getLoginStatus(){
    	return loginstatus;
    }
    
    public void setLoginStatus(String loginstatus){
    	this.loginstatus = loginstatus;
    }
    
    public String getSMSStatus(){
    	return sms_status;
    }
    
    public void setSMSStatus(String sms_status){
    	this.sms_status = sms_status;
    }
    public String getComeToKnowBy(){
    	return come_to_know_by;
    }
    
    public void setComeToKnowBy(String come_to_know_by){
    	this.come_to_know_by = come_to_know_by;
    }

    public String getToken(){
    	return token;
    }
    
    public void setToken(String token){
    	this.token = token;
    }
    public String getGender(){
    	return gender;
    }
    
    public void setGender(String gender){
    	this.gender = gender;
    }
    public String getNewUser(){
    	return new_user;
    }
    
    public void setNewUser(String new_user){
    	this.new_user = new_user;
    }

    public String getFileUrl(){
    	return file_url;
    }
    
    public void setFileUrl(String file_url){
    	this.file_url = file_url;
    }

    public short getDonatingBlood(){
    	return donating_blood;
    }
    
    public void setDonatingBlood(short donating_blood){
    	this.donating_blood = donating_blood;
    }
    public String getBloodGroup(){
    	return blood_group;
    }
    
    public void setBloodGroup(String blood_group){
    	this.blood_group = blood_group;
    }
    public String getAppVersion(){
    	return app_version;
    }
    
    public void setAppVersion(String app_version){
    	this.app_version = app_version;
    }

}