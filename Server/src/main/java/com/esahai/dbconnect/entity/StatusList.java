package com.esahai.dbconnect.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="status_list")
public class StatusList implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@Column(name = "status")
	private String status;
	
	@Column(name = "short_code")
	private String short_code;
	
	@Column(name = "created_date")
	private Timestamp created_date;
	
	@Column(name = "updated_date")
	private Timestamp updated_date;
	
	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id=id;
	}
	
	public String getStatus(){
		return status;
	}
	
	public void setStatus(String status){
		this.status=status;
	}
	
	public String getShort_code(){
		return short_code;
	}
	
	public void setShort_code(String code){
		this.short_code=code;
	}
	
	public Timestamp getCreated_date(){
		return created_date;
	}
	
	public void setCreated_date(Timestamp date){
		this.created_date=date;
	}
	
	public Timestamp getUpdated_date(){
		return updated_date;
	}
	
	public void setUpdated_date(Timestamp date){
		this.updated_date=date;
	}
}
