package com.esahai.dbconnect.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ambulance_attributes")
public class AmbulanceAttributes implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@Column(name = "attribute_name")
	private String attribute_name;

	@Column(name = "attribute_details")
	private String attribute_details;
	
	@Column(name = "ambulance_id")
	private int ambulance_id;
	
	@Column(name = "ambulance_e_type_id")
	private int ambulance_e_type_id;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getAttributeName(){
		return attribute_name;
	}
	
	public void setAttributeName(String attribute_name){
		this.attribute_name=attribute_name;
	}
	
	public String getAttributeDetails(){
		return attribute_details;
	}
	
	public void setAttributeDetails(String attribute_details){
		this.attribute_details=attribute_details;
	}
	    
	public int getAmbulanceId() {
		return ambulance_id;
	}

	public void setAmbulanceId(int ambulance_id) {
		this.ambulance_id = ambulance_id;
	}
    
	public int getAmbulance_e_type_Id() {
		return ambulance_e_type_id;
	}

	public void setAmbulance_e_type_Id(int ambulance_e_type_id) {
		this.ambulance_e_type_id = ambulance_e_type_id;
	}
    
	
	
}