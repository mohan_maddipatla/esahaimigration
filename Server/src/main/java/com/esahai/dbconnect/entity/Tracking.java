package com.esahai.dbconnect.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tracking")

public class Tracking implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	
	@Column(name = "ambulance_id")
	private int ambulance_id;
	
	@Column(name = "current_status")
	private String current_status;
	
	@Column(name = "driver_id")
	private int driver_id;
	
	@Column(name = "speed")
	private String speed;
	
	@Column(name = "longitude")
	private String longitude;
	
	@Column(name = "latitude")
	private String latitude;
	
	@Column(name = "created_date")
	private Timestamp created_date;
	
	@Column(name = "geoHashCode")
	private String geoHashCode;
	
	@Column(name = "is_booked")
	private byte is_booked;
	
	@Column(name = "booking_id")
	private long booking_id;
	
	@Column(name = "booking_status")
	private String booking_status;
	
	@Column(name = "end_longitude")
	private String end_longitude;
	
	@Column(name = "end_latitude")
	private String end_latitude;
	
	@Column(name = "end_geoHashCode")
	private String end_geoHashCode;
	
	@Column(name = "estimated_time_arrival")
	private String estimated_time_arrival;
	
	@Column(name = "emergency_longitude")
	private String emergency_longitude;
	
	@Column(name = "emergency_latitude")
	private String emergency_latitude;
	
	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id=id;
	}
	
	public int getAmbulance_id(){
		return ambulance_id;
	}
	
	public void setAmbulance_id(int id){
		this.ambulance_id=id;
	}
	
	public String getCurrent_status(){
		return current_status;
	}
	
	public void setCurrent_status(String status){
		this.current_status=status;
	}
	
	public int getDriver_id(){
		return driver_id;
	}
	
	public void setDriver_id(int driver_id){
		this.driver_id=driver_id;
	}
	
	public String getSpeed(){
		return speed;
	}
	
	public void setSpeed(String speed){
		this.speed=speed;
	}
	
	public String getLongitude(){
		return longitude;
	}
	
	public void setLongitude(String longitude){
		this.longitude=longitude;
	}
	
	public String getLlatitude(){
		return latitude;
	}
	
	public void setlatitude(String latitude){
		this.latitude=latitude;
	}
	
	public Timestamp getCreated_date(){
		return created_date;
	}
	
	public void setCreated_date(Timestamp date){
		this.created_date=date;
	}
	
	public String getGeoHashCode(){
		return geoHashCode;
	}
	
	public void setGeoHashCode(String code){
		this.geoHashCode=code;
	}
	
	public byte getIs_booked(){
		return is_booked;
	}
	
	public void setIs_booked(byte book){
		this.is_booked=book;
	}
	
	public long getBooking_id(){
		return booking_id;
	}
	
	public void setBooking_id(long id){
		this.booking_id=id;
	}
	
	public String getBooking_status(){
		return booking_status;
	}
	
	public void setBooking_status(String status){
		this.booking_status=status;
	}
	
	public String getEnd_longitude(){
		return end_longitude;
	}
	
	public void setEnd_longitude(String longitude){
		this.end_longitude=longitude;
	}
	
	public String getEnd_latitude(){
		return end_latitude;
	}
	
	public void setEnd_latitude(String latitude){
		this.end_latitude=latitude;
	}
	
	public String getEnd_geoHashCode(){
		return end_geoHashCode;
	}
	
	public void setEnd_geoHashCode(String code){
		this.end_geoHashCode=code;
	}
	
	public String getEstimated_time_arrival(){
		return estimated_time_arrival;
	}
	
	public void setEstimated_time_arrival(String time){
		this.estimated_time_arrival=time;
	}
	
	public String getEmergency_longitude(){
		return emergency_longitude;
	}
	
	public void setEmergency_longitude(String longitude){
		this.emergency_longitude=longitude;
	}
	
	public String getEmergency_latitude(){
		return emergency_latitude;
	}
	
	public void setEmergency_latitude(String latitude){
		this.emergency_latitude=latitude;
	}
	
	
}
