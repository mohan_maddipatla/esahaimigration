package com.esahai.dbconnect.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="apps")
public class App implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@Column(name = "api_key")
	private String api_key;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "app_name")
	private String app_name;
	
	@Column(name = "app_live")
	private String app_live;
	
	@Column(name = "isActive")
	private String isActive;
	
	@Column(name = "isDefault")
	private String isDefault;
	
	@Column(name = "role_ids")
	private String role_ids;
	
	@Column(name = "createDateTime")
	private Timestamp create_datetime;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

    public String description(){
    	return description;
    }
    
    public void setDescription(String description){
    	this.description = description;
    }

    public String getAppName(){
    	return app_name;
    }
    
    public void setAppName(String app_name){
    	this.app_name = app_name;
    }
    
    public String getAppLive(){
    	return app_live;
    }
    
    public void setAppLive(String app_live){
    	this.app_live = app_live;
    }

    public String getApiKey(){
    	return api_key;
    }
    
    public void setApiKey(String api_key){
    	this.api_key = api_key;
    }
    
    public String getActiveStatus(){
    	return isActive;
    }
    
    public void setActiveStatus(String isActive){
    	this.isActive = isActive;
    }
    
    public String getDefaultStatus(){
    	return isDefault;
    }
    
    public void setDefaultStatus(String isDefault){
    	this.isDefault = isDefault;
    }

    public Timestamp getCreatedDateTime(){
    	return create_datetime;
    }
    
    public void setCreatedDateTime(Timestamp created_datetime){
    	this.create_datetime = created_datetime;
    }
    
    public String getRoleIds(){
    	return role_ids;
    }
    
    public void setRoleIds(String role_ids){
    	this.role_ids = role_ids;
    }

}