package com.esahai.dbconnect.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="customer_attributes")
public class CustomerAttributes implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
    
	@Column(name = "attribute_name")
	private String attribute_name;
	
	@Column(name = "attribute_details")
	private String attribute_details;
	
	@Column(name = "customer_id")
	private int customer_id;
	
	@Column(name = "customer_e_type_id")
	private int customer_e_type_id;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

    public String getAttributeName(){
    	return attribute_name;
    }
    
    public void setAttributeName(String attribute_name){
    	this.attribute_name = attribute_name;
    }

    public String getAttributeDetails(){
    	return attribute_details;
    }
    
    public void setAttributeDetails(String attribute_details){
    	this.attribute_details = attribute_details;
    }
    
    public int getCustomerId(){
    	return customer_id;
    }
    
    public void setCustomerId(int customer_id){
    	this.customer_id = customer_id;
    }
    
    public int getCustomerETypeId(){
    	return customer_e_type_id;
    }
    
    public void setCustomerETypeId(int customer_e_type_id){
    	this.customer_e_type_id = customer_e_type_id;
    }
    
}