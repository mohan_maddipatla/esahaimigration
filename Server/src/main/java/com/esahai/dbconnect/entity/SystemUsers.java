package com.esahai.dbconnect.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="system_users")
public class SystemUsers implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "userId")
	private int userId;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "firstName")
	private String firstName;
	
	@Column(name = "lastName")
	private String lastName;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "displayName")
	private String displayName;
	
	@Column(name = "isActive")
	private String isActive;
	
	@Column(name = "createDateTime")
	private Timestamp	createDateTime;
	
	@Column(name = "token")
	private String token;
	
	@Column(name = "sendEmailInvitation")
	private String sendEmailInvitation;
	
	@Column(name = "userType")
	private int userType;
	
	@Column(name = "circle")
	private String circle;
	
	@Column(name = "circleAdminId")
	private String circleAdminId;
	
	@Column(name = "group_id")
	private int group_id;
	
	public int getUserId(){
		return userId;
	}
	
	public void setUserId(int id){
		this.userId=id;
	}
	
	public String getEmail(){
		return email;
	}
	
	public void setEmail(String email){
		this.email=email;
	}
	
	public String getFirstName(){
		return firstName;
	}
	
	public void setFirstName(String name){
		this.firstName=name;
	}
	
	public String getLastName(){
		return lastName;
	}
	
	public void setLastName(String name){
		this.lastName=name;
	}
	
	public String getPassword(){
		return password;
	}
	
	public void setPassword(String password){
		this.password=password;
	}
	
	public String getDisplayName(){
		return displayName;
	}
	
	public void setDisplayName(String name){
		this.displayName=name;
	}
	
	public String getIsActive(){
		return isActive;
	}
	
	public void setIsActive(String active){
		this.isActive=active;
	}
	
	public Timestamp getCreateDataTime(){
		return createDateTime;
	}
	
	public void setCreateDateTime(Timestamp date){
		this.createDateTime=date;
	}
	
	public String getToken(){
		return token;
	}
	
	public void setToken(String token){
		this.token=token;
	}
	
	public String getSendEmailInvitation(){
		return sendEmailInvitation;
	}
	
	public void setSendEmailInvitation(String text){
		this.sendEmailInvitation=text;
	}
	
	public int getUserType(){
		return userType;
	}
	
	public void setUserType(int userType){
		this.userType=userType;
	}
	
	public String getCircle(){
		return circle;
	}
	
	public void setCircle(String circle){
		this.circle=circle;
	}
	
	public String getCircleAdminId(){
		return circleAdminId;
	}
	
	public void setCircleAdminId(String circle){
		this.circleAdminId=circle;
	}
}

