package com.esahai.dbconnect.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="app_settings")
public class AppSettings implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@Column(name = "fieldName")
	private String fieldname;
	
	@Column(name = "value")
	private String value;
	
	@Column(name = "created_date")
	private Timestamp created_date;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

    public String getFieldName(){
    	return fieldname;
    }
    
    public void setFieldName(String fieldname){
    	this.fieldname = fieldname;
    }

    public String getValue(){
    	return value;
    }
    
    public void setValue(String value){
    	this.value = value;
    }

    public Timestamp getCreatedDate(){
    	return created_date;
    }
    
    public void setCreatedDate(Timestamp created_date){
    	this.created_date = created_date;
    }

}