package com.esahai.dbconnect.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="come_to_know_by")
public class ComeToKnowBy implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
    
	@Column(name = "status")
	private String status;
	
	@Column(name = "know_by")
	private String know_by;
	
	@Column(name = "created_datetime")
	private Timestamp created_datetime;
	
	@Column(name = "updated_datetime")
	private Timestamp updated_datetime;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

    public String getStatus(){
    	return status;
    }
    
    public void setStatus(String status){
    	this.status = status;
    }

    public String getKnowBy(){
    	return know_by;
    }
    
    public void setKnowBy(String know_by){
    	this.know_by = know_by;
    }
    
    public Timestamp getCreatedDateTime(){
    	return created_datetime;
    }
    
    public void setCreatedDatetime(Timestamp created_datetime){
    	this.created_datetime = created_datetime;
    }
    
    public Timestamp getUpdatedDateTime(){
    	return updated_datetime;
    }
    
    public void setUpdatedDatetime(Timestamp updated_datetime){
    	this.updated_datetime = updated_datetime;
    }
    
}