package com.esahai.dbconnect.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="api_service")
public class ApiService implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@Column(name = "api_id")
	private String api_id;
	
	@Column(name = "service_id")
	private String service_id;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

    public String getApiId(){
    	return api_id;
    }
    
    public void setApiId(String api_id){
    	this.api_id = api_id;
    }

    public String getServiceId(){
    	return service_id;
    }
    
    public void setServiceId(String service_id){
    	this.service_id = service_id;
    }

}