package com.esahai.dbconnect.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="geo_hashes")
public class GeoHashes implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	
	@Column(name = "geo_hash")
	private String geo_hash;
	
	@Column(name = "created_date")
	private Timestamp created_date;
	
	@Column(name = "updated_date")
	private Timestamp updated_date;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
        
    public String getGeoHash(){
    	return geo_hash;
    }
    
    public void setGeoHash(String geo_hash){
    	this.geo_hash= geo_hash;
    }
    
    public Timestamp getCreatedDate(){
    	return created_date;
    }
    
    public void setCreatedDate(Timestamp created_date){
    	this.created_date = created_date;
    }
    
    public Timestamp getUpdatedDate(){
    	return updated_date;
    }
    
    public void setUpdatedDate(Timestamp updated_date){
    	this.updated_date = updated_date;
    }
}