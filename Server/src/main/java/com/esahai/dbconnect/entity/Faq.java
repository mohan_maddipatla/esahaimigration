package com.esahai.dbconnect.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="faq")
public class Faq implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "faq_id")
	private int faq_id;
	
	@Column(name = "app_id")
	private String app_id;
	
	@Column(name = "updated_date")
	private Timestamp updated_date;
	
	@Column(name = "created_date")
	private Timestamp created_date;
	
	@Column(name = "answer")
	private String answer;
	
	@Column(name = "question")
	private String question;
	
	@Column(name = "faq_type")
	private String faq_type;
	
	public int getFaqId() {
		return faq_id;
	}

	public void setFaqId(int faq_id) {
		this.faq_id = faq_id;
	}
        
    public String getAppId(){
    	return app_id;
    }
    
    public void setAppId(String app_id){
    	this.app_id= app_id;
    }

    public String getAnswer(){
    	return answer;
    }
    
    public void setAnswer(String answer){
    	this.answer = answer;
    }
    
    public String getFaqType(){
    	return faq_type;
    }
    
    public void setFaqType(String faq_type){
    	this.faq_type = faq_type;
    }
    
    public String getQuestion(){
    	return question;
    }
    
    public void setQuestion(String question){
    	this.question = question;
    }
    public Timestamp getCreatedDate(){
    	return created_date;
    }
    
    public void setCreatedDate(Timestamp created_date){
    	this.created_date = created_date;
    }
    
    public Timestamp getUpdatedDate(){
    	return updated_date;
    }
    
    public void setUpdatedDate(Timestamp updated_date){
    	this.updated_date = updated_date;
    }
    
}