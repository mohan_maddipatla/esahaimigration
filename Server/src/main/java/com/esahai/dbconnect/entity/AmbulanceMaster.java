package com.esahai.dbconnect.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ambulance_master")
public class AmbulanceMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "amb_id")
	private int amb_id;

	@Column(name = "ambulance_number")
	private String ambulance_number;
	
	@Column(name = "vehicle_type")
	private int vehicle_type;
    
	@Column(name = "group_id")
	private int group_id;
	
	@Column(name = "device_imei")
	private String device_imei;
	
	@Column(name = "device_os")
	private String device_os;
    
	@Column(name = "device_type")
	private String device_type;
    
	@Column(name = "is_active")
	private String is_active;
	
	@Column(name = "device_token")
	private String device_token;
	
	@Column(name = "file_url")
	private String file_url;
	
	@Column(name = "ambulance_mobile")
	private String ambulance_mobile;
	
	@Column(name = "otp")
	private String otp;
	
	@Column(name = "is_verified")
	private String is_verified;
	
	@Column(name = "sms_status")
	private String sms_status;
	
	@Column(name = "emergency_type")
	private String emergency_type;
	
	@Column(name = "token")
	private String token;
	
	@Column(name = "login_status")
	private String login_status;
	
	@Column(name = "app_version")
	private String app_version;
	
	@Column(name = "created_date")
	private Timestamp created_date;
	
	@Column(name = "updated_date")
	private Timestamp updated_date;
	
	
	public int getAmb_Id() {
		return amb_id;
	}

	public void setAmb_Id(int amb_id) {
		this.amb_id = amb_id;
	}
	 
	public int getVehicleType() {
		return vehicle_type;
	}

	public void setVehicleType(int vehicle_type) {
		this.vehicle_type = vehicle_type;
	}
	
	public int getGroupId() {
		return group_id;
	}

	public void setGroupId(int group_id) {
		this.group_id = group_id;
	}
	
    public String getAmbulance_Number(){
    	return ambulance_number;
    }
    
    public void setAmbulance_Number(String ambulance_number){
    	this.ambulance_number = ambulance_number;
    }
    
    public String getDevice_imei(){
    	return device_imei;
    }
    
    public void setDevice_imei(String device_imei){
    	this.device_imei = device_imei;
    }
    
    public String getDevice_os(){
    	return device_os;
    }
    
    public void setDevice_os(String device_os){
    	this.device_os = device_os;
    }

    public String getDevice_type(){
    	return device_type;
    }
    
    public void setDevice_type(String device_type){
    	this.device_type = device_type;
    }

    public String getDevice_token(){
    	return device_token;
    }
    
    public void setDevice_token(String device_token){
    	this.device_token = device_token;
    }

    public String getActiveStatus(){
    	return is_active;
    }
    
    public void setActiveStatus(String is_active){
    	this.is_active = is_active;
    }

    public String getFile_url(){
    	return file_url;
    }
    
    public void setFile_url(String file_url){
    	this.file_url = file_url;
    }

    public String getAmbulance_mobile(){
    	return ambulance_mobile;
    }
    
    public void setAmbulance_mobile(String ambulance_mobile){
    	this.ambulance_mobile = ambulance_mobile;
    }

    public String getOtp(){
    	return otp;
    }
    
    public void setOtp(String otp){
    	this.otp = otp;
    }

    public String getVerificationStatus(){
    	return is_verified;
    }
    
    public void setVerificationStatus(String is_verified){
    	this.is_verified = is_verified;
    }

    public String getSmsStatus(){
    	return sms_status;
    }
    
    public void setSmsStatus(String sms_status){
    	this.sms_status = sms_status;
    }

    public String getEmergencyType(){
    	return emergency_type;
    }
    
    public void setEmergencyType(String emergency_type){
    	this.emergency_type = emergency_type;
    }

    public String getToken(){
    	return token;
    }
    
    public void setToken(String token){
    	this.token = token;
    }

    public String getLoginStatus(){
    	return login_status;
    }
    
    public void setLoginStatus(String login_status){
    	this.login_status = login_status;
    }

    public String getAppVersion(){
    	return app_version;
    }
    
    public void setAppVersion(String app_version){
    	this.app_version = app_version;
    }

    public Timestamp getCreatedDate(){
    	return created_date;
    }
    
    public void setCreatedDate(Timestamp created_date){
    	this.created_date = created_date;
    }
    public Timestamp getUpdatedDate(){
    	return updated_date;
    }
    
    public void setUpdatedDate(Timestamp updated_date){
    	this.updated_date = updated_date;
    }
}