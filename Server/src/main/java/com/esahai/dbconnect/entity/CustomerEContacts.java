package com.esahai.dbconnect.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="customer_e_contacts")
public class CustomerEContacts implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
    
	@Column(name = "contact_person")
	private String contact_person;
	
	@Column(name = "mobile_number")
	private String mobile_number;
	
	@Column(name = "relation")
	private String relation;
	
	@Column(name = "customer_id")
	private int customer_id;
	
	@Column(name = "is_notify")
	private short is_notify;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

    public String getContactPerson(){
    	return contact_person;
    }
    
    public void setContactPerson(String contact_person){
    	this.contact_person = contact_person;
    }

    public String getRelation(){
    	return relation;
    }
    
    public void setRelation(String relation){
    	this.relation = relation;
    }
    
    public int getCustomerId(){
    	return customer_id;
    }
    
    public void setCustomerId(int customer_id){
    	this.customer_id = customer_id;
    }
    
    public short getNotifyStatus(){
    	return is_notify;
    }
    
    public void setNotifyStatus(short is_notify){
    	this.is_notify = is_notify;
    }
    
}