package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.AccessRoles;
public class AccessRolesDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<AccessRoles> getAccessRolesDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(AccessRoles.class);
		criteria.addOrder(Order.asc("id"));
		List<AccessRoles> list = criteria.list();
		session.close();
		return list;
	}
}