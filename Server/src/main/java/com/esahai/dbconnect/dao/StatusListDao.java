package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.StatusList;


public class StatusListDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<StatusList> getStatusListDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(StatusList.class);
		criteria.addOrder(Order.asc("id"));
		List<StatusList> list = criteria.list();
		session.close();
		return list;
	}
}