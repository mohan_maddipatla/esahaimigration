package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.Tracking;
import com.esahai.dbconnect.entity.UserAppRoles;


public class TrackingDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<Tracking> getTracking() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(Tracking.class);
		criteria.addOrder(Order.asc("id"));
		List<Tracking> list = criteria.list();
		session.close();
		return list;
	}
}
