package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.GeoHashes;
public class GeoHashesDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<GeoHashes> getGeoHashesDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(GeoHashes.class);
		criteria.addOrder(Order.asc("id"));
		List<GeoHashes> list = criteria.list();
		session.close();
		return list;
	}
}