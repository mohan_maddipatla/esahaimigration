package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.CountryList;
public class CountryListDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<CountryList> getCountryListDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(CountryList.class);
		criteria.addOrder(Order.asc("id"));
		List<CountryList> list = criteria.list();
		session.close();
		return list;
	}
}