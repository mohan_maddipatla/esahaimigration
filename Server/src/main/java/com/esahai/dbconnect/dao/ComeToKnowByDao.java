package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.ComeToKnowBy;
public class ComeToKnowByDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<ComeToKnowBy> getComeToKnowByDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(ComeToKnowBy.class);
		criteria.addOrder(Order.asc("id"));
		List<ComeToKnowBy> list = criteria.list();
		session.close();
		return list;
	}
}