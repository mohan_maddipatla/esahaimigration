package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.TermsConditions;


public class TermsConditionsDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<TermsConditions> getTermsConditions() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(TermsConditions.class);
		criteria.addOrder(Order.asc("id"));
		List<TermsConditions> list = criteria.list();
		session.close();
		return list;
	}
}