package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.ApiService;
public class ApiServiceDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<ApiService> getApiServiceDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(ApiService.class);
		criteria.addOrder(Order.asc("id"));
		List<ApiService> list = criteria.list();
		session.close();
		return list;
	}
}