package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.GCMTopics;
public class GCMTopicsDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<GCMTopics> getGCMTopicsDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(GCMTopics.class);
		criteria.addOrder(Order.asc("id"));
		List<GCMTopics> list = criteria.list();
		session.close();
		return list;
	}
}