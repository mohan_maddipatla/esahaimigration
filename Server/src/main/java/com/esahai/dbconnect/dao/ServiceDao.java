package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.Services;

public class ServiceDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<Services> getServicesDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(Services.class);
		criteria.addOrder(Order.asc("name"));
		List<Services> list = criteria.list();
		session.close();
		return list;
	}
}