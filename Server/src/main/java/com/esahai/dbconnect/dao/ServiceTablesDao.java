package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.ServiceTables;

public class ServiceTablesDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<ServiceTables> getServiceTablesDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(ServiceTables.class);
		criteria.addOrder(Order.asc("id"));
		List<ServiceTables> list = criteria.list();
		session.close();
		return list;
	}
}