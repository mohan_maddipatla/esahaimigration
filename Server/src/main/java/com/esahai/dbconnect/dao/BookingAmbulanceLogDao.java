package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.BookingAmbulanceLog;
public class BookingAmbulanceLogDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<BookingAmbulanceLog> getBookingAmbulanceLogDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(BookingAmbulanceLog.class);
		criteria.addOrder(Order.asc("id"));
		List<BookingAmbulanceLog> list = criteria.list();
		session.close();
		return list;
	}
}