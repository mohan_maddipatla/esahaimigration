package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.BloodRequest;
public class BloodRequestDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<BloodRequest> getBloodRequestDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(BloodRequest.class);
		criteria.addOrder(Order.asc("id"));
		List<BloodRequest> list = criteria.list();
		session.close();
		return list;
	}
}