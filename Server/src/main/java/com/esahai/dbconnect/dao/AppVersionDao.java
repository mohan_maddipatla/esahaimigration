package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.AppVersion;
public class AppVersionDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<AppVersion> getAppVersionDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(AppVersion.class);
		criteria.addOrder(Order.asc("id"));
		@SuppressWarnings("unchecked")
		List<AppVersion> list = criteria.list();
		session.close();
		return list;
	}
}