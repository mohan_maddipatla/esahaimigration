package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.BloodBank;
import com.esahai.dbconnect.entity.AmbulanceDriver;;

public class AmbulanceDriverDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<AmbulanceDriver> getAmbulanceDriverDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(AmbulanceDriver.class);
		criteria.addOrder(Order.asc("id"));
		List<AmbulanceDriver> list = criteria.list();
		session.close();
		return list;
	}
}