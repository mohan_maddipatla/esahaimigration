package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.BloodBank;

public class BloodBankDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<BloodBank> getBloodBankDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(BloodBank.class);
		criteria.addOrder(Order.asc("name"));
		List<BloodBank> list = criteria.list();
		session.close();
		return list;
	}
}