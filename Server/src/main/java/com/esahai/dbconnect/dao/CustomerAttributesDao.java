package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.CustomerAttributes;
public class CustomerAttributesDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<CustomerAttributes> getCustomerAttributesDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(CustomerAttributes.class);
		criteria.addOrder(Order.asc("id"));
		List<CustomerAttributes> list = criteria.list();
		session.close();
		return list;
	}
}