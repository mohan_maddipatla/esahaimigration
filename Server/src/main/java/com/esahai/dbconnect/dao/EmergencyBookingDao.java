package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.EmergencyBooking;

public class EmergencyBookingDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<EmergencyBooking> getAllEmergencyBookings() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(EmergencyBooking.class);
		criteria.addOrder(Order.asc("customerName"));
		List<EmergencyBooking> list = criteria.list();
		session.close();
		return list;
	}
}