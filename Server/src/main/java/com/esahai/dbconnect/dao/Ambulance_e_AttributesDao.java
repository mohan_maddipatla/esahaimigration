package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.Ambulance_e_Attributes;
public class Ambulance_e_AttributesDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<Ambulance_e_Attributes> getAmbulance_e_AttributesDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(Ambulance_e_Attributes.class);
		criteria.addOrder(Order.asc("id"));
		List<Ambulance_e_Attributes> list = criteria.list();
		session.close();
		return list;
	}
}