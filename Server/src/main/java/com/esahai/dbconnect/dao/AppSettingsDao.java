package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.AppSettings;
public class AppSettingsDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<AppSettings> getAppSettingsDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(AppSettings.class);
		criteria.addOrder(Order.asc("id"));
		@SuppressWarnings("unchecked")
		List<AppSettings> list = criteria.list();
		session.close();
		return list;
	}
}