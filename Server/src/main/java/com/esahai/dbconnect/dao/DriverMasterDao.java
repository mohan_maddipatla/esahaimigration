package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.DriverMaster;
public class DriverMasterDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<DriverMaster> getDriverMasterDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(DriverMaster.class);
		criteria.addOrder(Order.asc("driver_id"));
		List<DriverMaster> list = criteria.list();
		session.close();
		return list;
	}
}