package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.UserType;

public class UserTypeDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<UserType> getUserTypeDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(UserType.class);
		criteria.addOrder(Order.asc("type_name"));
		List<UserType> list = criteria.list();
		session.close();
		return list;
	}
}