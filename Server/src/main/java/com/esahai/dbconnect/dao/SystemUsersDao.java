package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.SystemUsers;


public class SystemUsersDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<SystemUsers> getSytemUserDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(SystemUsers.class);
		criteria.addOrder(Order.asc("userId"));
		List<SystemUsers> list = criteria.list();
		session.close();
		return list;
	}
}