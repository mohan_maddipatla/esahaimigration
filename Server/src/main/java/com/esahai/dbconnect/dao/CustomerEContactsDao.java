package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.CustomerEContacts;
public class CustomerEContactsDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<CustomerEContacts> getCustomerEContactsDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(CustomerEContacts.class);
		criteria.addOrder(Order.asc("id"));
		List<CustomerEContacts> list = criteria.list();
		session.close();
		return list;
	}
}