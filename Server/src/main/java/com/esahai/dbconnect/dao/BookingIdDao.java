package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.BookingId;
public class BookingIdDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<BookingId> getBookingIdDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(BookingId.class);
		criteria.addOrder(Order.asc("booking_id"));
		List<BookingId> list = criteria.list();
		session.close();
		return list;
	}
}