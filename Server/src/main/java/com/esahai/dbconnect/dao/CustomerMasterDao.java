package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.CustomerMaster;
public class CustomerMasterDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<CustomerMaster> getCustomerMasterDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(CustomerMaster.class);
		criteria.addOrder(Order.asc("customer_id"));
		List<CustomerMaster> list = criteria.list();
		session.close();
		return list;
	}
}