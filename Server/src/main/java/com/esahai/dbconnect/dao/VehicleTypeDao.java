package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.VehicleType;

public class VehicleTypeDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<VehicleType> getVehicleTypeDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(VehicleType.class);
		criteria.addOrder(Order.asc("type_name"));
		List<VehicleType> list = criteria.list();
		session.close();
		return list;
	}
}