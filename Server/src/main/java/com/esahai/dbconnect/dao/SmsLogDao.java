package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.SmsLog;

public class SmsLogDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<SmsLog> getSmsLogDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(SmsLog.class);
		criteria.addOrder(Order.asc("id"));
		List<SmsLog> list = criteria.list();
		session.close();
		return list;
	}
}