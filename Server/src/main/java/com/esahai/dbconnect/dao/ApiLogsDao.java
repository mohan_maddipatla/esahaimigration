package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.ApiLogs;
public class ApiLogsDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<ApiLogs> getApiLogsDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(ApiLogs.class);
		criteria.addOrder(Order.asc("id"));
		List<ApiLogs> list = criteria.list();
		session.close();
		return list;
	}
}