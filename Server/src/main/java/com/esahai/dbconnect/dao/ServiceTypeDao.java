package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.ServiceType;

public class ServiceTypeDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<ServiceType> getServiceTypeDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(ServiceType.class);
		criteria.addOrder(Order.asc("id"));
		List<ServiceType> list = criteria.list();
		session.close();
		return list;
	}
}