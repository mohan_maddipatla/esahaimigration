package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.Ambulance_e_Attributes;
import com.esahai.dbconnect.entity.Ambulance_e_types;
public class Ambulance_e_typesDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<Ambulance_e_types> getAmbulance_e_typesDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(Ambulance_e_types.class);
		criteria.addOrder(Order.asc("id"));
		List<Ambulance_e_types> list = criteria.list();
		session.close();
		return list;
	}
}