package com.esahai.dbconnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.esahai.dbconnect.entity.SmsDetails;

public class SmsDetailsDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<SmsDetails> getSmsDetails() {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(SmsDetails.class);
		criteria.addOrder(Order.asc("id"));
		List<SmsDetails> list = criteria.list();
		session.close();
		return list;
	}
}