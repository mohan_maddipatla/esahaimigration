package com.esahai.services;

import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.esahai.dao.BloodBankDaoImpl;
import com.esahai.entity.BloodBank;
import com.esahai.util.HttpResponseStatusCode;
import com.esahai.util.JsonResponse;
import com.google.gson.Gson;

// TODO: Auto-generated Javadoc
/**
 * The Class BloodBankService.
 */
/*
 * Author: Version: 1.0 Operation: List Modified:
 */
@Path("/bloodBank")
public class BloodBankService {

	/** The Constant logger. */
	public static final Logger logger = Logger.getLogger(BloodBankService.class);

	/**
	 * Gets the blood bank details.
	 *
	 * @param servletContext
	 *            the servlet context
	 * @return the blood bank details
	 */
	// This method used for getBloodBankDetails GET Request :
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/getBloodBankDetails")
	public JsonResponse getBloodBankDetails(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		BloodBankDaoImpl bloodBankDaoImpl = (BloodBankDaoImpl) context.getBean("bloodBankDaoImpl",
				BloodBankDaoImpl.class);
		List<BloodBank> bloodBankList = bloodBankDaoImpl.getBloodBankDetails();
		String json = new Gson().toJson(bloodBankList);
		logger.info("json Object = " + json);
		return new JsonResponse(HttpResponseStatusCode.SUCCESS, HttpResponseStatusCode.OK, bloodBankList);

	}

}
