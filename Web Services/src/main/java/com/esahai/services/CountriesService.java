package com.esahai.services;

import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.esahai.dao.CountriesDaoImpl;
import com.esahai.entity.Countries;
import com.esahai.util.HttpResponseStatusCode;
import com.esahai.util.JsonResponse;
import com.esahai.util.JsonUtils;

/**
 * The Class CountriesService.
 */
/**
 * Author: SWATHI Version: 1.0 Modified:
 */
@Path("/countries")
public class CountriesService {

	/** The Constant logger. */
	public static final Logger logger = Logger.getLogger(CountriesService.class);

	/** The servlet context. */
	@Context
	private ServletContext servletContext;

	/**
	 * Gets the coutries.
	 *
	 * @return the coutries
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/getCountriesDetails")
	public JsonResponse getCountriesDetails() {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		CountriesDaoImpl countriesDaoImpl = (CountriesDaoImpl) context.getBean("countriesDaoImpl",
				CountriesDaoImpl.class);
		List<Countries> countriesList = countriesDaoImpl.getCountriesList();

		String json = JsonUtils.getJson(countriesList);
		logger.info("result = " + json);

		return new JsonResponse(HttpResponseStatusCode.SUCCESS, HttpResponseStatusCode.OK, countriesList);
	}
}
