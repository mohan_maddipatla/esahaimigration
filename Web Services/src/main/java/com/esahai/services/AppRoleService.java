package com.esahai.services;

import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.esahai.dao.AppRoleDaoImpl;
import com.esahai.entity.AppRoles;
import com.esahai.util.HttpResponseStatusCode;
import com.esahai.util.JsonResponse;
import com.google.gson.Gson;

/**
 * The Class AppRoleService.
 * 
 * @author Santhosh
 */
@Path("/approles")
public class AppRoleService {

	/** The servlet context. */
	@Context
	ServletContext servletContext;

	/** The Constant log. */
	private static final Logger log = Logger.getLogger(AppRoleService.class);

	/**
	 * Gets the app roles list.
	 *
	 * @return the app roles list
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getAppRoleslist")
	public JsonResponse getAppRolesList() {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		AppRoleDaoImpl userTypeDao = (AppRoleDaoImpl) context.getBean("appRoleDaoImpl", AppRoleDaoImpl.class);
		List<AppRoles> userTypeList = userTypeDao.getAppRolesList();
		log.info("Approles =" + userTypeList);
		return new JsonResponse(HttpResponseStatusCode.SUCCESS, HttpResponseStatusCode.OK, userTypeList);
	}

	/**
	 * Save app roles.
	 *
	 * @param roleName
	 *            the role name
	 * @param roleType
	 *            the role type
	 * @return the json response
	 */
	@POST
	@Path("/saveAppRoles")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public JsonResponse saveAppRoles(@FormParam("rolename") String roleName, @FormParam("roletype") String roleType) {
		log.info("roleName = " + roleName);
		log.info("roleType = " + roleType);

		JsonResponse jsonResponse = null;
		String json = null;
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		AppRoles appRoles = new AppRoles();
		appRoles.setRoleName(roleName);
		appRoles.setRoleType(roleType);
		AppRoleDaoImpl appRoleDao = (AppRoleDaoImpl) context.getBean("appRoleDaoImpl", AppRoleDaoImpl.class);
		String savedStatus = appRoleDao.saveAppRoles(appRoles);
		if (savedStatus.equalsIgnoreCase("success")) {
			jsonResponse = new JsonResponse(HttpResponseStatusCode.SUCCESS, HttpResponseStatusCode.CREATE_SUCCESS,
					savedStatus);
		} else if (savedStatus.equalsIgnoreCase("failed")) {
			jsonResponse = new JsonResponse(HttpResponseStatusCode.CLOSED, HttpResponseStatusCode.s116, savedStatus);
		}
		log.info("Approles saved =" + json);
		return jsonResponse;
	}

	/**
	 * Update app roles.
	 *
	 * @param roleId
	 *            the role id
	 * @param roleName
	 *            the role name
	 * @param roleType
	 *            the role type
	 * @return the json response
	 */
	@POST
	@Path("/updateAppRoles")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public JsonResponse updateAppRoles(@FormParam("roleid") int roleId, @FormParam("rolename") String roleName,
			@FormParam("roletype") String roleType) {

		log.info("roleId = " + roleId);
		log.info("roleName = " + roleName);
		log.info("roleType = " + roleType);
		String json = null;
		JsonResponse jsonResponse = null;
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		AppRoles appRoles = new AppRoles();
		appRoles.setAppRoleId(roleId);
		appRoles.setRoleName(roleName);
		appRoles.setRoleType(roleType);
		AppRoleDaoImpl appRoleDao = (AppRoleDaoImpl) context.getBean("appRoleDaoImpl", AppRoleDaoImpl.class);
		String updatedStatus = appRoleDao.updateAppRoles(appRoles);
		if (updatedStatus.equalsIgnoreCase("success")) {
			jsonResponse = new JsonResponse(HttpResponseStatusCode.SUCCESS, HttpResponseStatusCode.UPDATE_SUCCESS,
					updatedStatus);
		} else if (updatedStatus.equalsIgnoreCase("failed")) {
			jsonResponse = new JsonResponse(HttpResponseStatusCode.CLOSED, HttpResponseStatusCode.s117, updatedStatus);
		}
		log.info("Approles updated on= " + json);
		return jsonResponse;
	}

	/**
	 * Delete app role.
	 *
	 * @param roleId
	 *            the role id
	 * @return the json response
	 */
	@POST
	@Path("/deleteAppRoles")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public JsonResponse deleteAppRole(@FormParam("roleid") int roleId) {
		log.info("roleId = " + roleId);
		String json = null;
		JsonResponse jsonResponse = null;
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		AppRoles appRoles = new AppRoles();
		appRoles.setAppRoleId(roleId);
		AppRoleDaoImpl appRoleDao = (AppRoleDaoImpl) context.getBean("appRoleDaoImpl", AppRoleDaoImpl.class);
		String deletedStatus = appRoleDao.deleteAppRoles(appRoles);
		if (deletedStatus.equalsIgnoreCase("success")) {
			jsonResponse = new JsonResponse(HttpResponseStatusCode.SUCCESS, HttpResponseStatusCode.DELETE_SUCCESS,
					deletedStatus);
		} else if (deletedStatus.equalsIgnoreCase("failed")) {
			jsonResponse = new JsonResponse(HttpResponseStatusCode.CLOSED, HttpResponseStatusCode.s118, deletedStatus);
		}
		log.info("Approle deleted = " + json);
		return jsonResponse;

	}
}