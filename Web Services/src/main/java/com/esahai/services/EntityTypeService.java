package com.esahai.services;

import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.esahai.dao.EntityTypesDaoImpl;
import com.esahai.entity.EntityTypes;
import com.esahai.util.Constant;
import com.esahai.util.DateUtils;
import com.esahai.util.HttpResponseStatusCode;
import com.esahai.util.JsonResponse;
import com.esahai.util.JsonUtils;

/**
 * The Class EntityTypeService.
 */
/*
 * Author: SWATHI Version: 1.0
 *
 * Modified:
 */
@Path("/entityType")
public class EntityTypeService {

	/** The Constant logger. */
	public static final Logger logger = Logger.getLogger(EntityTypeService.class);

	/** The servlet context. */
	@Context
	ServletContext servletContext;

	/**
	 * Gets the entity type details.
	 * 
	 * @return the entity type details
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/getEntityTypeDetails")
	public JsonResponse getEntityTypeDetails() {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		EntityTypesDaoImpl entityTypesDaoImpl = (EntityTypesDaoImpl) context.getBean("entityTypesDaoImpl",
				EntityTypesDaoImpl.class);
		List<EntityTypes> entityTypesList = entityTypesDaoImpl.getEntityTypes();

		String json = JsonUtils.getJson(entityTypesList);
		logger.info("result = " + json);

		return new JsonResponse(HttpResponseStatusCode.SUCCESS, HttpResponseStatusCode.OK, entityTypesList);
	}

	/**
	 * Creates the entity type.
	 * 
	 * @param entityTypes
	 *            the entity types
	 * @return the json response
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/createEntityType")
	public JsonResponse createEntityType(EntityTypes entityTypes) {
		logger.info("Entity type object = "+ entityTypes);
		entityTypes.setCreated_date(DateUtils.getDate(new Date()));
		entityTypes.setUpdated_date(DateUtils.getDate(new Date()));
		entityTypes.setStatus(Constant.ACTIVE);
		logger.info("Entity Type = "+ JsonUtils.getJson(entityTypes));

		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		EntityTypesDaoImpl entityTypesDaoImpl = (EntityTypesDaoImpl) context.getBean("entityTypesDaoImpl",
				EntityTypesDaoImpl.class);
		boolean result = entityTypesDaoImpl.saveEntityType(entityTypes);
		logger.info("Result = "+ result);

		if (result) {

			return new JsonResponse(HttpResponseStatusCode.SUCCESS, HttpResponseStatusCode.CREATE_SUCCESS, result);
		} else {
			return new JsonResponse(HttpResponseStatusCode.CLOSED, HttpResponseStatusCode.s116, result);
		}
	}

	/**
	 * Update entity type.
	 * 
	 * @param entityTypes
	 *            the entity types
	 * @return the json response
	 */
	@POST
	@Path("/updateEntityType")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public JsonResponse updateEntityType(EntityTypes entityTypes) {
		logger.info("Entity type object = "+ entityTypes);

		entityTypes.setUpdated_date(DateUtils.getDate(new Date()));
		logger.info("Entity Type = "+ JsonUtils.getJson(entityTypes));

		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		EntityTypesDaoImpl entityTypesDaoImpl = (EntityTypesDaoImpl) context.getBean("entityTypesDaoImpl",
				EntityTypesDaoImpl.class);
		boolean result = entityTypesDaoImpl.updateEntityType(entityTypes);
		logger.info("Result = "+ result);

		if (result) {
			return new JsonResponse(HttpResponseStatusCode.SUCCESS, HttpResponseStatusCode.UPDATE_SUCCESS, result);
		} else {
			return new JsonResponse(HttpResponseStatusCode.CLOSED, HttpResponseStatusCode.s117, result);
		}
	}

	/**
	 * Delete entity type.
	 * 
	 * @param entity_type_id
	 *            the entity type id
	 * @return the json response
	 */
	@GET
	@Path("/deleteEntityType/{entity_type_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public JsonResponse deleteEntityType(@PathParam("entity_type_id") int entity_type_id) {
		EntityTypes entityTypes = new EntityTypes();
		logger.info("Entity Type ID = "+ entity_type_id);
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		EntityTypesDaoImpl entityTypesDaoImpl = (EntityTypesDaoImpl) context.getBean("entityTypesDaoImpl",
				EntityTypesDaoImpl.class);
		entityTypes.setStatus(Constant.DEACTIVE);
		logger.info("entityTypesDaoImpl object = " + entityTypesDaoImpl);
		boolean result = entityTypesDaoImpl.deleteEntityType(entity_type_id, entityTypes.getStatus());
		if (result) {

			return new JsonResponse(HttpResponseStatusCode.SUCCESS, HttpResponseStatusCode.DELETE_SUCCESS, result);
		} else {
			return new JsonResponse(HttpResponseStatusCode.CLOSED, HttpResponseStatusCode.s118, result);
		}
	}

}
