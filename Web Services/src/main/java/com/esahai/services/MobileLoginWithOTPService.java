package com.esahai.services;

import java.io.IOException;
import java.util.Random;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.esahai.util.HttpResponseStatusCode;
import com.esahai.util.JsonResponse;
import com.esahai.util.OTPSender;
import com.google.gson.Gson;

// TODO: Auto-generated Javadoc
/**
 * @author Santhosh
 * The Class MobileLoginWithOTPService.
 */
@Path("/mobilelogin")
public class MobileLoginWithOTPService {
	
	/** The Constant log. */
	private static final Logger log = Logger.getLogger(MobileLoginWithOTPService.class);

	
	/**
	 * Generate customer OTP.
	 *
	 * @param mobileNumber the mobile number
	 * @return the string
	 */
	@POST
	@Path("/generateOTP")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public JsonResponse generateCustomerOTP(@FormParam("mobile") int mobileNumber){
		log.info("mobileNumber  = "+mobileNumber);
		// To generate 6-digit random number
		Random rnd = new Random();
		int otp = 100000 + rnd.nextInt(900000);
		log.info("otp = "+otp);
		try {
		new OTPSender().sendSixDigitOTPNumber(mobileNumber,otp);
		} catch (IOException e) {
			e.printStackTrace();
			log.error("Error",e);
		}
		return new JsonResponse(HttpResponseStatusCode.SUCCESS, HttpResponseStatusCode.s127,otp);
	}

}
