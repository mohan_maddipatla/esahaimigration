package com.esahai.services;

import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.esahai.dao.EmergencyBookingDao;
import com.esahai.dao.EmergencyBookingDaoImpl;
import com.esahai.entity.EmergencyBooking;

// TODO: Auto-generated Javadoc
/**
 * The Class EmergencyBookingService.
 */
/*Author: 
 *Version: 1.0
 *Operation: List
 *Modified: 
 * */
@Path("/emergencyBooking")
public class EmergencyBookingService {

	/** The Constant logger. */
	public static final Logger logger = Logger.getLogger(EmergencyBookingService.class);

	/**
	 * Gets the emergency bookings.
	 *
	 * @param servletContext the servlet context
	 * @return the emergency bookings
	 */
	// This method used for the getEmergencyBookings GET Request:
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getEmergencyBookings")
	public List<EmergencyBooking> getEmergencyBookings(@Context ServletContext servletContext) {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		EmergencyBookingDaoImpl emergencyBookingDaoImpl = (EmergencyBookingDaoImpl) context.getBean("emergencyBookingDaoImpl", EmergencyBookingDao.class);

		List<EmergencyBooking> emergencyBookingList = emergencyBookingDaoImpl.getAllEmergencyBookings();

		//String json = new Gson().toJson(emergencyBookingList);
		return emergencyBookingList;
	}

}
