package com.esahai.services;

import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.esahai.dao.SystemUsersDaoImpl;
import com.esahai.entity.SystemUsers;
import com.esahai.util.Constant;
import com.esahai.util.HttpResponseStatusCode;
import com.esahai.util.JsonResponse;
import com.esahai.util.JsonUtils;

/**
 * The Class SystemUsersService.
 * 
 * Author: Satheesh 
 * Version: 1.0 
 */
@Path("/systemUsers")
public class SystemUsersService {

	public static final Logger logger = LoggerFactory.getLogger(SystemUsersService.class);

	@Context
	ServletContext servletContext;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/getSystemUsersDetails")
	public JsonResponse getSystemUsersDetails() {
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SystemUsersDaoImpl systemUsersDaoImpl = (SystemUsersDaoImpl) context.getBean("systemUsersDaoImpl",
				SystemUsersDaoImpl.class);
		List<SystemUsers> systemUsersList = systemUsersDaoImpl.getSystemUsers();

		String json = JsonUtils.getJson(systemUsersList);
		logger.info("result = " + json);

		return new JsonResponse(HttpResponseStatusCode.SUCCESS, HttpResponseStatusCode.OK, systemUsersList);
	}

	/**
	 * Creates the system users.
	 *
	 * @param systemUsers
	 *            the system users
	 * @return the json response
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/createSystemUsers")
	public JsonResponse createSystemUsers(SystemUsers systemUsers) {

		systemUsers.setStatus(Constant.ACTIVE);
		logger.info("System Users = ", JsonUtils.getJson(systemUsers));

		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SystemUsersDaoImpl systemUsersDaoImpl = (SystemUsersDaoImpl) context.getBean("systemUsersDaoImpl",
				SystemUsersDaoImpl.class);
		boolean result = systemUsersDaoImpl.saveSystemUsers(systemUsers);
		logger.info("Result = ", result);

		if (result) {

			return new JsonResponse(HttpResponseStatusCode.SUCCESS, HttpResponseStatusCode.CREATE_SUCCESS, result);
		} else {
			return new JsonResponse(HttpResponseStatusCode.CLOSED, HttpResponseStatusCode.s116, result);
		}
	}

	/**
	 * Update system users.
	 *
	 * @param systemUsers
	 *            the system users
	 * @return the json response
	 */
	@POST
	@Path("/updateSystemUsers")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public JsonResponse updateSystemUsers(SystemUsers systemUsers) {

		logger.info("System Users = ", JsonUtils.getJson(systemUsers));

		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SystemUsersDaoImpl systemUsersDaoImpl = (SystemUsersDaoImpl) context.getBean("systemUsersDaoImpl",
				SystemUsersDaoImpl.class);
		boolean result = systemUsersDaoImpl.updateSystemUsers(systemUsers);
		logger.info("Result = ", result);

		if (result) {
			return new JsonResponse(HttpResponseStatusCode.SUCCESS, HttpResponseStatusCode.UPDATE_SUCCESS, result);
		} else {
			return new JsonResponse(HttpResponseStatusCode.CLOSED, HttpResponseStatusCode.s117, result);
		}
	}

	/**
	 * Delete system users .
	 *
	 * @param id
	 *            the id
	 * @return the json response
	 */
	@GET
	@Path("/deleteSystemUsers/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public JsonResponse deleteSystemUsers(@PathParam("id") int id) {
		logger.info("id = " + id);
		SystemUsers systemUsers = new SystemUsers();
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SystemUsersDaoImpl systemUsersDaoImpl = (SystemUsersDaoImpl) context.getBean("systemUsersDaoImpl",
				SystemUsersDaoImpl.class);
		systemUsers.setStatus(Constant.DEACTIVE);
		logger.info("systemUsersDaoImpl object = ," + systemUsersDaoImpl);
		boolean result = systemUsersDaoImpl.deleteSystemUsers(id, systemUsers.getStatus());
		if (result) {

			return new JsonResponse(HttpResponseStatusCode.SUCCESS, HttpResponseStatusCode.DELETE_SUCCESS, result);
		}
		return new JsonResponse(HttpResponseStatusCode.CLOSED, HttpResponseStatusCode.s118, result);
	}

}
