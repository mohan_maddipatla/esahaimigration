package com.esahai.services;

import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.esahai.dao.AppVersionDaoImpl;
import com.esahai.entity.AppVersion;
import com.esahai.util.HttpResponseStatusCode;
import com.esahai.util.JsonResponse;
import com.esahai.util.JsonUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class AppVersionService.
 */
@Path("/appVersion")
public class AppVersionService {

	/** The Constant logger. */
	public static final Logger logger = Logger.getLogger(AppVersionService.class);

	/** The servlet context. */
	@Context
	private ServletContext servletContext;

	/**
	 * Gets the app version.
	 *
	 * @return the app version
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/getAppVersion/{app_type}")
	public JsonResponse getAppVersion(@PathParam("app_type") String app_type) {
		logger.info("app_type = "+ app_type);
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		AppVersionDaoImpl appVersionDaoImpl = (AppVersionDaoImpl) context.getBean("appVersionDaoImpl",
				AppVersionDaoImpl.class);
		List<AppVersion> appVesion = appVersionDaoImpl.getAppVersion(app_type);

		String json = JsonUtils.getJson(appVesion);
		logger.info("result = " + json);

		return new JsonResponse(HttpResponseStatusCode.SUCCESS, HttpResponseStatusCode.OK, appVesion);
	}
}
