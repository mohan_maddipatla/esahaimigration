package com.esahai.services;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.esahai.dao.LoginDaoImpl;
import com.esahai.entity.LoginJsonEntity;
import com.esahai.entity.SystemUsers;
import com.esahai.util.HttpResponseStatusCode;
import com.esahai.util.JsonResponse;

@Path("/login")
public class LoginService {

	/** The servlet context. */
	@Context
	ServletContext servletContext;

	/** The Constant log. */
	private static final Logger log = Logger.getLogger(LoginService.class);

	@POST
	@Path("/authenticateuser")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public JsonResponse authenticateUser(@FormParam("email") String email, @FormParam("password") String password) {
		log.info("email = "+email);
		log.info("password = "+password);
		String loginStatus = null,json=null;
		JsonResponse jsonResponse=null;
		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		LoginDaoImpl loginDao = (LoginDaoImpl) context.getBean("loginDaoImpl", LoginDaoImpl.class);
		SystemUsers systemUsers = new SystemUsers();
		systemUsers.setEmail(email);
		systemUsers.setPassword(password);
	   LoginJsonEntity loginJsonEntity =loginDao.authenticateUser(systemUsers);
		if(loginJsonEntity!=null){
			jsonResponse = new JsonResponse(HttpResponseStatusCode.SUCCESS,HttpResponseStatusCode.s103,loginJsonEntity);
		}else{
			loginStatus = "Invalid Credentials.";
			jsonResponse = new JsonResponse(HttpResponseStatusCode.BAD_REQUEST,HttpResponseStatusCode.s125,loginStatus);
		}
		return jsonResponse;
	}
	

}
