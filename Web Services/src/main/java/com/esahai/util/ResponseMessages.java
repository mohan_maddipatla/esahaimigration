package com.esahai.util;

import org.springframework.stereotype.Component;

// TODO: Auto-generated Javadoc
/**
 * The Class ResponseMessages.
 */
@Component("responseMessages")
public class ResponseMessages {
	
	
	/** The Constant OK. */
	public static final int OK=200;
	
	/** The Constant Created. */
	public static final int Created=201;
	
	/** The Constant Accepted. */
	public static final int Accepted=202;
	
	/** The Constant Non_Authoritative_Information. */
	public static final int Non_Authoritative_Information=203;
	
	/** The Constant No_Content. */
	public static final int No_Content=204;
	
	/** The Constant Reset_Content. */
	public static final int Reset_Content=205;
	
	/** The Constant Partial_Content. */
	public static final int Partial_Content=206;
	
	/** The Constant Multiple_Choices. */
	public static final int Multiple_Choices=300;
	
	/** The Constant Moved_Permanently. */
	public static final int Moved_Permanently=301;
	
	/** The Constant Found. */
	public static final int Found=302;
	
	/** The Constant See_Other. */
	public static final int See_Other=303;
	
	/** The Constant Not_Modified. */
	public static final int Not_Modified=304;
	
	/** The Constant Use_Proxy. */
	public static final int Use_Proxy=305;
	
	/** The Constant Temporary_Redirect. */
	public static final int Temporary_Redirect=307;
	
	/** The Constant Bad_Request. */
	public static final int Bad_Request=400;
	
	/** The Constant Unauthorized. */
	public static final int Unauthorized=401;
	
	/** The Constant Payment_Required. */
	public static final int Payment_Required=402;
	
	/** The Constant Forbidden. */
	public static final int Forbidden=403;
	
	/** The Constant Not_Found. */
	public static final int Not_Found=404;
	
	/** The Constant Method_Not_Allowed. */
	public static final int Method_Not_Allowed=405;
	
	/** The Constant Not_Acceptable. */
	public static final int Not_Acceptable=406;
	
	/** The Constant Proxy_Authentication_Required. */
	public static final int Proxy_Authentication_Required=407;
	
	/** The Constant Request_Timeout. */
	public static final int Request_Timeout=408;
	
	/** The Constant Conflict. */
	public static final int Conflict=409;
	
	/** The Constant Gone. */
	public static final int Gone=410;
	
	/** The Constant Length_Required. */
	public static final int Length_Required=411;
	
	/** The Constant Precondition_Failed. */
	public static final int Precondition_Failed=412;
	
	/** The Constant Request_Entity_Too_Large. */
	public static final int Request_Entity_Too_Large=413;
	
	/** The Constant Request_URI_Too_Long. */
	public static final int Request_URI_Too_Long=414;
	
	/** The Constant Unsupported_Media_Type. */
	public static final int Unsupported_Media_Type=415;
	
	/** The Constant Requested_Range_Not_Satisfiable. */
	public static final int Requested_Range_Not_Satisfiable=416;
	
	/** The Constant Expectation_Failed. */
	public static final int Expectation_Failed=417;
	
	/** The Constant Unprocessable_Entity. */
	public static final int Unprocessable_Entity=422;
	 
	
	/** The Constant Internal_Server_Error. */
	public static final int Internal_Server_Error=500;
	
	/** The Constant Not_Implemented. */
	public static final int Not_Implemented=501;
	
	/** The Constant Bad_Gateway. */
	public static final int Bad_Gateway=502;
	
	/** The Constant Service_Unavailable. */
	public static final int Service_Unavailable=503;
	
	/** The Constant Gateway_Timeout. */
	public static final int Gateway_Timeout=504;
	
	/** The Constant HTTP_Version_Not_Supported. */
	public static final int HTTP_Version_Not_Supported=505;
	
	
	
	/**
	 * Gets the msg OK.
	 *
	 * @return the msg OK
	 */
	public static String getMsg_OK(){
		return "The 200 status code is by far the most common returned. It means, simply, that the request was received and understood and is being processed.";
		
	}
	
	/**
	 * Gets the msg created.
	 *
	 * @return the msg created
	 */
	public static String getMsg_Created(){
		return "A 201 status code indicates that a request was successful and as a result, a resource has been created (for example a new page).";
		
	}
	
	/**
	 * Gets the msg accepted.
	 *
	 * @return the msg accepted
	 */
	public static String getMsg_Accepted(){
		return "The status code 202 indicates that server has received and understood the request, and that it has been accepted for processing, although it may not be processed immediately.";
		
	}
	
	
	
	/**
	 * Gets the msg non authoritative information.
	 *
	 * @return the msg non authoritative information
	 */
	public static String getMsg_Non_Authoritative_Information(){
		return "A 203 status code means that the request was received and understood, and that information sent back about the response is from a third party, rather than the original server. This is virtually identical in meaning to a 200 status code.";
		
	}
	
	/**
	 * Gets the msg no content.
	 *
	 * @return the msg no content
	 */
	public static String getMsg_No_Content(){
		return "The 204 status code means that the request was received and understood, but that there is no need to send any data back.";
		
	}
	
	/**
	 * Gets the msg reset content.
	 *
	 * @return the msg reset content
	 */
	public static String getMsg_Reset_Content(){
		return "The 205 status code is a request from the server to the client to reset the document from which the original request was sent. For example, if a user fills out a form, and submits it, a status code of 205 means the server is asking the browser to clear the form.";
		
	}
	
	
	
	/**
	 * Gets the msg partial content.
	 *
	 * @return the msg partial content
	 */
	public static String getMsg_Partial_Content(){
		return "A status code of 206 is a response to a request for part of a document. This is used by advanced caching tools, when a user agent requests only a small part of a page, and just that section is returned.";
		
	}
	
	/**
	 * Gets the msg multiple choices.
	 *
	 * @return the msg multiple choices
	 */
	public static String getMsg_Multiple_Choices(){
		return "The 300 status code indicates that a resource has moved. The response will also include a list of locations from which the user agent can select the most appropriate.";
		
	}
	
	/**
	 * Gets the msg moved permanently.
	 *
	 * @return the msg moved permanently
	 */
	public static String getMsg_Moved_Permanently(){
		return "A status code of 301 tells a client that the resource they asked for has permanently moved to a new location. The response should also include this location. It tells the client to use the new URL the next time it wants to fetch the same resource.";
		
	}
	
	
	
	/**
	 * Gets the msg found.
	 *
	 * @return the msg found
	 */
	public static String getMsg_Found(){
		return "A status code of 302 tells a client that the resource they asked for has temporarily moved to a new location. The response should also include this location. It tells the client that it should carry on using the same URL to access this resource.";
		
	}
	
	/**
	 * Gets the msg see other.
	 *
	 * @return the msg see other
	 */
	public static String getMsg_See_Other(){
		return "A 303 status code indicates that the response to the request can be found at the specified URL, and should be retrieved from there. It does not mean that something has moved - it is simply specifying the address at which the response to the request can be found.";
		
	}
	
	/**
	 * Gets the msg not modified.
	 *
	 * @return the msg not modified
	 */
	public static String getMsg_Not_Modified(){
		return "The 304 status code is sent in response to a request (for a document) that asked for the document only if it was newer than the one the client already had. Normally, when a document is cached, the date it was cached is stored. The next time the document is viewed, the client asks the server if the document has changed. If not, the client just reloads the document from the cache.";
		
	}
	
	
	
	/**
	 * Gets the msg use proxy.
	 *
	 * @return the msg use proxy
	 */
	public static String getMsg_Use_Proxy(){
		return "A 305 status code tells the client that the requested resource has to be reached through a proxy, which will be specified in the response.";
		
	}
	
	/**
	 * Gets the msg temporary redirect.
	 *
	 * @return the msg temporary redirect
	 */
	public static String getMsg_Temporary_Redirect(){
		return "307 is the status code that is sent when a document is temporarily available at a different URL, which is also returned. There is very little difference between a 302 status code and a 307 status code. 307 was created as another, less ambiguous, version of the 302 status code.";
		
	}
	
	/**
	 * Gets the msg bad request.
	 *
	 * @return the msg bad request
	 */
	public static String getMsg_Bad_Request(){
		return "A status code of 400 indicates that the server did not understand the request due to bad syntax.";
		
	}
	
	
	
	/**
	 * Gets the msg unauthorized.
	 *
	 * @return the msg unauthorized
	 */
	public static String getMsg_Unauthorized(){
		return "A 401 status code indicates that before a resource can be accessed, the client must be authorised by the server.";
		
	}
	
	/**
	 * Gets the msg payment required.
	 *
	 * @return the msg payment required
	 */
	public static String getMsg_Payment_Required(){
		return "The 402 status code is not currently in use, being listed as reserved for future use.";
		
	}
	
	/**
	 * Gets the msg forbidden.
	 *
	 * @return the msg forbidden
	 */
	public static String getMsg_Forbidden(){
		return "A 403 status code indicates that the client cannot access the requested resource. That might mean that the wrong username and password were sent in the request, or that the permissions on the server do not allow what was being asked.";
		
	}
	
	
	
	/**
	 * Gets the msg not found.
	 *
	 * @return the msg not found
	 */
	public static String getMsg_Not_Found(){
		return "The best known of them all, the 404 status code indicates that the requested resource was not found at the URL given, and the server has no idea how long for.";
		
	}
	
	/**
	 * Gets the msg method not allowed.
	 *
	 * @return the msg method not allowed
	 */
	public static String getMsg_Method_Not_Allowed(){
		return "A 405 status code is returned when the client has tried to use a request method that the server does not allow. Request methods that are allowed should be sent with the response (common request methods are POST and GET).";
		
	}
	
	/**
	 * Gets the msg not acceptable.
	 *
	 * @return the msg not acceptable
	 */
	public static String getMsg_Not_Acceptable(){
		return "The 406 status code means that, although the server understood and processed the request, the response is of a form the client cannot understand. A client sends, as part of a request, headers indicating what types of data it can use, and a 406 error is returned when the response is of a type not in that list.";
		
	}
	
	
	
	/**
	 * Gets the msg proxy authentication required.
	 *
	 * @return the msg proxy authentication required
	 */
	public static String getMsg_Proxy_Authentication_Required(){
		return "The 407 status code is very similar to the 401 status code, and means that the client must be authorised by the proxy before the request can proceed.";
		
	}
	
	/**
	 * Gets the msg request timeout.
	 *
	 * @return the msg request timeout
	 */
	public static String getMsg_Request_Timeout(){
		return "A 408 status code means that the client did not produce a request quickly enough. A server is set to only wait a certain amount of time for responses from clients, and a 408 status code indicates that time has passed.";
		
	}
	
	/**
	 * Gets the msg conflict.
	 *
	 * @return the msg conflict
	 */
	public static String getMsg_Conflict(){
		return "A 409 status code indicates that the server was unable to complete the request, often because a file would need to be editted, created or deleted, and that file cannot be editted, created or deleted.";
		
	}
	
	
	
	/**
	 * Gets the msg gone.
	 *
	 * @return the msg gone
	 */
	public static String getMsg_Gone(){
		return "A 410 status code is the 404's lesser known cousin. It indicates that a resource has permanently gone (a 404 status code gives no indication if a resource has gine permanently or temporarily), and no new address is known for it.";
		
	}
	
	/**
	 * Gets the msg length required.
	 *
	 * @return the msg length required
	 */
	public static String getMsg_Length_Required(){
		return "The 411 status code occurs when a server refuses to process a request because a content length was not specified.";
		
	}
	
	/**
	 * Gets the msg precondition failed.
	 *
	 * @return the msg precondition failed
	 */
	public static String getMsg_Precondition_Failed(){
		return "A 412 status code indicates that one of the conditions the request was made under has failed.";
		
	}
	
	
	
	/**
	 * Gets the msg request entity too large.
	 *
	 * @return the msg request entity too large
	 */
	public static String getMsg_Request_Entity_Too_Large(){
		return "The 413 status code indicates that the request was larger than the server is able to handle, either due to physical constraints or to settings. Usually, this occurs when a file is sent using the POST method from a form, and the file is larger than the maximum size allowed in the server settings.";
		
	}
	
	/**
	 * Gets the msg request UR I too long.
	 *
	 * @return the msg request UR I too long
	 */
	public static String getMsg_Request_URI_Too_Long(){
		return "The 414 status code indicates the the URL requested by the client was longer than it can process.";
		
	}
	
	/**
	 * Gets the msg unsupported media type.
	 *
	 * @return the msg unsupported media type
	 */
	public static String getMsg_Unsupported_Media_Type(){
		return "A 415 status code is returned by a server to indicate that part of the request was in an unsupported format.";
		
	}
	
	
	
	
	/**
	 * Gets the msg requested range not satisfiable.
	 *
	 * @return the msg requested range not satisfiable
	 */
	public static String getMsg_Requested_Range_Not_Satisfiable(){
		return "A 416 status code indicates that the server was unable to fulfill the request. This may be, for example, because the client asked for the 800th-900th bytes of a document, but the document was only 200 bytes long.";
		
	}
	
	/**
	 * Gets the msg expectation failed.
	 *
	 * @return the msg expectation failed
	 */
	public static String getMsg_Expectation_Failed(){
		return "The 417 status code means that the server was unable to properly complete the request. One of the headers sent to the server, the Expect header, indicated an expectation the server could not meet.";
		
	}
	
	/**
	 * Gets the msg unprocessable entity.
	 *
	 * @return the msg unprocessable entity
	 */
	public static String getMsg_Unprocessable_Entity(){
		return "The request was well-formed but was unable to be followed due to semantic errors.";
	}
	
	
	
	
	/**
	 * Gets the msg internal server error.
	 *
	 * @return the msg internal server error
	 */
	public static String getMsg_Internal_Server_Error(){
		return "A 500 status code (all too often seen by Perl programmers) indicates that the server encountered something it didn't expect and was unable to complete the request.";
		
	}
	
	
	
	/**
	 * Gets the msg not implemented.
	 *
	 * @return the msg not implemented
	 */
	public static String getMsg_Not_Implemented(){
		return "The 501 status code indicates that the server does not support all that is needed for the request to be completed.";
		
	}
	
	/**
	 * Gets the msg bad gateway.
	 *
	 * @return the msg bad gateway
	 */
	public static String getMsg_Bad_Gateway(){
		return "A 502 status code indicates that a server, while acting as a proxy, received a response from a server further upstream that it judged invalid.";
		
	}
	
	/**
	 * Gets the msg service unavailable.
	 *
	 * @return the msg service unavailable
	 */
	public static String getMsg_Service_Unavailable(){
		return "A 503 status code is most often seen on extremely busy servers, and it indicates that the server was unable to complete the request due to a server overload.";
		
	}
	
	
	
	/**
	 * Gets the msg gateway timeout.
	 *
	 * @return the msg gateway timeout
	 */
	public static String getMsg_Gateway_Timeout(){
		return "A 504 status code is returned when a server acting as a proxy has waited too long for a response from a server further upstream.";
		
	}
	

	/**
	 * Gets the msg HTT P version not supported.
	 *
	 * @return the msg HTT P version not supported
	 */
	public static String getMsg_HTTP_Version_Not_Supported(){
		return "A 505 status code is returned when the HTTP version indicated in the request is no supported. The response should indicate which HTTP versions are supported.";
		
	}

}
