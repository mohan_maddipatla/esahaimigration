package com.esahai.util;

import org.springframework.stereotype.Component;

// TODO: Auto-generated Javadoc
/**
 * The Class HttpResponseStatusCode.
 */
@Component("responseMessages")
public class HttpResponseStatusCode {

	/** The Constant SUCCESS. */
	public static final String SUCCESS = "300";
	
	/** The Constant UNAUTHORISED. */
	//public static final String INVALID_TOKEN="204";
	public static final String UNAUTHORISED="401";
	
	/** The Constant BOOKED. */
	public static final String BOOKED="415";
	
	/** The Constant CLOSED. */
	public static final String CLOSED="500";
	
	/** The Constant CANCELLED. */
	public static final String CANCELLED="411";
	
	/** The Constant NO_AMBULANCES. */
	public static final String NO_AMBULANCES="303"; 
	
	/** The Constant INVALIDINPUT. */
	public static final String INVALIDINPUT="410";
	
	/** The Constant EROROPIN. */
	public static final String EROROPIN="411"; 
	
	/** The Constant ERRORAMBFREE. */
	public static final String ERRORAMBFREE="412";
	
	/** The Constant NO_TRACK. */
	public static final String NO_TRACK="413";

	/** The Constant STATUS_CODE. */
	public static final String STATUS_CODE="statusCode";
	
	/** The Constant STATUS_MESSAGE. */
	public static final String STATUS_MESSAGE="statusMessage";
	
	/** The Constant TABLE_NAME. */
	public static final String TABLE_NAME="tableName";
	
	/** The Constant RESULT. */
	public static final String RESULT="Result";

	/** The Constant CREATE_SUCCESS. */
	public static final String CREATE_SUCCESS= "Successfully Created";
	
	/** The Constant UPDATE_SUCCESS. */
	public static final String UPDATE_SUCCESS= "Updated Successfully";
	
	/** The Constant DELETE_SUCCESS. */
	public static final String DELETE_SUCCESS= "Deleted successfully";
	
	/** The Constant INSERT_SUCCESS. */
	public static final String INSERT_SUCCESS= "Inserted Successfully";
	
	/** The Constant SAVE_SUCCESS. */
	public static final String SAVE_SUCCESS= "Successfully Saved";


	/** The Constant s100. */
	public static final String s100= "Successfully Added Column";
	
	/** The Constant s101. */
	public static final String s101= "Successfully Deleted Column";
	
	/** The Constant s102. */
	public static final String s102= "Successfully Updated Column";
	
	/** The Constant s103. */
	public static final String s103= "success";
	
	/** The Constant s104. */
	public static final String s104= "Invalid token";
	
	/** The Constant s105. */
	public static final String s105= "No User";
	
	/** The Constant s106. */
	public static final String s106= "Unauthorized";
	
	/** The Constant s107. */
	public static final String s107= "Error occured while getting the data";
	
	/** The Constant s108. */
	public static final String s108= "err";
	
	/** The Constant s109. */
	public static final String s109= "No Roles Assigned";
	
	/** The Constant s110. */
	public static final String s110= "No rows affected";
	
	/** The Constant s111. */
	public static final String s111= "No Services";
	
	/** The Constant s112. */
	public static final String s112= "No data";
	
	/** The Constant s113. */
	public static final String s113= "Duplicate entry, please try with valid data";

	/** The Constant s114. */
	public static final String s114= "Insertion Failed";
	
	/** The Constant s115. */
	public static final String s115= "Updation Failed";
	
	/** The Constant s116. */
	public static final String s116= "Error occured while inserting";
	
	/** The Constant s117. */
	public static final String s117= "Error occured while updating";
	
	/** The Constant s118. */
	public static final String s118= "Error occured while deleting";
	
	/** The Constant s119. */
	public static final String s119= "Error occured while adding column";
	
	/** The Constant s120. */
	public static final String s120= "Error occured while dropping column";
	
	/** The Constant s121. */
	public static final String s121= "DUP";
	
	/** The Constant s122. */
	public static final String s122= "null";
	
	/** The Constant s123. */
	public static final String s123= "NotMatched";
	
	/** The Constant s124. */
	public static final String s124= "noUser";
	
	/** The Constant s125. */
	public static final String s125= "No user found";
	
	/** The Constant s126. */
	public static final String s126= "Invalid Request";
	
	/** The Constant s127. */
	public static final String s127= "Your OTP has been sent to registered mobile, please enter to proceed";
	
	/** The Constant s128. */
	public static final String s128= "Invalid OTP";
	
	/** The Constant s129. */
	public static final String s129="inActive";





	/** The Constant OK. */
	public static final String OK= "300";
	
	/** The Constant INVALID_TOKEN. */
	public static final String INVALID_TOKEN= "204";
	
	/** The Constant UNAUTHORIZED. */
	public static final String UNAUTHORIZED= "401";
	
	/** The Constant INTERNAL_ERROR. */
	public static final String INTERNAL_ERROR= "500";
	
	/** The Constant DUPLICATE. */
	public static final String DUPLICATE= "222";
	
	/** The Constant BAD_REQUEST. */
	public static final String BAD_REQUEST= "400";
	
	/** The Constant NO_USER. */
	public static final String NO_USER= "201";

	/** The Constant TABLE_USER_APP_ROLES. */
	public static final String TABLE_USER_APP_ROLES= "user_app_roles";
	
	/** The Constant TABLE_ACCESS_ROLES. */
	public static final String TABLE_ACCESS_ROLES= "access_roles";
	
	/** The Constant TABLE_APPS. */
	public static final String TABLE_APPS= "Apps";
	
	/** The Constant TABLE_SERVICE_FIELD_VALUES. */
	public static final String TABLE_SERVICE_FIELD_VALUES= "serviceFieldValues";
	
	/** The Constant TABLE_BACKEND_USERS. */
	public static final String TABLE_BACKEND_USERS= "backend_users";
	
	/** The Constant TABLE_ROLES. */
	public static final String TABLE_ROLES= "roles";
	
	/** The Constant TABLE_SERVICES. */
	public static final String TABLE_SERVICES= "services";
	
	/** The Constant TABLE_APP_USER. */
	public static final String TABLE_APP_USER= "app_user";
	
	/** The Constant TABLE_DEVICE. */
	public static final String TABLE_DEVICE= "deviceTable";
	
	/** The Constant TABLE_ACTIVITY_LOG. */
	public static final String TABLE_ACTIVITY_LOG= "activity_log";
	
	/** The Constant TABLE_ERROR_LOG. */
	public static final String TABLE_ERROR_LOG= "error_log";
	
	/** The Constant TABLE_MESSAGE_LOG. */
	public static final String TABLE_MESSAGE_LOG= "message_log";
	
	/** The Constant TABLE_SERVICES_TABLES. */
	public static final String TABLE_SERVICES_TABLES= "serviceTables";
	
	/** The Constant TABLE_API_LOG. */
	public static final String TABLE_API_LOG= "api_log";
	
	/** The Constant TABLE_APP_META_DATA. */
	public static final String TABLE_APP_META_DATA= "app_meta_data";
	
	/** The Constant TABLE_CIRCLE_MASTER. */
	public static final String TABLE_CIRCLE_MASTER= "circle_master";


}
