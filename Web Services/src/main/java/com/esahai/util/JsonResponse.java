package com.esahai.util;

// TODO: Auto-generated Javadoc
/**
 * The Class JsonResponse.
 *
 * @author Swathi.P
 */
public class JsonResponse {
	
	/** The status code. */
	private Object statusCode;
	
	/** The status data. */
	private String statusData;
	
	/** The response data. */
	private Object responseData;
	
	/**
	 * Instantiates a new json response.
	 */
	public JsonResponse() {
	}
	
	/**
	 * Instantiates a new json response.
	 *
	 * @param statusCode the status code
	 * @param statusData the status data
	 * @param responseData the response data
	 */
	public JsonResponse(Object statusCode, String statusData, Object responseData) {
		super();
		this.statusCode = statusCode;
		this.statusData = statusData;
		this.responseData = responseData;
	}

	/**
	 * Gets the status code.
	 *
	 * @return the status code
	 */
	public Object getStatusCode() {
		return statusCode;
	}

	/**
	 * Sets the status code.
	 *
	 * @param statusCode the new status code
	 */
	public void setStatusCode(Object statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * Gets the status data.
	 *
	 * @return the status data
	 */
	public String getStatusData() {
		return statusData;
	}

	/**
	 * Sets the status data.
	 *
	 * @param statusData the new status data
	 */
	public void setStatusData(String statusData) {
		this.statusData = statusData;
	}

	/**
	 * Gets the response data.
	 *
	 * @return the response data
	 */
	public Object getResponseData() {
		return responseData;
	}

	/**
	 * Sets the response data.
	 *
	 * @param responseData the new response data
	 */
	public void setResponseData(Object responseData) {
		this.responseData = responseData;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "JsonResponse [statusCode=" + statusCode + ", statusData=" + statusData + ", responseData="
				+ responseData + "]";
	}
}
