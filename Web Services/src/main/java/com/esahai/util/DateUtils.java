package com.esahai.util;

import java.sql.Timestamp;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * The Class DateUtils.
 */
public class DateUtils {
	
	/**
	 * Gets the date.
	 *
	 * @param date the date
	 * @return the date
	 */
	public static Timestamp getDate(Date date){
		return new Timestamp(date.getTime());
	}
}
