package com.esahai.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.apache.log4j.Logger;


// TODO: Auto-generated Javadoc
/**
 * @author Santhosh
 * 
 *         The Class OTPSender.
 */
public class OTPSender {

	/** The Constant log. */
	private static final Logger log = Logger.getLogger(OTPSender.class);

	/**
	 * Send six digit OTP number.
	 *
	 * @param mobileNumber
	 *            the mobile number
	 * @param otp
	 *            the otp
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void sendSixDigitOTPNumber(long mobileNumber, int otp) throws IOException {
		String postData = "";
		String retval = "";
		String mobilenumber = String.valueOf(mobileNumber);
		String user = SMSCountryCredentials.USER;
		String password = SMSCountryCredentials.PASSWORD;
		String message = SMSCountryCredentials.MESSAGE;
		String sid = SMSCountryCredentials.SID;
		String messageType = SMSCountryCredentials.MESSAGE_TYPE;
		String deliveryReport = SMSCountryCredentials.DELIVERY_REPORT;
		String formattedMessageWithOTP = message + " " + otp;

		log.info("user = " + user);
		log.info("password = " + password);
		log.info("formattedMessageWithOTP = " + formattedMessageWithOTP);
		log.info("sid = " + sid);
		log.info("messageType = " + messageType);
		log.info("deliveryReport = " + deliveryReport);
		log.info("mobilenumber = " + mobilenumber);

		postData += "User=" + URLEncoder.encode(user, "UTF-8") + "&passwd=" + password + "&mobilenumber=" + mobilenumber
				+ "&message=" + URLEncoder.encode(formattedMessageWithOTP, "UTF-8") + "&sid=" + sid + "&mtype="
				+ messageType + "&DR=" + deliveryReport;
		URL url = new URL("http://smscountry.com/SMSCwebservice_Bulk.aspx");
		HttpURLConnection urlconnection = (HttpURLConnection) url.openConnection();
		urlconnection.setRequestMethod("POST");
		urlconnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		urlconnection.setDoOutput(true);
		OutputStreamWriter out = new OutputStreamWriter(urlconnection.getOutputStream());
		out.write(postData);
		out.close();
		BufferedReader in = new BufferedReader(new InputStreamReader(urlconnection.getInputStream()));
		String decodedString;
		while ((decodedString = in.readLine()) != null) {
			retval += decodedString;
		}
		in.close();
		log.info("Response from SMSCountry =" + retval);
	}

}
