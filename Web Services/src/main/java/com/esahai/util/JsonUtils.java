package com.esahai.util;

import com.google.gson.Gson;

// TODO: Auto-generated Javadoc
/**
 * The Class JsonUtils.
 */
public class JsonUtils {
	
	/**
	 * Gets the json.
	 *
	 * @param obj the obj
	 * @return the json
	 */
	public static String getJson(Object obj){
		return new Gson().toJson(obj);
	}
}
