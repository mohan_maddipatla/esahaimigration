package com.esahai.util;

import org.apache.log4j.Logger;

// TODO: Auto-generated Javadoc
/**
 * @author Santhosh
 * The Class SMSCountryCredentials.
 */
public class SMSCountryCredentials {
	
	/** The Constant log. */
	private static final Logger log = Logger.getLogger(SMSCountryCredentials.class);


	/** The Constant USER. */
	public static final String USER = "MyInd";
	
	/** The Constant PASSWORD. */
	public static final String PASSWORD = "Password1";
	
	/** The Constant MESSAGE. */
	public static final String MESSAGE = "Your One-Time Password(OTP) is - ";
	
	/** The Constant SID. */
	public static final String SID = "ESahai";
	
	/** The Constant MESSAGE_TYPE. */
	public static final String MESSAGE_TYPE = "N";
	
	/** The Constant DELIVERY_REPORT. */
	public static final String DELIVERY_REPORT = "Y";
	
}
