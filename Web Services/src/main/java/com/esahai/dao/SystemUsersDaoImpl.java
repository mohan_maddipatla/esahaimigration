package com.esahai.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.esahai.entity.SystemUsers;

// TODO: Auto-generated Javadoc
/**
 * The SystemUsersDaoImpl implements SystemUsersDao application that simply
 * displays list of system users , to save,update and delete the system users,
 * to the standard output.
 *
 * @author Satheesh E
 * @version 1.0
 * @since 2017-07-03
 */

@Repository("systemUsersDaoImpl")
public class SystemUsersDaoImpl implements SystemUsersDao {

	/** The Constant logger. */
	public static final Logger logger = Logger.getLogger(SystemUsersDaoImpl.class);

	/** The session factory. */
	private SessionFactory sessionFactory;

	/**
	 * Sets the session factory.
	 *
	 * @param sessionFactory
	 *            the new session factory
	 */
	// setters for Session Factory
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.esahai.dao.SystemUsersDao#getSystemUsers()
	 */
	@SuppressWarnings("unchecked")
	public List<SystemUsers> getSystemUsers() {
		List<SystemUsers> list = null;
		Session session = null;
		Criteria criteria = null;

		try {
			// getting session object from session factory
			session = sessionFactory.openSession();
			criteria = session.createCriteria(SystemUsers.class);
			list = criteria.list();
			logger.info("ascending order = "+ list);
			session.close();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("exception occured = "+ list);
			logger.error(e);
			return list;
		}

	}

	/* (non-Javadoc)
	 * @see com.esahai.dao.SystemUsersDao#saveSystemUsers(com.esahai.entity.SystemUsers)
	 */
	public boolean saveSystemUsers(SystemUsers systemUsers) {
		logger.info("system Users type object = "+ systemUsers);

		Session session = null;
		try {
			// getting session object from session factory
			session = sessionFactory.openSession();
			// getting transaction object from session object
			session.beginTransaction();
			session.save(systemUsers);
			logger.info("Inserted Successfully = "+ systemUsers);
			session.getTransaction().commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Message = "+ e.getMessage());
			logger.error(e);
			return false;
		} finally {
			session.close();
		}
	}
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.esahai.dao.EntityTypeDao#updateEntityType(com.esahai.entity.
	 * EntityTypes)
	 */
	public boolean updateSystemUsers(SystemUsers systemUsers) {
		Session session = null;
		try {
			// getting session object from session factory
			session = sessionFactory.openSession();
			// getting transaction object from session object
			session.beginTransaction();
			String SQL_FOR_SYSTEM_USERS_UPDATE = "Update SystemUsers set email=:email, firstName=:firstName, lastName=:lastName, password=:password, displayName=:displayName, isActive=:isActive, token=:token, email_invitation_accepted=:email_invitation_accepted, role_id=:role_id, group_id=:group_id, updated_datetime=:updated_datetime where id =:id";

			Query query = session.createQuery(SQL_FOR_SYSTEM_USERS_UPDATE);

			query.setParameter("email", systemUsers.getEmail());
			query.setParameter("firstName", systemUsers.getFirstName());
			query.setParameter("lastName", systemUsers.getLastName());
			query.setParameter("password", systemUsers.getPassword());
			query.setParameter("displayName", systemUsers.getDisplayName());
			query.setParameter("isActive", systemUsers.getIsActive());
			query.setParameter("token", systemUsers.getToken());
			query.setParameter("email_invitation_accepted", systemUsers.getEmail_invitation_accepted());
			query.setParameter("role_id", systemUsers.getRole_id());
			query.setParameter("group_id", systemUsers.getGroup_id());
			query.setParameter("updated_datetime", systemUsers.getUpdated_datetime());
			query.setParameter("id", systemUsers.getId());

			query.executeUpdate();
			logger.info("Updated Successfully = "+ systemUsers);
			session.getTransaction().commit();

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Message = "+ e.getMessage());
			logger.error(e);
			return false;

		} finally {
			session.close();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.esahai.dao.EntityTypeDao#deleteEntityType(int, java.lang.String)
	 */
	public boolean deleteSystemUsers(int id, String status) {
		Session session = null;
		try {
			// getting session object from session factory
			session = sessionFactory.openSession();
			// getting transaction object from session object
			session.beginTransaction();
			String SQL_FOR_SYSTEM_USERS_DELETE = "Update SystemUsers set status=:status where id =:id";
			Query query = session.createQuery(SQL_FOR_SYSTEM_USERS_DELETE);
			query.setParameter("id", id);
			query.setParameter("status", status);
			query.executeUpdate();
			session.getTransaction().commit();

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Message = "+ e.getMessage());
			logger.error(e);
			return false;
		} finally {
			session.close();
		}
	}

}
