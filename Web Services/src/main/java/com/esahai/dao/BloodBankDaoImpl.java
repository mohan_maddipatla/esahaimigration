package com.esahai.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

import com.esahai.entity.BloodBank;

/**
 * The Class BloodBankDaoImpl.
 */
/**
 * The BloodBankDaoImpl implements BloodBankDao application that simply
 * displays list of BloodBank to the standard output.
 *
 * @author Swathi P
 * @version 1.0
 * @since 2017-06-30
 */
@Repository("bloodBankDaoImpl")
public class BloodBankDaoImpl implements BloodBankDao {

	/** The Constant logger. */
	public static final Logger logger = Logger.getLogger(BloodBankDaoImpl.class);

	/** The session factory. */
	private SessionFactory sessionFactory;

	/**
	 * Sets the session factory.
	 *
	 * @param sessionFactory the new session factory
	 */
	// setters for Session Factory
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	// This method used for the fetch the all BloodBank Details: started

	/* (non-Javadoc)
	 * @see com.esahai.dao.BloodBankDao#getBloodBankDetails()
	 */
	@SuppressWarnings("unchecked")
	public List<BloodBank> getBloodBankDetails() {
		
		List<BloodBank> list = null;
		Session session=null;
		Criteria criteria=null;
		try {
			session= sessionFactory.openSession();
			criteria = session.createCriteria(BloodBank.class);
			logger.info("criteria object = "+ criteria);
			criteria.addOrder(Order.asc("name"));
			list = criteria.list();
			logger.info("ascending order = "+ list);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("exception occured = "+ e.getMessage());
			logger.error(e);
			return list;
		}finally {
			session.close();
		}
	}

}
