package com.esahai.dao;

import java.util.List;

import com.esahai.entity.EmergencyBooking;

// TODO: Auto-generated Javadoc
/**
 * The Interface EmergencyBookingDao.
 */
public interface EmergencyBookingDao {
	
	/**
	 * Gets the all emergency bookings.
	 *
	 * @return the all emergency bookings
	 */
	public List<EmergencyBooking> getAllEmergencyBookings();
}