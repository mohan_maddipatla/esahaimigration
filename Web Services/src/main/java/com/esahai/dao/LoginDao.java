package com.esahai.dao;

import com.esahai.entity.LoginJsonEntity;
import com.esahai.entity.SystemUsers;

// TODO: Auto-generated Javadoc
/**
 * @author Santhosh
 * The Interface LoginDao.
 */
public interface LoginDao {
	
	/**
	 * Authenticate user.
	 *
	 * @param systemUsers the system users
	 * @return the login json entity
	 */
	LoginJsonEntity authenticateUser(SystemUsers systemUsers);

}
