package com.esahai.dao;

import java.util.List;

import com.esahai.entity.Countries;

/**
 * The Interface CountryDao.
 */
public interface CountriesDao {

	/**
	 * Gets the countries list entity.
	 *
	 * @return the countries list entity
	 */
	List<Countries> getCountriesList();
}
