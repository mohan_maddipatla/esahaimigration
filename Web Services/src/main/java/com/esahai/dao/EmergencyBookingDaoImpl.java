package com.esahai.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

import com.esahai.entity.EmergencyBooking;
// TODO: Auto-generated Javadoc

/**
 * The Class EmergencyBookingDaoImpl.
 */

/**
 * The EmergencyBookingDaoImpl implements EmergencyBookingDao application that simply
 * displays list of EmergencyBooking to the standard output.
 *
 * @author Swathi P
 * @version 1.0
 * @since 2017-06-26
 */

@Repository("emergencyBookingDaoImpl")
public class EmergencyBookingDaoImpl implements EmergencyBookingDao {

	/** The Constant logger. */
	public static final Logger logger = Logger.getLogger(EmergencyBookingDaoImpl.class);

	/** The session factory. */
	private SessionFactory sessionFactory;
	
    /**
     * Sets the session factory.
     *
     * @param sessionFactory the new session factory
     */
    //setters for Session Factory 
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/* (non-Javadoc)
	 * @see com.esahai.dao.EmergencyBookingDao#getAllEmergencyBookings()
	 */
	// This method used for the fetch the all EmergencyBooking Details: started
	@SuppressWarnings("unchecked")
	public List<EmergencyBooking> getAllEmergencyBookings() {
		Session session=null;
		Criteria criteria=null;
		List<EmergencyBooking> list=null;
		try{
		session = sessionFactory.openSession();
	    criteria = session.createCriteria(EmergencyBooking.class);
		logger.info("criteria object = "+ criteria);
		criteria.addOrder(Order.asc("customerName"));
		list = criteria.list();
		logger.debug("ascending order = "+ list);
		session.close();
		return list;
		}catch(Exception e){
			logger.info("exception occured = "+ e.getMessage());
			e.printStackTrace();
			
			return list;

		}
	}
	// This method used for the fetch the all EmergencyBooking Details: end
}
