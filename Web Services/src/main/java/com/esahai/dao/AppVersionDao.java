package com.esahai.dao;

import java.util.List;

import com.esahai.entity.AppVersion;

/**
 * The Interface AppVersionDao.
 */
public interface AppVersionDao {


	/**
	 * Gets the app version.
	 *
	 * @return the app version
	 */
	public abstract List<AppVersion> getAppVersion(String app_type);
}
