package com.esahai.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.esahai.entity.AppVersion;

@Repository("appVersionDaoImpl")
public class AppVersionDaoImpl implements AppVersionDao {

	/** The Constant logger. */
	public static final Logger logger = Logger.getLogger(AppVersionDaoImpl.class);

	/** The session factory. */
	private SessionFactory sessionFactory;

	/**
	 * Sets the session factory.
	 *
	 * @param sessionFactory
	 *            the new session factory
	 */
	// setters for Session Factory
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.esahai.dao.CountryDao#getCountriesList()
	 */
	@SuppressWarnings("unchecked")
	public List<AppVersion> getAppVersion(String app_type) {
		List<AppVersion> list = null;
		Session session = null;

		try {
			// getting session object from session factory
			session = sessionFactory.openSession();
			Query query = session.createQuery("From AppVersion where app_type=:app_type");
			query.setParameter("app_type", app_type);
			list = query.list();
			logger.info("ascending order = "+ list);
			session.close();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("exception occured = "+ e.getMessage());
			return list;
		}

	}
}
