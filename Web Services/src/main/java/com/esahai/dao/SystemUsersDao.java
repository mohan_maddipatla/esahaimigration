package com.esahai.dao;

import java.util.List;

import com.esahai.entity.SystemUsers;

/**
 * The Interface SystemUsersDao.
 */
public interface SystemUsersDao {

	/**
	 * Gets the system users.
	 *
	 * @return the system users
	 */
	public List<SystemUsers> getSystemUsers();

	/**
	 * Save entity type.
	 *
	 * @param systemUsers
	 *            the system users
	 * @return true, if successful
	 */
	public boolean saveSystemUsers(SystemUsers systemUsers);

	/**
	 * Update entity type.
	 *
	 * @param systemUsers
	 *            the system users
	 * @return true, if successful
	 */
	public boolean updateSystemUsers(SystemUsers systemUsers);

	/**
	 * Delete entity type.
	 *
	 * @param id
	 *            the id
	 * @param status
	 *            the status
	 * @return true, if successful
	 */
	public boolean deleteSystemUsers(int id, String status);

}
