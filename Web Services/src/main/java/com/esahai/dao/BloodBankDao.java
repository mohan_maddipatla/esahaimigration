package com.esahai.dao;

import java.util.List;

import com.esahai.entity.BloodBank;

// TODO: Auto-generated Javadoc
/**
 * The Interface BloodBankDao.
 */
public interface BloodBankDao {
	
	/**
	 * Gets the blood bank details.
	 *
	 * @return the blood bank details
	 */
	public List<BloodBank> getBloodBankDetails();
}