package com.esahai.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

import com.esahai.entity.Countries;

/**
 * The Class CountiesDaoImpl.
 */
@Repository("countriesDaoImpl")
public class CountriesDaoImpl implements CountriesDao {

	/** The Constant logger. */
	public static final Logger logger = Logger.getLogger(CountriesDaoImpl.class);

	/** The session factory. */
	private SessionFactory sessionFactory;

	/**
	 * Sets the session factory.
	 *
	 * @param sessionFactory
	 *            the new session factory
	 */
	// setters for Session Factory
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.esahai.dao.CountryDao#getCountriesList()
	 */
	@SuppressWarnings("unchecked")
	public List<Countries> getCountriesList() {
		List<Countries> list = null;
		Session session = null;
		Criteria criteria = null;

		try {
			// getting session object from session factory
			session = sessionFactory.openSession();
			criteria = session.createCriteria(Countries.class);
			criteria.addOrder(Order.asc("country_name"));
			list = criteria.list();
			logger.info("ascending order = " + list);
			session.close();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("exception occured = " + e.getMessage());
			logger.error(e);
			return list;
		}

	}
}
