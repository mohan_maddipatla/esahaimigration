package com.esahai.dao;

import java.sql.Timestamp;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.esahai.entity.LoginJsonEntity;
import com.esahai.entity.SystemUsers;

// TODO: Auto-generated Javadoc
/**
 * @author Santhosh
 * The Class LoginDao.
 */
@Repository("loginDaoImpl")
public class LoginDaoImpl {

	/** The Constant log. */
	private static final Logger log = Logger.getLogger(LoginDaoImpl.class);

	/** The session factory. */
	private SessionFactory sessionFactory;

	Criteria criteria = null;

	/**
	 * Sets the session factory.
	 *
	 * @param sessionFactory
	 *            the new session factory
	 */
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/**
	 * Authenticate user.
	 *
	 * @param systemUsers
	 *            the system users
	 * @return true, if successful
	 */
		public LoginJsonEntity authenticateUser(SystemUsers systemUsers) {
		LoginJsonEntity loginJsonEntity=null;
		Session session = sessionFactory.openSession();
		criteria = session.createCriteria(SystemUsers.class);
		criteria.add(Restrictions.eq("email", systemUsers.getEmail()));
		criteria.add(Restrictions.eq("password", systemUsers.getPassword()));
		@SuppressWarnings("unchecked")
		List<SystemUsers> userList = criteria.list();
		if (userList.size() > 0) {
			Query query = session
					.createSQLQuery("select s.id,s.email,s.firstName,s.lastName,s.displayName,s.isActive,s.token,"
							+ "s.email_invitation_accepted,s.role_id,s.group_id,s.created_datetime,s.updated_datetime,a.role_name from system_users s join "
							+ "app_roles a on s.role_id=a.id and s.email = :emailId");
			query.setParameter("emailId", systemUsers.getEmail());
			List<LoginJsonEntity> list = query.list();
			Iterator iterator= list.iterator();
			while(iterator.hasNext()){
		        Object[] obj= (Object[]) iterator.next();
		        loginJsonEntity= new LoginJsonEntity();
		        loginJsonEntity.setId( (Integer) obj[0]);
		        loginJsonEntity.setEmail((String)obj[1]);
		        loginJsonEntity.setFirstName((String)obj[2]);
		        loginJsonEntity.setLastName((String)obj[3]);
		        loginJsonEntity.setDisplayName((String)obj[4]);
		        loginJsonEntity.setActive(((Boolean)obj[5]));
		        loginJsonEntity.setToken((String)obj[6]);
		        loginJsonEntity.setEmailInvitationAccepted((Boolean)obj[7]);
		        loginJsonEntity.setRoleId((Integer)obj[8]);
		        loginJsonEntity.setGroupId((Integer)obj[9]);
		        loginJsonEntity.setCreatedOn((Timestamp)obj[10]);
		        loginJsonEntity.setUpdatedOn((Timestamp)obj[11]);
		        loginJsonEntity.setRoleName((String)obj[12]);
		}
			   log.info("loginJsonEntity =" + loginJsonEntity);
	}
		return loginJsonEntity;


}
}