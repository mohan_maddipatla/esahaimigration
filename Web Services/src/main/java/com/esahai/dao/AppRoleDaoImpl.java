package com.esahai.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.esahai.entity.AppRoles;

// TODO: Auto-generated Javadoc
/**
 * @author Santhosh
 */
@Repository("appRoleDaoImpl")
public class AppRoleDaoImpl implements AppRoleDao {

	/** The session factory. */
	private SessionFactory sessionFactory;

	private static final Logger log = Logger.getLogger(AppRoleDaoImpl.class);

	/**
	 * Sets the session factory.
	 *
	 * @param sessionFactory
	 *            the new session factory
	 */
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/**
	 * Gets the user types list.
	 *
	 * @return the user types list
	 */
	public List<AppRoles> getAppRolesList() {
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("From AppRoles");
		@SuppressWarnings("unchecked")
		List<AppRoles> list = query.list();
		session.close();
		return list;
	}

	/**
	 * Save user types.
	 * 
	 * @param userType
	 *            the user type
	 * @return the string
	 */
	public String saveAppRoles(AppRoles appRoles) {
		Session session = null;
		Transaction transaction = null;
		String savedStatus = null;
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			session.save(appRoles);
			transaction.commit();
			savedStatus = "success";
		} catch (Exception e) {
			e.printStackTrace();
			log.error("message", e);
			savedStatus = "failed";
		} finally {
			session.close();
		}
		return savedStatus;
	}

	/**
	 * Update user types.
	 * 
	 * @param userType
	 *            the user type
	 * @return the string
	 */
	public String updateAppRoles(AppRoles appRoles) {
		Session session = null;
		Transaction transaction = null;
		String updatedStatus = null;
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			String hql = "Update AppRoles set roleName=:roleName , roleType=:roleType where appRoleId = :roleId";
			log.info(appRoles.getRoleName() + " " + appRoles.getRoleType() + " " + appRoles.getAppRoleId());
			Query query = session.createQuery(hql);
			query.setParameter("roleName", appRoles.getRoleName());
			query.setParameter("roleType", appRoles.getRoleType());
			query.setParameter("roleId", appRoles.getAppRoleId());
			query.executeUpdate();
			transaction.commit();
			updatedStatus = "success";
		} catch (Exception e) {
			e.printStackTrace();
			log.error("message", e);
			updatedStatus = "failed";
		} finally {
			session.close();
		}
		return updatedStatus;
	}

	/**
	 * Delete user types.
	 * 
	 * @param userType
	 *            the user type
	 * @return the string
	 */
	public String deleteAppRoles(AppRoles appRoles) {
		Session session = null;
		Transaction transaction = null;
		String deletedStatus = null;
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			String hql = "Delete from AppRoles where appRoleId = :id";
			Query query = session.createQuery(hql);
			query.setParameter("id", appRoles.getAppRoleId());
			query.executeUpdate();
			transaction.commit();
			deletedStatus = "success";
		} catch (Exception e) {
			e.printStackTrace();
			log.error("message", e);
			deletedStatus = "failed";
		} finally {
			session.close();
		}
		return deletedStatus;
	}

}
