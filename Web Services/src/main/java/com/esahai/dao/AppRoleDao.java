package com.esahai.dao;

import java.util.List;

import com.esahai.entity.AppRoles;

// TODO: Auto-generated Javadoc
/**
 * 
 * @author Santhosh
 * The Interface AppRoleDao.
 */
public interface AppRoleDao {
	
	/**
	 * Gets the app roles list.
	 *
	 * @return the app roles list
	 */
	List<AppRoles> getAppRolesList();
	
	/**
	 * Save app roles.
	 *
	 * @param appRoles the app roles
	 * @return the string
	 */
	String saveAppRoles(AppRoles appRoles);
	
	/**
	 * Update app roles.
	 *
	 * @param appRoles the app roles
	 * @return the string
	 */
	String updateAppRoles(AppRoles appRoles);
	
	/**
	 * Delete app roles.
	 *
	 * @param appRoles the app roles
	 * @return the string
	 */
	String deleteAppRoles(AppRoles appRoles);
	

}
