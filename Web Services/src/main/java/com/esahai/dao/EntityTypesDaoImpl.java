package com.esahai.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

import com.esahai.entity.EntityTypes;

/**
 * The Class EntityTypesDaoImpl.
 */

/**
 * The EntityTypesDaoImpl implements EntityTypesDao application that simply
 * displays list of EntityTypes , to save,update and delete the EntityTypes, to
 * the standard output.
 *
 * @author Swathi P
 * @version 1.0
 * @since 2017-07-02
 */

@Repository("entityTypesDaoImpl")
public class EntityTypesDaoImpl implements EntityTypeDao {

	/** The Constant logger. */
	public static final Logger logger = Logger.getLogger(EntityTypesDaoImpl.class);

	/** The session factory. */
	private SessionFactory sessionFactory;

	/**
	 * Sets the session factory.
	 *
	 * @param sessionFactory
	 *            the new session factory
	 */
	// setters for Session Factory
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.esahai.dao.EntityTypeDao#getEntityTypes()
	 */
	@SuppressWarnings("unchecked")
	public List<EntityTypes> getEntityTypes() {
		List<EntityTypes> list = null;
		Session session = null;
		Criteria criteria = null;

		try {
			// getting session object from session factory
			session = sessionFactory.openSession();
			criteria = session.createCriteria(EntityTypes.class);
			criteria.addOrder(Order.asc("type_name"));
			list = criteria.list();
			logger.info("ascending order = "+ list);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("exception occured = "+ e.getMessage());
			logger.error(e);
			return list;
		} finally {
			session.close();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.esahai.dao.EntityTypeDao#saveEntityType(com.esahai.entity.
	 * EntityTypes)
	 */
	public boolean saveEntityType(EntityTypes entityTypes) {
		logger.info("Entity type object = "+ entityTypes);

		Session session = null;
		try {
			// getting session object from session factory
			session = sessionFactory.openSession();
			// getting transaction object from session object
			session.beginTransaction();
			session.save(entityTypes);
			logger.info("Entity Type Inserted Successfully = "+ entityTypes);
			session.getTransaction().commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Message = " + e.getMessage());
			logger.error(e);
			return false;
		} finally {
			session.close();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.esahai.dao.EntityTypeDao#updateEntityType(com.esahai.entity.
	 * EntityTypes)
	 */
	public boolean updateEntityType(EntityTypes entityTypes) {
		logger.info("Entity type object = "+ entityTypes);

		Session session = null;
		try {
			// getting session object from session factory
			session = sessionFactory.openSession();
			// getting transaction object from session object
			session.beginTransaction();
			String SQL_FOR_ENTITY_TYPE_UPDATE = "Update EntityTypes set type_name=:type_name, updated_date=:updated_date where entity_type_id = :entity_type_id";

			Query query = session.createQuery(SQL_FOR_ENTITY_TYPE_UPDATE);
			query.setParameter("type_name", entityTypes.getType_name());
			query.setParameter("updated_date", entityTypes.getUpdated_date());
			query.setParameter("entity_type_id", entityTypes.getEntity_type_id());

			query.executeUpdate();
			logger.info("Entity Type Updated Successfully = "+ entityTypes);
			session.getTransaction().commit();

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Message = " + e.getMessage());
			logger.error(e);
			return false;

		} finally {
			session.close();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.esahai.dao.EntityTypeDao#deleteEntityType(int, java.lang.String)
	 */
	public boolean deleteEntityType(int entity_type_id, String status) {

		logger.info("entity_type_id = " + entity_type_id);
		logger.info("status = " + status);
		Session session = null;
		try {
			// getting session object from session factory
			session = sessionFactory.openSession();
			// getting transaction object from session object
			session.beginTransaction();
			// String SQL_FOR_ENTITY_TYPE_DELETE = "Delete EntityTypes where
			// entity_type_id = :entity_type_id";
			String SQL_FOR_ENTITY_TYPE_DELETE = "Update EntityTypes set status=:status where entity_type_id = :entity_type_id";
			Query query = session.createQuery(SQL_FOR_ENTITY_TYPE_DELETE);
			query.setParameter("entity_type_id", entity_type_id);
			query.setParameter("status", status);
			query.executeUpdate();
			logger.info("Entity Type Deleted Successfully = "+ query);
			session.getTransaction().commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(" Message = " + e.getMessage());
			logger.error(e);
			return false;
		} finally {
			session.close();
		}
	}
}
