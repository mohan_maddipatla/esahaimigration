package com.esahai.dao;

import java.util.List;

import com.esahai.entity.EntityTypes;

/**
 * The Interface EntityTypeDao.
 */
public interface EntityTypeDao {

	/**
	 * Gets the entity types.
	 *
	 * @return the entity types
	 */
	public List<EntityTypes> getEntityTypes();
	
	/**
	 * Save entity type.
	 *
	 * @param entityTypes the entity types
	 * @return true, if successful
	 */
	public boolean saveEntityType(EntityTypes entityTypes);
	
	/**
	 * Update entity type.
	 *
	 * @param entityTypes the entity types
	 * @return true, if successful
	 */
	public boolean updateEntityType(EntityTypes entityTypes);
	
	/**
	 * Delete entity type.
	 *
	 * @param entity_type_id the entity type id
	 * @param status the status
	 * @return true, if successful
	 */
	public boolean deleteEntityType(int entity_type_id, String status);

}
