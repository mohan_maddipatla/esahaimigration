package com.esahai.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

// TODO: Auto-generated Javadoc
/**
 * The Class EmergencyBooking.
 */
@Entity
@Table(name="emergency_booking")
public class EmergencyBooking implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The booking id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "booking_id")
	private Long bookingId;

	/** The customer mobile. */
	@Column(name = "customer_mobile")
	private String customerMobile;

	/** The customer name. */
	@Column(name = "customer_name")
	private String customerName;

	/** The booked by. */
	@Column(name = "booked_by")
	private String bookedBy;

	/** The emergency type. */
	@Column(name = "emergency_type")
	private String emergencyType;

	/** The emergency lontitude. */
	@Column(name = "emergency_longitude")
	private String emergencyLontitude;

	/** The emergency latitude. */
	@Column(name = "emergency_latitude")
	private String emergencyLatitude;

	/** The ambulance id. */
	@Column(name = "ambulance_id")
	private Integer ambulanceId;

	/** The hospital. */
	@Column(name = "hospital")
	private String hospital;

	/** The ambulance start longitude. */
	@Column(name = "ambulance_start_longitude")
	private String ambulanceStartLongitude;

	/** The ambulance start latitude. */
	@Column(name = "ambulance_start_latitude")
	private String ambulanceStartLatitude;

	/** The created date time. */
	@Column(name = "created_datetime")
	private Date createdDateTime;

	/** The updated date time. */
	@Column(name = "updated_datetime")
	private Date updatedDateTime;

	/** The distance. */
	@Column(name = "distance")
	private String distance;

	/** The duration. */
	@Column(name = "duration")
	private String duration;

	/** The cost. */
	@Column(name = "cost")
	private String cost;

	/** The emergency geo hash code. */
	@Column(name = "emergency_geoHashCode")
	private String emergencyGeoHashCode;

	/** The ambulance start geo hash code. */
	@Column(name = "ambulance_start_geoHashCode")
	private String ambulanceStartGeoHashCode;

	/** The booking status. */
	@Column(name = "booking_status")
	private String bookingStatus;

	/** The customer id. */
	@Column(name = "customer_id")
	private String customerId;

	/** The eme address. */
	@Column(name = "eme_address")
	private String emeAddress;

	/** The ambulance number. */
	@Column(name = "ambulance_number")
	private String ambulanceNumber;

	/** The group id. */
	@Column(name = "group_id")
	private String groupId;

	/** The group type id. */
	@Column(name = "group_type_id")
	private String groupTypeId;

	/** The emergency type id. */
	@Column(name = "emergency_type_id")
	private String emergencyTypeId;

	/** The is self. */
	@Column(name = "is_self")
	private Integer isSelf;

	/** The patient number. */
	@Column(name = "patient_number")
	private String patientNumber;

	/** The reason. */
	@Column(name = "reason")
	private String reason;

	/** The drop latitude. */
	@Column(name = "drop_latitude")
	private String dropLatitude;

	/** The drop longitude. */
	@Column(name = "drop_longitude")
	private String dropLongitude;

	/** The drop geo hash code. */
	@Column(name = "drop_geoHashCode")
	private String dropGeoHashCode;

	/** The pickup datetime. */
	@Column(name = "pickup_datetime")
	private Date pickupDatetime;

	/** The assistance. */
	@Column(name = "assistance")
	private String assistance;

	/**
	 * Gets the booking id.
	 *
	 * @return the booking id
	 */
	public Long getBookingId() {
		return bookingId;
	}

	/**
	 * Sets the booking id.
	 *
	 * @param bookingId the new booking id
	 */
	public void setBookingId(Long bookingId) {
		this.bookingId = bookingId;
	}

	/**
	 * Gets the customer mobile.
	 *
	 * @return the customer mobile
	 */
	public String getCustomerMobile() {
		return customerMobile;
	}

	/**
	 * Sets the customer mobile.
	 *
	 * @param customerMobile the new customer mobile
	 */
	public void setCustomerMobile(String customerMobile) {
		this.customerMobile = customerMobile;
	}

	/**
	 * Gets the customer name.
	 *
	 * @return the customer name
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * Sets the customer name.
	 *
	 * @param customerName the new customer name
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * Gets the booked by.
	 *
	 * @return the booked by
	 */
	public String getBookedBy() {
		return bookedBy;
	}

	/**
	 * Sets the booked by.
	 *
	 * @param bookedBy the new booked by
	 */
	public void setBookedBy(String bookedBy) {
		this.bookedBy = bookedBy;
	}

	/**
	 * Gets the emergency type.
	 *
	 * @return the emergency type
	 */
	public String getEmergencyType() {
		return emergencyType;
	}

	/**
	 * Sets the emergency type.
	 *
	 * @param emergencyType the new emergency type
	 */
	public void setEmergencyType(String emergencyType) {
		this.emergencyType = emergencyType;
	}

	/**
	 * Gets the emergency lontitude.
	 *
	 * @return the emergency lontitude
	 */
	public String getEmergencyLontitude() {
		return emergencyLontitude;
	}

	/**
	 * Sets the emergency lontitude.
	 *
	 * @param emergencyLontitude the new emergency lontitude
	 */
	public void setEmergencyLontitude(String emergencyLontitude) {
		this.emergencyLontitude = emergencyLontitude;
	}

	/**
	 * Gets the emergency latitude.
	 *
	 * @return the emergency latitude
	 */
	public String getEmergencyLatitude() {
		return emergencyLatitude;
	}

	/**
	 * Sets the emergency latitude.
	 *
	 * @param emergencyLatitude the new emergency latitude
	 */
	public void setEmergencyLatitude(String emergencyLatitude) {
		this.emergencyLatitude = emergencyLatitude;
	}

	/**
	 * Gets the ambulance id.
	 *
	 * @return the ambulance id
	 */
	public Integer getAmbulanceId() {
		return ambulanceId;
	}

	/**
	 * Sets the ambulance id.
	 *
	 * @param ambulanceId the new ambulance id
	 */
	public void setAmbulanceId(Integer ambulanceId) {
		this.ambulanceId = ambulanceId;
	}

	/**
	 * Gets the hospital.
	 *
	 * @return the hospital
	 */
	public String getHospital() {
		return hospital;
	}

	/**
	 * Sets the hospital.
	 *
	 * @param hospital the new hospital
	 */
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}

	/**
	 * Gets the ambulance start longitude.
	 *
	 * @return the ambulance start longitude
	 */
	public String getAmbulanceStartLongitude() {
		return ambulanceStartLongitude;
	}

	/**
	 * Sets the ambulance start longitude.
	 *
	 * @param ambulanceStartLongitude the new ambulance start longitude
	 */
	public void setAmbulanceStartLongitude(String ambulanceStartLongitude) {
		this.ambulanceStartLongitude = ambulanceStartLongitude;
	}

	/**
	 * Gets the ambulance start latitude.
	 *
	 * @return the ambulance start latitude
	 */
	public String getAmbulanceStartLatitude() {
		return ambulanceStartLatitude;
	}

	/**
	 * Sets the ambulance start latitude.
	 *
	 * @param ambulanceStartLatitude the new ambulance start latitude
	 */
	public void setAmbulanceStartLatitude(String ambulanceStartLatitude) {
		this.ambulanceStartLatitude = ambulanceStartLatitude;
	}

	/**
	 * Gets the created date time.
	 *
	 * @return the created date time
	 */
	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	/**
	 * Sets the created date time.
	 *
	 * @param createdDateTime the new created date time
	 */
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	/**
	 * Gets the updated date time.
	 *
	 * @return the updated date time
	 */
	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}

	/**
	 * Sets the updated date time.
	 *
	 * @param updatedDateTime the new updated date time
	 */
	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	/**
	 * Gets the distance.
	 *
	 * @return the distance
	 */
	public String getDistance() {
		return distance;
	}

	/**
	 * Sets the distance.
	 *
	 * @param distance the new distance
	 */
	public void setDistance(String distance) {
		this.distance = distance;
	}

	/**
	 * Gets the duration.
	 *
	 * @return the duration
	 */
	public String getDuration() {
		return duration;
	}

	/**
	 * Sets the duration.
	 *
	 * @param duration the new duration
	 */
	public void setDuration(String duration) {
		this.duration = duration;
	}

	/**
	 * Gets the cost.
	 *
	 * @return the cost
	 */
	public String getCost() {
		return cost;
	}

	/**
	 * Sets the cost.
	 *
	 * @param cost the new cost
	 */
	public void setCost(String cost) {
		this.cost = cost;
	}

	/**
	 * Gets the emergency geo hash code.
	 *
	 * @return the emergency geo hash code
	 */
	public String getEmergencyGeoHashCode() {
		return emergencyGeoHashCode;
	}

	/**
	 * Sets the emergency geo hash code.
	 *
	 * @param emergencyGeoHashCode the new emergency geo hash code
	 */
	public void setEmergencyGeoHashCode(String emergencyGeoHashCode) {
		this.emergencyGeoHashCode = emergencyGeoHashCode;
	}

	/**
	 * Gets the ambulance start geo hash code.
	 *
	 * @return the ambulance start geo hash code
	 */
	public String getAmbulanceStartGeoHashCode() {
		return ambulanceStartGeoHashCode;
	}

	/**
	 * Sets the ambulance start geo hash code.
	 *
	 * @param ambulanceStartGeoHashCode the new ambulance start geo hash code
	 */
	public void setAmbulanceStartGeoHashCode(String ambulanceStartGeoHashCode) {
		this.ambulanceStartGeoHashCode = ambulanceStartGeoHashCode;
	}

	/**
	 * Gets the booking status.
	 *
	 * @return the booking status
	 */
	public String getBookingStatus() {
		return bookingStatus;
	}

	/**
	 * Sets the booking status.
	 *
	 * @param bookingStatus the new booking status
	 */
	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}

	/**
	 * Gets the customer id.
	 *
	 * @return the customer id
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * Sets the customer id.
	 *
	 * @param customerId the new customer id
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	/**
	 * Gets the eme address.
	 *
	 * @return the eme address
	 */
	public String getEmeAddress() {
		return emeAddress;
	}

	/**
	 * Sets the eme address.
	 *
	 * @param emeAddress the new eme address
	 */
	public void setEmeAddress(String emeAddress) {
		this.emeAddress = emeAddress;
	}

	/**
	 * Gets the ambulance number.
	 *
	 * @return the ambulance number
	 */
	public String getAmbulanceNumber() {
		return ambulanceNumber;
	}

	/**
	 * Sets the ambulance number.
	 *
	 * @param ambulanceNumber the new ambulance number
	 */
	public void setAmbulanceNumber(String ambulanceNumber) {
		this.ambulanceNumber = ambulanceNumber;
	}

	/**
	 * Gets the group id.
	 *
	 * @return the group id
	 */
	public String getGroupId() {
		return groupId;
	}

	/**
	 * Sets the group id.
	 *
	 * @param groupId the new group id
	 */
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	/**
	 * Gets the group type id.
	 *
	 * @return the group type id
	 */
	public String getGroupTypeId() {
		return groupTypeId;
	}

	/**
	 * Sets the group type id.
	 *
	 * @param groupTypeId the new group type id
	 */
	public void setGroupTypeId(String groupTypeId) {
		this.groupTypeId = groupTypeId;
	}

	/**
	 * Gets the emergency type id.
	 *
	 * @return the emergency type id
	 */
	public String getEmergencyTypeId() {
		return emergencyTypeId;
	}

	/**
	 * Sets the emergency type id.
	 *
	 * @param emergencyTypeId the new emergency type id
	 */
	public void setEmergencyTypeId(String emergencyTypeId) {
		this.emergencyTypeId = emergencyTypeId;
	}

	/**
	 * Gets the checks if is self.
	 *
	 * @return the checks if is self
	 */
	public Integer getIsSelf() {
		return isSelf;
	}

	/**
	 * Sets the checks if is self.
	 *
	 * @param isSelf the new checks if is self
	 */
	public void setIsSelf(Integer isSelf) {
		this.isSelf = isSelf;
	}

	/**
	 * Gets the patient number.
	 *
	 * @return the patient number
	 */
	public String getPatientNumber() {
		return patientNumber;
	}

	/**
	 * Sets the patient number.
	 *
	 * @param patientNumber the new patient number
	 */
	public void setPatientNumber(String patientNumber) {
		this.patientNumber = patientNumber;
	}

	/**
	 * Gets the reason.
	 *
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * Sets the reason.
	 *
	 * @param reason the new reason
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * Gets the drop latitude.
	 *
	 * @return the drop latitude
	 */
	public String getDropLatitude() {
		return dropLatitude;
	}

	/**
	 * Sets the drop latitude.
	 *
	 * @param dropLatitude the new drop latitude
	 */
	public void setDropLatitude(String dropLatitude) {
		this.dropLatitude = dropLatitude;
	}

	/**
	 * Gets the drop longitude.
	 *
	 * @return the drop longitude
	 */
	public String getDropLongitude() {
		return dropLongitude;
	}

	/**
	 * Sets the drop longitude.
	 *
	 * @param dropLongitude the new drop longitude
	 */
	public void setDropLongitude(String dropLongitude) {
		this.dropLongitude = dropLongitude;
	}

	/**
	 * Gets the drop geo hash code.
	 *
	 * @return the drop geo hash code
	 */
	public String getDropGeoHashCode() {
		return dropGeoHashCode;
	}

	/**
	 * Sets the drop geo hash code.
	 *
	 * @param dropGeoHashCode the new drop geo hash code
	 */
	public void setDropGeoHashCode(String dropGeoHashCode) {
		this.dropGeoHashCode = dropGeoHashCode;
	}

	/**
	 * Gets the pickup datetime.
	 *
	 * @return the pickup datetime
	 */
	public Date getPickupDatetime() {
		return pickupDatetime;
	}

	/**
	 * Sets the pickup datetime.
	 *
	 * @param pickupDatetime the new pickup datetime
	 */
	public void setPickupDatetime(Date pickupDatetime) {
		this.pickupDatetime = pickupDatetime;
	}

	/**
	 * Gets the assistance.
	 *
	 * @return the assistance
	 */
	public String getAssistance() {
		return assistance;
	}

	/**
	 * Sets the assistance.
	 *
	 * @param assistance the new assistance
	 */
	public void setAssistance(String assistance) {
		this.assistance = assistance;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "Booking Id = " + this.bookingId + ", Booking Status = " + this.bookingStatus + ", cost = " + this.cost;
	}
}