package com.esahai.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class SystemUsers.
 * 
 * containing setter and getter methods .
 */
@Entity
@Table(name = "system_users")
public class SystemUsers implements Serializable {

	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	/** The email. */
	@Column(name = "email")
	private String email;

	/** The firstName. */
	@Column(name = "firstName")
	private String firstName;

	/** The lastName. */
	@Column(name = "lastName")
	private String lastName;

	/** The password. */
	@Column(name = "password")
	private String password;

	/** The displayName. */
	@Column(name = "displayName")
	private String displayName;

	/** The is isActive. */
	@Column(name = "isActive")
	private int isActive;

	/** The token. */
	@Column(name = "token")
	private String token;

	/** The email_invitation_accepted. */
	@Column(name = "email_invitation_accepted")
	private int email_invitation_accepted;

	/** The role id. */
	@Column(name = "role_id")
	private int role_id;

	/** The group id. */
	@Column(name = "group_id")
	private int group_id;

	@Column(name = "status")
	private String status;

	/** The created_datetime. */
	@Column(name = "created_datetime")
	private String created_datetime;

	/** The updated_datetime. */
	@Column(name = "updated_datetime")
	private String updated_datetime;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets and Sets the email.
	 *
	 ** @param getEmail
	 *            the new getemail
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email
	 *            the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the firstname.
	 *
	 * @return the firstname
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the firstname.
	 *
	 * @param firstname
	 *            the new firstname
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets the lastname.
	 *
	 * @return the lastname
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the lastname.
	 *
	 * @param lastname
	 *            the new lastname
	 */
	public void setLastname(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password
	 *            the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName
	 *            the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * Sets the token.
	 *
	 * @param token
	 *            the new token
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * Gets the role id.
	 *
	 * @return the role id
	 */
	public int getRole_id() {
		return role_id;
	}

	/**
	 * Sets the role id.
	 *
	 * @param role_id
	 *            the new role id
	 */
	public void setRole_id(int role_id) {
		this.role_id = role_id;
	}

	/**
	 * Gets the group id.
	 *
	 * @return the group id
	 */
	public int getGroup_id() {
		return group_id;
	}

	/**
	 * Sets the group id.
	 *
	 * @param group_id
	 *            the new group id
	 */
	public void setGroup_id(int group_id) {
		this.group_id = group_id;
	}

	/**
	 * @return the created_datetime
	 */
	public String getCreated_datetime() {
		return created_datetime;
	}

	/**
	 * @param created_datetime
	 *            the created_datetime to set
	 */
	public void setCreated_datetime(String created_datetime) {
		this.created_datetime = created_datetime;
	}

	/**
	 * @return the updated_datetime
	 */
	public String getUpdated_datetime() {
		return updated_datetime;
	}

	/**
	 * @param updated_datetime
	 *            the updated_datetime to set
	 */
	public void setUpdated_datetime(String updated_datetime) {
		this.updated_datetime = updated_datetime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the isActive
	 */
	public int getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the email_invitation_accepted
	 */
	public int getEmail_invitation_accepted() {
		return email_invitation_accepted;
	}

	/**
	 * @param email_invitation_accepted
	 *            the email_invitation_accepted to set
	 */
	public void setEmail_invitation_accepted(int email_invitation_accepted) {
		this.email_invitation_accepted = email_invitation_accepted;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SystemUsers [id=" + id + ", email=" + email + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", password=" + password + ", displayName=" + displayName + ", isActive=" + isActive + ", token="
				+ token + ", email_invitation_accepted=" + email_invitation_accepted + ", role_id=" + role_id
				+ ", group_id=" + group_id + ", status=" + status + ", created_datetime=" + created_datetime
				+ ", updated_datetime=" + updated_datetime + "]";
	}

}
