package com.esahai.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class AppVersion.
 */
@Entity
@Table(name = "app_version")
public class AppVersion implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	/** The previous version number. */
	@Column(name = "previous_version_number")
	private String previous_version_number;

	/** The current version number. */
	@Column(name = "current_version_number")
	private String current_version_number;

	/** The build date. */
	@Column(name = "build_date")
	private Timestamp build_date;

	/** The build os. */
	@Column(name = "build_os")
	private String build_os;

	/** The force upgrade. */
	@Column(name = "force_upgrade")
	private int force_upgrade;

	/** The app type. */
	@Column(name = "app_type")
	private String app_type;

	/** The created date. */
	@Column(name = "created_date")
	private String created_date;

	/** The updated date. */
	@Column(name = "updated_date")
	private String updated_date;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the previous_version_number
	 */
	public String getPrevious_version_number() {
		return previous_version_number;
	}

	/**
	 * @param previous_version_number
	 *            the previous_version_number to set
	 */
	public void setPrevious_version_number(String previous_version_number) {
		this.previous_version_number = previous_version_number;
	}

	/**
	 * @return the current_version_number
	 */
	public String getCurrent_version_number() {
		return current_version_number;
	}

	/**
	 * @param current_version_number
	 *            the current_version_number to set
	 */
	public void setCurrent_version_number(String current_version_number) {
		this.current_version_number = current_version_number;
	}

	/**
	 * @return the build_date
	 */
	public Timestamp getBuild_date() {
		return build_date;
	}

	/**
	 * @param build_date
	 *            the build_date to set
	 */
	public void setBuild_date(Timestamp build_date) {
		this.build_date = build_date;
	}

	/**
	 * @return the build_os
	 */
	public String getBuild_os() {
		return build_os;
	}

	/**
	 * @param build_os
	 *            the build_os to set
	 */
	public void setBuild_os(String build_os) {
		this.build_os = build_os;
	}

	/**
	 * @return the force_upgrade
	 */
	public int getForce_upgrade() {
		return force_upgrade;
	}

	/**
	 * @param force_upgrade
	 *            the force_upgrade to set
	 */
	public void setForce_upgrade(int force_upgrade) {
		this.force_upgrade = force_upgrade;
	}

	/**
	 * @return the app_type
	 */
	public String getApp_type() {
		return app_type;
	}

	/**
	 * @param app_type
	 *            the app_type to set
	 */
	public void setApp_type(String app_type) {
		this.app_type = app_type;
	}

	/**
	 * @return the created_date
	 */
	public String getCreated_date() {
		return created_date;
	}

	/**
	 * @param created_date
	 *            the created_date to set
	 */
	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}

	/**
	 * @return the updated_date
	 */
	public String getUpdated_date() {
		return updated_date;
	}

	/**
	 * @param updated_date
	 *            the updated_date to set
	 */
	public void setUpdated_date(String updated_date) {
		this.updated_date = updated_date;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AppVersion [id=" + id + ", previous_version_number=" + previous_version_number
				+ ", current_version_number=" + current_version_number + ", build_date=" + build_date + ", build_os="
				+ build_os + ", force_upgrade=" + force_upgrade + ", app_type=" + app_type + ", created_date="
				+ created_date + ", updated_date=" + updated_date + "]";
	}

}
