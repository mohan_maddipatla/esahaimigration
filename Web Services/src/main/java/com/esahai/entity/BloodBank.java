package com.esahai.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

// TODO: Auto-generated Javadoc
/**
 * The Class BloodBank.
 */
@Entity
@Table(name = "blood_bank")
public class BloodBank implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	/** The name. */
	@Column(name = "name")
	private String name;

	/** The contact number. */
	@Column(name = "contact_number_1")
	private long contactNumber;

	/** The contact number 2. */
	@Column(name = "contact_number_2")
	private Integer contactNumber2;

	/** The contact number 3. */
	@Column(name = "contact_number_3")
	private Integer contactNumber3;

	/** The website. */
	@Column(name = "website")
	private String website;

	/** The address. */
	@Column(name = "address")
	private String address;

	/** The latitude. */
	@Column(name = "latitude")
	private BigDecimal latitude;

	/** The longitude. */
	@Column(name = "longitude")
	private BigDecimal longitude;

	/** The geohash. */
	@Column(name = "geohash")
	private String geohash;

	/**
	 * @return the geohash
	 */
	public String getGeohash() {
		return geohash;
	}

	/**
	 * @param geohash
	 *            the geohash to set
	 */
	public void setGeohash(String geohash) {
		this.geohash = geohash;
	}

	/** The created date. */
	@Column(name = "created_date")
	private String created_date;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the contact number.
	 *
	 * @return the contact number
	 */
	public int getContactNumber() {
		return (int) (contactNumber);
	}

	/**
	 * Gets the contact number 2.
	 *
	 * @return the contact number 2
	 */
	public Integer getContactNumber2() {
		return contactNumber2;
	}

	/**
	 * Gets the contact number 3.
	 *
	 * @return the contact number 3
	 */
	public Integer getContactNumber3() {
		return contactNumber3;
	}

	/**
	 * Sets the contact number.
	 *
	 * @param contactNumber
	 *            the new contact number
	 */
	public void setContactNumber(long contactNumber) {
		this.contactNumber = contactNumber;
	}

	/**
	 * Sets the contact number 2.
	 *
	 * @param contactNumber
	 *            the new contact number 2
	 */
	public void setContactNumber2(Integer contactNumber) {
		this.contactNumber2 = contactNumber;
	}

	/**
	 * Sets the contact number 3.
	 *
	 * @param contactNumber
	 *            the new contact number 3
	 */
	public void setContactNumber3(Integer contactNumber) {
		this.contactNumber3 = contactNumber;
	}

	/**
	 * Sets the longitude.
	 *
	 * @param longitude
	 *            the new longitude
	 */
	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	/**
	 * Gets the longitude.
	 *
	 * @return the longitude
	 */
	public BigDecimal getLongitude() {
		return longitude;
	}

	/**
	 * Sets the latitude.
	 *
	 * @param latitude
	 *            the new latitude
	 */
	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	/**
	 * Gets the latitude.
	 *
	 * @return the latitude
	 */
	public BigDecimal getLatitude() {
		return latitude;
	}

	/**
	 * @return the website
	 */
	public String getWebsite() {
		return website;
	}

	/**
	 * @param website
	 *            the website to set
	 */
	public void setWebsite(String website) {
		this.website = website;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the created_date
	 */
	public String getCreated_date() {
		return created_date;
	}

	/**
	 * @param created_date
	 *            the created_date to set
	 */
	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BloodBank [id=" + id + ", name=" + name + ", contactNumber=" + contactNumber + ", contactNumber2="
				+ contactNumber2 + ", contactNumber3=" + contactNumber3 + ", website=" + website + ", address="
				+ address + ", latitude=" + latitude + ", longitude=" + longitude + ", geohash=" + geohash
				+ ", created_date=" + created_date + "]";
	}

}