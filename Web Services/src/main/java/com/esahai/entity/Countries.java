package com.esahai.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "country_list")
public class Countries implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The entity type id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	/** The country_name. */
	@Column(name = "country_name")
	private String country_name;

	/** The country_code. */
	@Column(name = "country_code")
	private String country_code;

	/** The is_active. */
	@Column(name = "is_active")
	private int is_active;

	/** The created date. */
	@Column(name = "created_datetime")
	private String created_datetime;

	/** The updated datetime. */
	@Column(name = "updated_datetime")
	private String updated_datetime;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the country_name
	 */
	public String getCountry_name() {
		return country_name;
	}

	/**
	 * @param country_name
	 *            the country_name to set
	 */
	public void setCountry_name(String country_name) {
		this.country_name = country_name;
	}

	/**
	 * @return the country_code
	 */
	public String getCountry_code() {
		return country_code;
	}

	/**
	 * @param country_code
	 *            the country_code to set
	 */
	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}

	/**
	 * @return the is_active
	 */
	public int getIs_active() {
		return is_active;
	}

	/**
	 * @param is_active
	 *            the is_active to set
	 */
	public void setIs_active(int is_active) {
		this.is_active = is_active;
	}

	/**
	 * @return the created_datetime
	 */
	public String getCreated_datetime() {
		return created_datetime;
	}

	/**
	 * @param created_datetime
	 *            the created_datetime to set
	 */
	public void setCreated_datetime(String created_datetime) {
		this.created_datetime = created_datetime;
	}

	/**
	 * @return the updated_datetime
	 */
	public String getUpdated_datetime() {
		return updated_datetime;
	}

	/**
	 * @param updated_datetime
	 *            the updated_datetime to set
	 */
	public void setUpdated_datetime(String updated_datetime) {
		this.updated_datetime = updated_datetime;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Countries [id=" + id + ", country_name=" + country_name + ", country_code=" + country_code
				+ ", is_active=" + is_active + ", created_datetime=" + created_datetime + ", updated_datetime="
				+ updated_datetime + "]";
	}

}
