package com.esahai.entity;

import java.sql.Timestamp;

public class LoginJsonEntity {
	private int id;
	private String email;
	private String lastName;
	private String firstName;
	private String displayName;
	private boolean isActive;
	private boolean emailInvitationAccepted;
	private int roleId;
	private int groupId;
	private Timestamp createdOn;
	private String roleName;
	private String token;
	private Timestamp updatedOn;
	
	
	
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	public int getRoleId() {
		return roleId;
	}
	
	public boolean isEmailInvitationAccepted() {
		return emailInvitationAccepted;
	}
	public void setEmailInvitationAccepted(boolean emailInvitationAccepted) {
		this.emailInvitationAccepted = emailInvitationAccepted;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public int getGroupId() {
		return groupId;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}
	
	
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LoginJsonEntity [id=");
		builder.append(id);
		builder.append(", email=");
		builder.append(email);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", firstName=");
		builder.append(firstName);
		builder.append(", displayName=");
		builder.append(displayName);
		builder.append(", isActive=");
		builder.append(isActive);
		builder.append(", emailInvitationAccepted=");
		builder.append(emailInvitationAccepted);
		builder.append(", roleId=");
		builder.append(roleId);
		builder.append(", groupId=");
		builder.append(groupId);
		builder.append(", createdOn=");
		builder.append(createdOn);
		builder.append(", roleName=");
		builder.append(roleName);
		builder.append(", token=");
		builder.append(token);
		builder.append(", updatedOn=");
		builder.append(updatedOn);
		builder.append("]");
		return builder.toString();
	}
	
	
	
	
	

}
