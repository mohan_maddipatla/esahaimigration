package com.esahai.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

// TODO: Auto-generated Javadoc
/**
 * The Class EntityTypes.
 */
@Entity
@Table(name = "entity_types")
public class EntityTypes implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The entity type id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "entity_type_id")
	private int entity_type_id;

	/** The type name. */
	@Column(name = "type_name")
	private String type_name;

	/** The created date. */
	@Column(name = "created_date")
	private Timestamp created_date;

	/** The updated date. */
	@Column(name = "updated_date")
	private Timestamp updated_date;
	
	/** The status. */
	@Column(name = "status")
	private String status;

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the entity type id.
	 *
	 * @return the entity_type_id
	 */
	public int getEntity_type_id() {
		return entity_type_id;
	}

	/**
	 * Sets the entity type id.
	 *
	 * @param entity_type_id            the entity_type_id to set
	 */
	public void setEntity_type_id(int entity_type_id) {
		this.entity_type_id = entity_type_id;
	}

	/**
	 * Gets the type name.
	 *
	 * @return the type_name
	 */
	public String getType_name() {
		return type_name;
	}

	/**
	 * Sets the type name.
	 *
	 * @param type_name            the type_name to set
	 */
	public void setType_name(String type_name) {
		this.type_name = type_name;
	}

	/**
	 * Gets the created date.
	 *
	 * @return the created_date
	 */
	public Timestamp getCreated_date() {
		return created_date;
	}

	/**
	 * Sets the created date.
	 *
	 * @param created_date            the created_date to set
	 */
	public void setCreated_date(Timestamp created_date) {
		this.created_date = created_date;
	}

	/**
	 * Gets the updated date.
	 *
	 * @return the updated_date
	 */
	public Timestamp getUpdated_date() {
		return updated_date;
	}

	/**
	 * Sets the updated date.
	 *
	 * @param updated_date            the updated_date to set
	 */
	public void setUpdated_date(Timestamp updated_date) {
		this.updated_date = updated_date;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EntityTypes [entity_type_id=" + entity_type_id + ", type_name=" + type_name + ", created_date="
				+ created_date + ", updated_date=" + updated_date + ", status=" + status + "]";
	}
}
