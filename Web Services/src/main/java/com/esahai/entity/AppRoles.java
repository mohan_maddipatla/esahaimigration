package com.esahai.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

// TODO: Auto-generated Javadoc
/**
 * The Class AppRoles.
 */
@Entity
@Table(name= "app_roles")
public class AppRoles {
	
	/** The app role id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int appRoleId;
	
	/** The role name. */
	@Column(name="role_name")
	private String roleName;
	
	/** The role type. */
	@Column(name="role_type")
	private String roleType;
	
	/** The created on. */
	@Transient
	@Column(name="created_date")
	private String createdOn;
	
	/** The updated on. */
	@Column(name="updated_date")
	private String updatedOn;

	/**
	 * Gets the app role id.
	 *
	 * @return the app role id
	 */
	public int getAppRoleId() {
		return appRoleId;
	}

	/**
	 * Sets the app role id.
	 *
	 * @param appRoleId the new app role id
	 */
	public void setAppRoleId(int appRoleId) {
		this.appRoleId = appRoleId;
	}

	/**
	 * Gets the role name.
	 *
	 * @return the role name
	 */
	public String getRoleName() {
		return roleName;
	}

	/**
	 * Sets the role name.
	 *
	 * @param roleName the new role name
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	/**
	 * Gets the role type.
	 *
	 * @return the role type
	 */
	public String getRoleType() {
		return roleType;
	}

	/**
	 * Sets the role type.
	 *
	 * @param roleType the new role type
	 */
	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	/**
	 * Gets the created on.
	 *
	 * @return the created on
	 */
	public String getCreatedOn() {
		return createdOn;
	}

	/**
	 * Sets the created on.
	 *
	 * @param createdOn the new created on
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * Gets the updated on.
	 *
	 * @return the updated on
	 */
	public String getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * Sets the updated on.
	 *
	 * @param updatedOn the new updated on
	 */
	public void setUpdatedOn(String updatedOn) {
		this.updatedOn = updatedOn;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AppRoles [appRoleId=");
		builder.append(appRoleId);
		builder.append(", roleName=");
		builder.append(roleName);
		builder.append(", roleType=");
		builder.append(roleType);
		builder.append(", createdOn=");
		builder.append(createdOn);
		builder.append(", updatedOn=");
		builder.append(updatedOn);
		builder.append("]");
		return builder.toString();
	}
}
